import { can } from '../helpers/gate'

export default (el, binding, vnode) => {
    const gate = binding.value
    if (can(gate)) {
        $(function () {
            $(el).remove()
        })
    }
}
