import vue from 'vue'
import app from './app'
import router from './routes'
import store from './stores'

require('./bootstrap')

var vm = new vue({
  el: '#app',
  router,
  store,
  render: h => h(app)
})
