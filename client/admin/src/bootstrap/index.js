window._ = require('lodash')

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

window.$ = global.jQuery = window.jQuery = require('jquery')

require('bootstrap-sass')

_.mixin({ chunkObj (input, size) {
  return _.chain(input).toPairs().chunk(size).map(_.object).value()
}})
