export default {
    props: {
        index: {
            default: ''
        }
    },

    computed: {
        hasIndex () {
            if (this.index.toString().length) {
                return true
            }

            return false
        }
    }
}