import vue from 'vue'
import vuex from 'vuex'
import * as getters from './getters'
import * as mutations from './mutations'
import * as actions from './actions'
import auth from './modules/auth'
import clas from './modules/class'
import createPersistedState from 'vuex-persistedstate'
import createMutationsSharer from 'vuex-shared-mutations'
import faculty from './modules/faculty'
import internship from './modules/internship'
import lecturer from './modules/lecturer'
import logSystem from './modules/logSystem'
import major from './modules/major'
import mediaBrowser from './modules/mediaBrowser'
import menu from './modules/menu'
import setting from './modules/setting'
import student from './modules/student'
import term from './modules/term'
import report from './modules/report'
import user from './modules/user/index'

vue.use(vuex)

const state = {
    appLoading: false,
    activeColumnLeft: false,
    breadcrumbs: [],
    pageTitle: 'Home'
}

export default new vuex.Store({
    state,
    getters,
    mutations,
    actions,
    modules: {
        auth,
        class: clas,
        faculty,
        internship,
        lecturer,
        logSystem,
        major,
        mediaBrowser,
        menu,
        setting,
        student,
        term,
        report,
        user
    },
    plugins: [
        createPersistedState({
            key: '__________'
        }),
        createMutationsSharer({ predicate: [] })
    ],
    strict: true
})
