import internshipApi from '../../api/internship'

const state = {
    data: [],
    queries: {
        perpage: 1,
        page: 1
    },
    detail: {
        id: 0,
        name: '',
        type: '',
        term: '',
        date_open: '0000-00-00',
        date_close: '0000-00-00',
        created_at: '0000-00-00 00:00:00',
        updated_at: '0000-00-00 00:00:00',
        faculties: {
            data: [],
            total: 0,
        },
        majors: {
            data: [],
            total: 0,
        },
        classes: {
            data: [],
            total: 0,
        },
        majors: {
            data: [],
            total: 0,
        },
        lecturers: {
            data: [],
            total: 0,  
        },
        students: {
            data: [],
            total: 0,  
        }
    },
    total: 0,
    totalPage: 0
}

const getters = {
    collections (state) {
        return {
            data: state.data,
            total: state.total,
            perpage: state.queries.perpage,
            page: state.queries.page,
            totalPage: state.totalPage
        }
    },

    detail (state) {
        return state.detail
    },

    isEmpty (state) {
        return state.total === 0
    }
}

const mutations = {
    setState (state, payload) {
        for (let key in payload) {
            state[key] = payload[key]
        }
    },

    setDetail (state, payload) {
        for (let key in payload) {
            state.detail[key] = payload[key]
        }
    }
}

const actions = {
    paginate ({ commit, dispatch }, payload) {
        const data = payload.query
        const options = payload.options ? payload.options : {}
        return internshipApi.paginate({
            data,
            ...options,
            success (res) {
                commit('setState', {
                    data: res.data,
                    total: res.total,
                    totalPage: res.total_page,
                    queries: {
                        perpage: res.perpage,
                        page: res.page
                    }
                })
            }
        })
    },

    get ({ commit, dispatch }, payload) {
        const internshipId = payload.internshipId
        const options = payload.options ? payload.options : {}
        return internshipApi.get(internshipId, {
            ...options,
            success (res) {
                commit('setDetail', {
                    id: res.id,
                    name: res.name,
                    code: res.code,
                    sort_order: res.sort_order,
                    created_at: res.created_at,
                    updated_at: res.updated_at
                })
            }
        })
    },

    delete ({ commit, dispatch }, payload) {
        const internshipId = payload.internshipId
        const options = payload.options ? payload.options : {}
        return internshipApi.delete(internshipId, {
            ...options
        })
    },

    create ({ commit, dispatch }, payload) {
        const data = payload.data
        const options = payload.options ? payload.options : {}
        return internshipApi.create({
            data,
            ...options
        })
    },

    addStudent ({ commit, dispatch }, payload) {
        const data = payload.data
        const internshipId = payload.internshipId
        const options = payload.options ? payload.options : {}
        return internshipApi.addStudent(internshipId, {
            data,
            ...options
        })
    },

    existsStudent ({ commit, dispatch }, payload) {
        const data = payload.data
        const internshipId = payload.internshipId
        const studentId = payload.studentId
        const options = payload.options ? payload.options : {}
        return internshipApi.existsStudent(internshipId, studentId, {
            data,
            ...options
        })
    },

    getStudent ({ commit, dispatch }, payload) {
        const data = payload.data
        const internshipId = payload.internshipId
        const studentId = payload.studentId
        const options = payload.options ? payload.options : {}
        return internshipApi.getStudent(internshipId, studentId, {
            data,
            ...options
        })
    },

    updateStudent ({ commit, dispatch }, payload) {
        const data = payload.data
        const internshipId = payload.internshipId
        const studentId = payload.studentId
        const options = payload.options ? payload.options : {}
        return internshipApi.updateStudent(internshipId, studentId, {
            data,
            ...options
        })
    },

    fullStudent ({ commit, dispatch }, payload) {
        const data = payload.data
        const internshipId = payload.internshipId
        const lecturerId = payload.lecturerId
        const options = payload.options ? payload.options : {}
        return internshipApi.fullStudent(internshipId, lecturerId, {
            data,
            ...options
        })
    },

    update ({ commit, dispatch }, payload) {
        const internshipId = payload.internshipId
        const data = payload.data
        const options = payload.options ? payload.options : {}
        return internshipApi.update(internshipId, {
            data,
            ...options,
            success (res) {
                const internship = res.internship
                commit('setDetail', {
                    id: internship.id,
                    name: internship.name,
                    code: internship.code,
                    sort_order: internship.sort_order,
                    created_at: internship.created_at,
                    updated_at: internship.updated_at
                })
            }
        })
    }
}

export default {
    state,
    getters,
    mutations,
    actions,
    namespaced: true
}
