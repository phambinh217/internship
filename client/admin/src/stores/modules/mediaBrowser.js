import broserApi from '../../api/mediaBrowser'
import fileApi from '../../api/file'

const state = {
    data: [],
    queries: {
        perpage: 1,
        page: 1
    },
    detail: {
        id: 0,
        name: 'Root category',
        path: '',
        pri_index: 0,
        size: '',
        type: 'folder',
        date_create: '0000-00-00 00:00:00'
    },
    total: 0,
    totalPage: 0
}

const getters = {
    collections (state) {
        return {
            data: state.data,
            total: state.total,
            perpage: state.queries.perpage,
            page: state.queries.page,
            totalPage: state.totalPage
        }
    },

    detail (state) {
        return state.detail
    },

    isEmpty (state) {
        return state.total === 0
    }
}

const mutations = {
    setState (state, payload) {
        // state = _.cloneDeep(payload)
        for (let key in payload) {
            state[key] = payload[key]
        }
    },

    setDetail (state, payload) {
        // state.detail = _.cloneDeep(payload)
        for (let key in payload) {
            state.detail[key] = payload[key]
        }
    }
}

const actions = {
    paginate ({ commit, dispatch }, payload) {
        const data = payload.query
        const options = payload.options ? payload.options : {}
        return broserApi.paginate({
            data,
            ...options,
            success (res) {
                commit('setState', {
                    data: res.data,
                    total: res.total,
                    totalPage: res.total_page,
                    queries: {
                        perpage: res.perpage,
                        page: res.page
                    }
                })
            }
        })
    },

    createDir ({ commit }, payload) {
        const data = payload.data
        const options = payload.options ? payload.options : {}
        return fileApi.directory.create({
            data,
            ...options
        })
    },

    updateDir ({ commit, dispatch }, payload) {
        const directoryId = payload.directoryId
        const data = payload.data
        const options = payload.options ? payload.options : {}
        return fileApi.directory.update(directoryId, {
            data,
            ...options,
            success (res) {
                const directory = res.directory
                commit('setDetail', {
                    id: directory.id,
                    name: directory.name,
                    path: '',
                    size: '',
                    type: 'folder',
                    pri_index: 0,
                    date_create: directory.created_at
                })
            }
        })
    },

    removeDir ({ commit }, payload) {
        const directoryId = payload.directoryId
        const options = payload.options ? payload.options : {}
        return fileApi.directory.delete(directoryId, {
            ...options
        })
    },

    removeFile ({ commit }, payload) {
        const fileId = payload.fileId
        const options = payload.options ? payload.options : {}
        return fileApi.file.delete(fileId, {
            ...options
        })
    },

    upload ({ commit }, payload) {
        const directoryId = payload.directoryId
        const data = payload.data
        const options = payload.options ? payload.options : {}
        return fileApi.directory.upload(directoryId, {
            data,
            ...options
        })
    },

    setState ({ commit }, payload) {
        commit('setState', payload)
    }
}

export default {
    state,
    getters,
    mutations,
    actions,
    namespaced: true
}
