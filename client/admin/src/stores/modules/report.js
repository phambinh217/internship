import reportApi from '../../api/report'

const state = {
  dashboard: {
    total_users: 0,
    total_roles: 0,
    total_logs: 0,
    total_users_online: 0,
    top_ten_users: 0,
    roles: [],
    top_logs: [],
  },
}

const getters = {
  dashboard (state) {
    return state.dashboard
  },
}

const mutations = {
  setDashboard (state, payload) {
    // state.dashboard = _.cloneDeep(payload)
    for (let key in payload) {
      state.dashboard[key] = payload[key]
    }
  },
}

const actions = {
  getDashboard ({ commit }) {
    return reportApi.dashboard({
      success (res) {
        commit('setDashboard', res)
      }
    })
  },
}

export default {
  state,
  getters,
  mutations,
  actions,
  namespaced: true
}
