import facultyApi from '../../api/faculty'

const state = {
    data: [],
    queries: {
        perpage: 1,
        page: 1
    },
    detail: {
        id: 0,
        name: '',
        code: '',
        sort_order: '',
        created_at: '0000-00-00 00:00:00',
        updated_at: '0000-00-00 00:00:00'
    },
    total: 0,
    totalPage: 0
}

const getters = {
    collections (state) {
        return {
            data: state.data,
            total: state.total,
            perpage: state.queries.perpage,
            page: state.queries.page,
            totalPage: state.totalPage
        }
    },

    detail (state) {
        return state.detail
    },

    isEmpty (state) {
        return state.total === 0
    }
}

const mutations = {
    setState (state, payload) {
        for (let key in payload) {
            state[key] = payload[key]
        }
    },

    setDetail (state, payload) {
        for (let key in payload) {
            state.detail[key] = payload[key]
        }
    }
}

const actions = {
    paginate ({ commit, dispatch }, payload) {
        const data = payload.query
        const options = payload.options ? payload.options : {}
        return facultyApi.paginate({
            data,
            ...options,
            success (res) {
                commit('setState', {
                    data: res.data,
                    total: res.total,
                    totalPage: res.total_page,
                    queries: {
                        perpage: res.perpage,
                        page: res.page
                    }
                })
            }
        })
    },

    get ({ commit, dispatch }, payload) {
        const facultyId = payload.facultyId
        const options = payload.options ? payload.options : {}
        return facultyApi.get(facultyId, {
            ...options,
            success (res) {
                commit('setDetail', {
                    id: res.id,
                    name: res.name,
                    code: res.code,
                    sort_order: res.sort_order,
                    created_at: res.created_at,
                    updated_at: res.updated_at
                })
            }
        })
    },

    delete ({ commit, dispatch }, payload) {
        const facultyId = payload.facultyId
        const options = payload.options ? payload.options : {}
        return facultyApi.delete(facultyId, {
            ...options
        })
    },

    create ({ commit, dispatch }, payload) {
        const data = payload.data
        const options = payload.options ? payload.options : {}
        return facultyApi.create({
            data,
            ...options
        })
    },

    update ({ commit, dispatch }, payload) {
        const facultyId = payload.facultyId
        const data = payload.data
        const options = payload.options ? payload.options : {}
        return facultyApi.update(facultyId, {
            data,
            ...options,
            success (res) {
                const faculty = res.faculty
                commit('setDetail', {
                    id: faculty.id,
                    name: faculty.name,
                    code: faculty.code,
                    sort_order: faculty.sort_order,
                    created_at: faculty.created_at,
                    updated_at: faculty.updated_at
                })
            }
        })
    },

    sortOrder ({ commit, dispatch }, payload) {
        const data = payload.data
        const options = payload.options ? payload.options : {}

        return facultyApi.sortOrder({
          data,
          ...options
      })
    }
}

export default {
    state,
    getters,
    mutations,
    actions,
    namespaced: true
}
