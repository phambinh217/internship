const state = {
    active: 'home',
    data: [
        {
            name: 'home',
            icon: 'storage/icon/home.png',
            label: 'Home page',
            route: 'home',
            gate: 'home'
        },
        {
            name: 'internship',
            icon: 'storage/icon/bill.png',
            label: 'Internships',
            route: 'internship.index',
            gate: 'internship.read'
        },
        {
            name: 'student',
            icon: 'storage/icon/student.png',
            label: 'Students',
            route: 'student.index',
            gate: 'student.read'
        },
        {
            name: 'lecturer',
            icon: 'storage/icon/lecturer.png',
            label: 'Lecturers',
            route: 'lecturer.index',
            gate: 'lecturer.read'
        },
        {
            name: 'school',
            icon: 'storage/icon/ictu.png',
            label: 'Ictu school',
            route: 'school.index',
            gate: 'school.read'
        },
        {
            name: 'user',
            icon: 'storage/icon/users.png',
            label: 'Users',
            route: 'user.index',
            gate: 'user.read'
        },
        {
            name: 'setting',
            icon: 'storage/icon/setting.png',
            label: 'Settings',
            route: 'setting.index',
            gate: 'setting'
        },
        {
            name: 'log-system',
            icon: 'storage/icon/log.png',
            label: 'Logs system',
            route: 'log-system.index',
            gate: 'log-system.read'
        }
    ],
    total: 4
}

const getters = {
    collections (state) {
        return state.data
    },

    active (state) {
        return state.active
    }
}

const mutations = {
    setActive (state, menu) {
        state.active = menu
    }
}

const actions = {
    setActive ({ commit }, menu) {
        commit('setActive', menu)
    }
}

export default {
    state,
    getters,
    mutations,
    actions,
    namespaced: true
}
