import studentApi from '../../api/student'

const state = {
    data: [],
    queries: {
        perpage: 1,
        code: '',
        page: 1
    },
    detail: {
        id: 0,
        code: '',
        phone: '',
        email: '',
        avatar: '',
        firstname: '',
        lastname: '',
        class_id: '',
        major_id: '',
        faculty_id: '',
        term: '',
        birth: '0000-00-00',
        created_at: '0000-00-00 00:00:00',
        updated_at: '0000-00-00 00:00:00'
    },
    total: 0,
    totalPage: 0
}

const getters = {
    collections (state) {
        return {
            data: state.data,
            total: state.total,
            perpage: state.queries.perpage,
            page: state.queries.page,
            totalPage: state.totalPage
        }
    },

    detail (state) {
        return state.detail
    },

    isEmpty (state) {
        return state.total === 0
    }
}

const mutations = {
    setState (state, payload) {
        for (let key in payload) {
            state[key] = payload[key]
        }
    },

    setDetail (state, payload) {
        for (let key in payload) {
            state.detail[key] = payload[key]
        }
    }
}

const actions = {
    paginate ({ commit, dispatch }, payload) {
        const data = payload.query
        const options = payload.options ? payload.options : {}
        return studentApi.paginate({
            data,
            ...options,
            success (res) {
                commit('setState', {
                    data: res.data,
                    total: res.total,
                    totalPage: res.total_page,
                    queries: {
                        perpage: res.perpage,
                        page: res.page
                    }
                })
            }
        })
    },

    get ({ commit, dispatch }, payload) {
        const studentId = payload.studentId
        const options = payload.options ? payload.options : {}
        return studentApi.get(studentId, {
            ...options,
            success (res) {
                commit('setDetail', {
                    id: res.id,
                    code: res.code,
                    phone: res.phone,
                    email: res.email,
                    avatar: res.avatar,
                    firstname: res.firstname,
                    lastname: res.lastname,
                    class_id: res.class_id,
                    major_id: res.major_id,
                    faculty_id: res.faculty_id,
                    term: res.term,
                    birth: res.birth,
                    created_at: res.created_at,
                    updated_at: res.updated_at
                })
            }
        })
    },

    delete ({ commit, dispatch }, payload) {
        const studentId = payload.studentId
        const options = payload.options ? payload.options : {}
        return studentApi.delete(studentId, {
            ...options
        })
    },

    create ({ commit, dispatch }, payload) {
        const data = payload.data
        const options = payload.options ? payload.options : {}
        return studentApi.create({
            data,
            ...options
        })
    },

    update ({ commit, dispatch }, payload) {
        const studentId = payload.studentId
        const data = payload.data
        const options = payload.options ? payload.options : {}
        return studentApi.update(studentId, {
            data,
            ...options,
            success (res) {
                const student = res.student
                commit('setDetail', {
                    id: student.id,
                    code: student.code,
                    phone: student.phone,
                    email: student.email,
                    avatar: student.avatar,
                    firstname: student.firstname,
                    lastname: student.lastname,
                    class_id: student.class_id,
                    major_id: student.major_id,
                    faculty_id: student.faculty_id,
                    term: student.term,
                    birth: student.birth,
                    created_at: student.created_at,
                    updated_at: student.updated_at
                })
            }
        })
    }
}

export default {
    state,
    getters,
    mutations,
    actions,
    namespaced: true
}
