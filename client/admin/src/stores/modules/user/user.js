import userApi from '../../../api/user'

const state = {
  data: [],
  queries: {
    perpage: 1,

    page: 1
  },
  detail: {
    id: 0,
    lastname: '',
    firstname: '',
    role_id: '',
    birth: '',
    email: '',
    phone: '',
    avatar: '',
    send_email_new_login: '',
    last_online_at: '',
    created_at: '0000-00-00 00:00:00',
    updated_at: '0000-00-00 00:00:00'
  },
  total: 0,
  totalPage: 0
}

const getters = {
  collections (state) {
    return {
      data: state.data,
      total: state.total,
      perpage: state.queries.perpage,
      page: state.queries.page,
      totalPage: state.totalPage
    }
  },

  detail (state) {
    return state.detail
  },

  isEmpty (state) {
    return state.total === 0
  }
}

const mutations = {
  setState (state, payload) {
    // state = _.cloneDeep(payload)
    for (let key in payload) {
      state[key] = payload[key]
    }
  },

  setDetail (state, payload) {
    // state.detail = _.cloneDeep(payload)
    for (let key in payload) {
      state.detail[key] = payload[key]
    }
  }
}

const actions = {
  paginate ({ commit, dispatch }, payload) {
    const data = payload.query
    const options = payload.options ? payload.options : {}
    return userApi.user.paginate({
      data,
      ...options,
      success (res) {
        commit('setState', {
          data: res.data,
          total: res.total,
          totalPage: res.total_page,
          queries: {
            perpage: res.perpage,
            page: res.page
          }
        })
      }
    })
  },

  get ({ commit, dispatch }, payload) {
    const userId = payload.userId
    const options = payload.options ? payload.options : {}
    return userApi.user.get(userId, {
      ...options,
      success (res) {
        commit('setDetail', {
          id: res.id,
          lastname: res.lastname,
          firstname: res.firstname,
          role_id: res.role_id,
          birth: res.birth,
          email: res.email,
          phone: res.phone,
          avatar: res.avatar,
          send_email_new_login: res.send_email_new_login,
          last_online_at: res.last_online_at,
          created_at: res.created_at,
          updated_at: res.updated_at
        })
      }
    })
  },

  delete ({ commit, dispatch }, payload) {
    const userId = payload.userId
    const options = payload.options ? payload.options : {}
    return userApi.user.delete(userId, {
      ...options
    })
  },

  create ({ commit, dispatch }, payload) {
    const data = payload.data
    const options = payload.options ? payload.options : {}
    return userApi.user.create({
      data,
      ...options
    })
  },

  update ({ commit, dispatch }, payload) {
    const userId = payload.userId
    const data = payload.data
    const options = payload.options ? payload.options : {}
    return userApi.user.update(userId, {
      data,
      ...options,
      success (res) {
        const user = res.user
        commit('setDetail', {
          id: user.id,
          lastname: user.lastname,
          firstname: user.firstname,
          role_id: user.role_id,
          birth: user.birth,
          email: user.email,
          phone: user.phone,
          avatar: user.avatar,
          send_email_new_login: user.send_email_new_login,
          last_online_at: user.last_online_at,
          created_at: user.created_at,
          updated_at: user.updated_at
        })
      }
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions,
  namespaced: true
}
