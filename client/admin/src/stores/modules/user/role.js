import userApi from '../../../api/user'

const state = {
  data: [],
  queries: {
    perpage: 1,

    page: 1
  },
  detail: {
    id: 0,
    name: '',
    permission: ''
  },
  total: 0,
  totalPage: 0
}

const getters = {
  collections (state) {
    return {
      data: state.data,
      total: state.total,
      perpage: state.queries.perpage,
      page: state.queries.page,
      totalPage: state.totalPage
    }
  },

  detail (state) {
    return state.detail
  },

  isEmpty (state) {
    return state.total === 0
  }
}

const mutations = {
  setState (state, payload) {
    // state = _.cloneDeep(payload)
    for (var key in payload) {
      state[key] = payload[key]
    }
  },

  setDetail (state, payload) {
    // state.detail = _.cloneDeep(payload)
    for (var key in payload) {
      state.detail[key] = payload[key]
    }
  }
}

const actions = {
  paginate ({ commit, dispatch }, payload) {
    const data = payload.query
    const options = payload.options ? payload.options : {}
    return userApi.role.paginate({
      data,
      ...options,
      success (res) {
        commit('setState', {
          data: res.data,
          total: res.total,
          totalPage: res.total_page,
          queries: {
            perpage: res.perpage,
            page: res.page
          }
        })
      }
    })
  },

  get ({ commit, dispatch }, payload) {
    const roleId = payload.roleId
    const options = payload.options ? payload.options : {}
    return userApi.role.get(roleId, {
      ...options,
      success (res) {
        commit('setDetail', {
          id: res.id,
          name: res.name,
          permission: res.permission
        })
      }
    })
  },

  delete ({ commit, dispatch }, payload) {
    const roleId = payload.roleId
    const options = payload.options ? payload.options : {}

    return userApi.role.delete(roleId, {
      ...options
    })
  },

  create ({ commit, dispatch }, payload) {
    const data = payload.data
    const options = payload.options ? payload.options : {}

    return userApi.role.create({
      data,
      ...options
    })
  },

  update ({ commit, dispatch }, payload) {
    const roleId = payload.roleId
    const data = payload.data
    const options = payload.options ? payload.options : {}
    return userApi.role.update(roleId, {
      data,
      ...options,

      success (res) {
        const role = res.role
        commit('setDetail', {
          id: role.id,
          name: role.name,
          permission: role.permission
        })
      }
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions,
  namespaced: true
}
