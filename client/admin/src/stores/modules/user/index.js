import role from './role'
import user from './user'
import permission from './permission'

export default {
  modules: {
    role,
    user,
    permission
  },
  namespaced: true
}
