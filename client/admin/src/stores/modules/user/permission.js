import userApi from '../../../api/user'

const state = {
  data: [],
  total: 0
}

const getters = {
  collections (state) {
    return {
      data: state.data,
      total: state.total
    }
  },

  isEmpty (state) {
    return state.total === 0
  }
}

const mutations = {
  setState (state, payload) {
    // state = _.cloneDeep(payload)
    for (var key in payload) {
      state[key] = payload[key]
    }
  }
}

const actions = {
  all ({ commit, dispatch }, payload) {
    const options = payload.options ? payload.options : {}
    return userApi.permission.all({
      ...options,
      success (res) {
        commit('setState', {
          data: res.data,
          total: res.total
        })
      }
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions,
  namespaced: true
}
