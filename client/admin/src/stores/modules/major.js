import majorApi from '../../api/major'

const state = {
    data: [],
    queries: {
        perpage: 1,
        page: 1
    },
    detail: {
        id: 0,
        name: '',
        code: '',
        faculty_id: '',
        sort_order: '',
        created_at: '0000-00-00 00:00:00',
        updated_at: '0000-00-00 00:00:00'
    },
    total: 0,
    totalPage: 0
}

const getters = {
    collections (state) {
        return {
            data: state.data,
            total: state.total,
            perpage: state.queries.perpage,
            page: state.queries.page,
            totalPage: state.totalPage
        }
    },

    detail (state) {
        return state.detail
    },

    isEmpty (state) {
        return state.total === 0
    }
}

const mutations = {
    setState (state, payload) {
        for (let key in payload) {
            state[key] = payload[key]
        }
    },

    setDetail (state, payload) {
        for (let key in payload) {
            state.detail[key] = payload[key]
        }
    }
}

const actions = {
    paginate ({ commit, dispatch }, payload) {
        const data = payload.query
        const options = payload.options ? payload.options : {}
        return majorApi.paginate({
            data,
            ...options,
            success (res) {
                commit('setState', {
                    data: res.data,
                    total: res.total,
                    totalPage: res.total_page,
                    queries: {
                        perpage: res.perpage,
                        page: res.page
                    }
                })
            }
        })
    },

    get ({ commit, dispatch }, payload) {
        const majorId = payload.majorId
        const options = payload.options ? payload.options : {}
        return majorApi.get(majorId, {
            ...options,
            success (res) {
                commit('setDetail', {
                    id: res.id,
                    name: res.name,
                    code: res.code,
                    faculty_id: res.faculty_id,
                    sort_order: res.sort_order,
                    created_at: res.created_at,
                    updated_at: res.updated_at
                })
            }
        })
    },

    delete ({ commit, dispatch }, payload) {
        const majorId = payload.majorId
        const options = payload.options ? payload.options : {}
        return majorApi.delete(majorId, {
            ...options
        })
    },

    create ({ commit, dispatch }, payload) {
        const data = payload.data
        const options = payload.options ? payload.options : {}
        return majorApi.create({
            data,
            ...options
        })
    },

    update ({ commit, dispatch }, payload) {
        const majorId = payload.majorId
        const data = payload.data
        const options = payload.options ? payload.options : {}
        return majorApi.update(majorId, {
            data,
            ...options,
            success (res) {
                const major = res.major
                commit('setDetail', {
                    id: major.id,
                    name: major.name,
                    code: major.code,
                    faculty_id: major.faculty_id,
                    sort_order: major.sort_order,
                    created_at: major.created_at,
                    updated_at: major.updated_at
                })
            }
        })
    },

    sortOrder ({ commit, dispatch }, payload) {
        const data = payload.data
        const options = payload.options ? payload.options : {}

        return majorApi.sortOrder({
          data,
          ...options
      })
    }
}

export default {
    state,
    getters,
    mutations,
    actions,
    namespaced: true
}
