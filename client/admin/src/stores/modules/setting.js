import configApi from '../../api/config'

const state = {
    'logo': '',
    'register': 0,
    'default_user_role_id': 0
}

const getters = {
}

const mutations = {
    setState (state, payload) {
        // state = _.cloneDeep(payload)
        for (let key in payload) {
            state[key] = payload[key]
        }
    }
}

const actions = {
    all ({ commit }) {
        return configApi.namespace('setting', {
            success (res) {
                commit('setState', res.data)
            }
        })
    },

    save ({ commit }, payload) {
        const setting = payload.data
        const data = {config: { setting }}
        const options = payload.options ? payload.options : {}
        return configApi.save({
            data,
            ...options,
            success (res) {
                commit('setState', res.data.setting)
            }
        })
    },
}

export default {
    state,
    getters,
    mutations,
    actions,
    namespaced: true
}
