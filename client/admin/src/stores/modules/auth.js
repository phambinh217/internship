import authApi from '../../api/auth'
import meApi from '../../api/me'

const state = {
    check: false,
    user: {
        id: '',
        firstname: '',
        lastname: '',
        birth: '',
        email: '',
        phone: '',
        role_id: '',
        avatar: '',
        send_email_new_login: '',
        created_at: '',
        updated_at: '',
        is_online: ''
    },
    token: '',
    permissions: {},
}

const getters = {
    check (state) {
        return state.check
    },

    user (state) {
        return state.user
    },

    permissions (state) {
        return state.permissions
    }
}

const mutations = {
    setSate (state, payload) {
        // state = _.cloneDeep(payload)
        for (let key in payload) {
            state[key] = payload[key]
        }
    },

    setToken (state, token) {
        state.token = token
    },

    setCheck (state, status) {
        state.check = status
    },

    setUser (state, payload) {
        // state.user = _.cloneDeep(payload)
        for (let key in payload) {
            state.user[key] = payload[key]
        }
    },

    setPermission (state, payload) {
        if (typeof payload === 'object') {
            state.permissions = _.cloneDeep(payload)
        } else {
            state.permissions = payload
        }
    },
}

const actions = {
    login ({ commit, dispatch }, payload) {
        const data = payload.data
        const redirecTo = payload.redirecTo

        return authApi.login({
            data,
            success (res) {
                commit('setToken', res.token)
                commit('setCheck', true)
            }
        })
    },

    getMe({ commit }) {
        return meApi.get({
            success (res) {
                commit('setUser', res.me)
            }
        })
    },

    getPermission({ commit }) {
        return meApi.permission({
            success (res) {
                commit('setPermission', res.permission)
            }
        })
    },

    forgotPassword ({ commit }, payload) {
        const data = payload.data
        return authApi.forgotPassword({
            data
        })
    },

    register ({ commit }, payload) {
        const data = payload.data
        return authApi.register({
            data
        })
    },

    logout ({ commit }) {
        commit('setSate', {
            check: false,
            user: {
                id: '',
                firstname: '',
                lastname: '',
                birth: '',
                email: '',
                phone: '',
                role_id: '',
                avatar: '',
                send_email_new_login: '',
                created_at: '',
                updated_at: '',
                is_online: ''
            },
            token: '',
            permissions: [],
        })
    },

    update ({ commit, dispatch }, payload) {
        const data = payload.data
        const options = payload.options ? payload.options : {}

        return meApi.update({
            data,
            ...options,
            success (res) {
                commit('setUser', res.me)
            }
        })
    },

    updatePassword ({ commit, dispatch }, payload) {
        const data = payload.data
        const options = payload.options ? payload.options : {}

        return meApi.updatePassword({
            data,
            ...options
        })
    }
}

export default {
    state,
    getters,
    mutations,
    actions,
    namespaced: true
}
