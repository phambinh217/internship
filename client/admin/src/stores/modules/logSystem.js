import logSystemApi from '../../api/logSystem'

const state = {
    data: [],
    active: '',
    total: 0,
    detail: {
        total: 0,
        log: []
    },
}

const getters = {
    collections (state) {
        return {
            data: state.data,
            total: state.total
        }
    },

    detail (state) {
        return state.detail
    },

    isEmpty (state) {
        return state.total === 0
    }
}

const mutations = {
    setState (state, payload) {
        // state = _.cloneDeep(payload)
        for (let key in payload) {
            state[key] = payload[key]
        }
    },

    setDetail (state, payload) {
        // state.detail = _.cloneDeep(payload)
        for (let key in payload) {
            state.detail[key] = payload[key]
        }
    }
}

const actions = {
    all ({ commit }) {
        return logSystemApi.all({
            success (res) {
                commit('setState', res)
            }
        })
    },

    get ({ commit }, date) {
        return logSystemApi.get(date, {
            success (res) {
                commit('setDetail', res)
            }
        })
    },

    delete ({ commit }, payload) {
        const data = payload.data
        const options = payload.options ? payload.options : {}
        return logSystemApi.delete({
            data,
            ...options
        })
    },
}

export default {
    state,
    getters,
    mutations,
    actions,
    namespaced: true
}
