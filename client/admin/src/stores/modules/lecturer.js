import lecturerApi from '../../api/lecturer'

const state = {
    data: [],
    queries: {
        perpage: 1,
        code: '',
        page: 1
    },
    detail: {
        id: 0,
        code: '',
        firstname: '',
        lastname: '',
        phone: '',
        email: '',
        faculty_id: '',
        forte: '',
        birth: '',
        created_at: '0000-00-00 00:00:00',
        updated_at: '0000-00-00 00:00:00'
    },
    total: 0,
    totalPage: 0
}

const getters = {
    collections (state) {
        return {
            data: state.data,
            total: state.total,
            perpage: state.queries.perpage,
            page: state.queries.page,
            totalPage: state.totalPage
        }
    },

    detail (state) {
        return state.detail
    },

    isEmpty (state) {
        return state.total === 0
    }
}

const mutations = {
    setState (state, payload) {
        for (let key in payload) {
            state[key] = payload[key]
        }
    },

    setDetail (state, payload) {
        for (let key in payload) {
            state.detail[key] = payload[key]
        }
    }
}

const actions = {
    paginate ({ commit, dispatch }, payload) {
        const data = payload.query
        const options = payload.options ? payload.options : {}
        return lecturerApi.paginate({
            data,
            ...options,
            success (res) {
                commit('setState', {
                    data: res.data,
                    total: res.total,
                    totalPage: res.total_page,
                    queries: {
                        perpage: res.perpage,
                        page: res.page
                    }
                })
            }
        })
    },

    get ({ commit, dispatch }, payload) {
        const lecturerId = payload.lecturerId
        const options = payload.options ? payload.options : {}
        return lecturerApi.get(lecturerId, {
            ...options,
            success (res) {
                commit('setDetail', {
                    id: res.id,
                    code: res.code,
                    phone: res.phone,
                    email: '',
                    firstname: res.firstname,
                    lastname: res.lastname,
                    faculty_id: res.faculty_id,
                    birth: res.birth,
                    forte: res.forte,
                    created_at: res.created_at,
                    updated_at: res.updated_at
                })
            }
        })
    },

    delete ({ commit, dispatch }, payload) {
        const lecturerId = payload.lecturerId
        const options = payload.options ? payload.options : {}
        return lecturerApi.delete(lecturerId, {
            ...options
        })
    },

    create ({ commit, dispatch }, payload) {
        const data = payload.data
        const options = payload.options ? payload.options : {}
        return lecturerApi.create({
            data,
            ...options
        })
    },

    update ({ commit, dispatch }, payload) {
        const lecturerId = payload.lecturerId
        const data = payload.data
        const options = payload.options ? payload.options : {}
        return lecturerApi.update(lecturerId, {
            data,
            ...options,
            success (res) {
                const lecturer = res.lecturer
                commit('setDetail', {
                    id: lecturer.id,
                    code: lecturer.code,
                    phone: lecturer.phone,
                    email: '',
                    firstname: lecturer.firstname,
                    lastname: lecturer.lastname,
                    faculty_id: lecturer.faculty_id,
                    birth: lecturer.birth,
                    forte: lecturer.forte,
                    created_at: lecturer.created_at,
                    updated_at: lecturer.updated_at
                })
            }
        })
    }
}

export default {
    state,
    getters,
    mutations,
    actions,
    namespaced: true
}
