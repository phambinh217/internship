import termApi from '../../api/term'

const state = {
    data: [],
    queries: {
        perpage: 1,
        page: 1
    },
    detail: {
        term: 0,
    },
    total: 0,
    totalPage: 0
}

const getters = {
    collections (state) {
        return {
            data: state.data,
            total: state.total,
            perpage: state.queries.perpage,
            page: state.queries.page,
            totalPage: state.totalPage
        }
    },

    detail (state) {
        return state.detail
    },

    isEmpty (state) {
        return state.total === 0
    }
}

const mutations = {
    setState (state, payload) {
        for (let key in payload) {
            state[key] = payload[key]
        }
    },

    setDetail (state, payload) {
        for (let key in payload) {
            state.detail[key] = payload[key]
        }
    }
}

const actions = {
    paginate ({ commit, dispatch }, payload) {
        const data = payload.query
        const options = payload.options ? payload.options : {}
        return termApi.paginate({
            data,
            ...options,
            success (res) {
                commit('setState', {
                    data: res.data,
                    total: res.total,
                    totalPage: res.total_page,
                    queries: {
                        perpage: res.perpage,
                        page: res.page
                    }
                })
            }
        })
    },

    get ({ commit, dispatch }, payload) {
        const term = payload.term
        const options = payload.options ? payload.options : {}
        return termApi.get(term, {
            ...options,
            success (res) {
                commit('setDetail', {
                    term: res.term
                })
            }
        })
    },

    delete ({ commit, dispatch }, payload) {
        const term = payload.term
        const options = payload.options ? payload.options : {}
        return termApi.delete(term, {
            ...options
        })
    },

    create ({ commit, dispatch }, payload) {
        const data = payload.data
        const options = payload.options ? payload.options : {}
        return termApi.create({
            data,
            ...options
        })
    },

    update ({ commit, dispatch }, payload) {
        const term = payload.term
        const data = payload.data
        const options = payload.options ? payload.options : {}
        return termApi.update(term, {
            data,
            ...options,
            success (res) {
                const term = res.term
                commit('setDetail', {
                    term: term.term,
                })
            }
        })
    }
}

export default {
    state,
    getters,
    mutations,
    actions,
    namespaced: true
}
