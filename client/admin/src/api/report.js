import { urlApi } from '../helpers/url'
import store from '../stores'

export default {
    token () {
        return store.state.auth.token
    },

    dashboard (options) {
        options = {
            url: urlApi('reports/dashboard', { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    }
}
