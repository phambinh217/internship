import { urlApi } from '../helpers/url'
import store from '../stores'

export default {
    token () {
        return store.state.auth.token
    },

    paginate (options) {
        options = {
            url: urlApi('lecturers', { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    },

    get (lecturerId, options) {
        options = {
            url: urlApi('lecturers/' + lecturerId, { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    },

    create (options) {
        options = {
            url: urlApi('lecturers/store', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    update (lecturerId, options) {
        options = {
            url: urlApi('lecturers/' + lecturerId + '/update', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    delete (lecturerId, options) {
        options = {
            url: urlApi('lecturers/' + lecturerId + '/delete', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    }
}
