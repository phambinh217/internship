import { urlApi } from '../helpers/url'
import store from '../stores'

export default {
    token () {
        return store.state.auth.token
    },

    paginate (options) {
        options = {
            url: urlApi('files/browser', { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    }
}
