import { urlApi } from '../helpers/url'

export default {
    login (options) {
        options = {
            url: urlApi('login'),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    forgotPassword (options) {
        options = {
            url: urlApi('forgot-password'),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    resetPassword (options) {
        options = {
            url: urlApi('reset-password'),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    register (options) {
        options = {
            url: urlApi('register'),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    }
}
