import { urlApi } from '../helpers/url'
import store from '../stores'

export default {
    token () {
        return store.state.auth.token
    },

    paginate (options) {
        options = {
            url: urlApi('classes', { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    },

    get (classId, options) {
        options = {
            url: urlApi('classes/' + classId, { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    },

    create (options) {
        options = {
            url: urlApi('classes/store', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    update (classId, options) {
        options = {
            url: urlApi('classes/' + classId + '/update', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    delete (classId, options) {
        options = {
            url: urlApi('classes/' + classId + '/delete', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    sortOrder (options) {
        options = {
            url: urlApi('classes/sort-order', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    }
}
