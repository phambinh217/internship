import user from './user'
import role from './role'
import permission from './permission'

export default {
    user,
    role,
    permission
}
