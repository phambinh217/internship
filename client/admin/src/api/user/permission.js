import { urlApi } from '../../helpers/url'
import store from '../../stores'

export default {
    token () {
        return store.state.auth.token
    },

    all (options) {
        options = {
            url: urlApi('permissions', { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    }
}
