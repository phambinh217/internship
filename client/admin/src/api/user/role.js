import { urlApi } from '../../helpers/url'
import store from '../../stores'

export default {
    token () {
        return store.state.auth.token
    },

    paginate (options) {
        options = {
            url: urlApi('roles', { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    },

    get (roleId, options) {
        options = {
            url: urlApi('roles/' + roleId, { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    },

    create (options) {
        options = {
            url: urlApi('roles/store', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    update (roleId, options) {
        options = {
            url: urlApi('roles/' + roleId + '/update', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    delete (roleId, options) {
        options = {
            url: urlApi('roles/' + roleId + '/delete', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    }
}
