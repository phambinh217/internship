import { urlApi } from '../helpers/url'
import store from '../stores'

export default {
    token () {
        return store.state.auth.token
    },

    paginate (options) {
        options = {
            url: urlApi('majors', { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    },

    get (majorId, options) {
        options = {
            url: urlApi('majors/' + majorId, { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    },

    create (options) {
        options = {
            url: urlApi('majors/store', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    update (majorId, options) {
        options = {
            url: urlApi('majors/' + majorId + '/update', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    delete (majorId, options) {
        options = {
            url: urlApi('majors/' + majorId + '/delete', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    sortOrder (options) {
        options = {
            url: urlApi('majors/sort-order', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    }
}
