import { urlApi } from '../../helpers/url'
import store from '../../stores'

export default {
    token () {
        return store.state.auth.token
    },

    delete (fileId, options) {
        options = {
            url: urlApi('files/' + fileId + '/delete', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    }
}
