import directory from './directory'
import file from './file'

export default {
    directory,
    file
}
