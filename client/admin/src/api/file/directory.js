import { urlApi } from '../../helpers/url'
import store from '../../stores'

export default {
    token () {
        return store.state.auth.token
    },

    get (directoryId, options) {
        options = {
            url: urlApi('files/directories/' + directoryId, { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    },

    create (options) {
        options = {
            url: urlApi('files/directories/store', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    update (directoryId, options) {
        options = {
            url: urlApi('files/directories/' + directoryId + '/update', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    delete (directoryId, options) {
        options = {
            url: urlApi('files/directories/' + directoryId + '/delete', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    upload (directoryId, options) {
        options = {
            url: urlApi('files/directories/' + directoryId + '/upload', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    }
}
