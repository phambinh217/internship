import { urlApi } from '../helpers/url'
import store from '../stores'

export default {
    token () {
        return store.state.auth.token
    },

    paginate (options) {
        options = {
            url: urlApi('internships', { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    },

    get (internshipId, options) {
        options = {
            url: urlApi('internships/' + internshipId, { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    },

    statistic (internshipId, options) {
        options = {
            url: urlApi('internships/' + internshipId + '/statistic', { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    },

    create (options) {
        options = {
            url: urlApi('internships/store', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    update (internshipId, options) {
        options = {
            url: urlApi('internships/' + internshipId + '/update', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    delete (internshipId, options) {
        options = {
            url: urlApi('internships/' + internshipId + '/delete', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    getStudents (internshipId, options) {
        options = {
            url: urlApi('internships/' + internshipId + '/students', { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    },

    addStudent (internshipId, options) {
        options = {
            url: urlApi('internships/' + internshipId + '/students/store', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    getStudent (internshipId, studentId, options) {
        options = {
            url: urlApi('internships/' + internshipId + '/students/' + studentId, { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    },

    updateStudent (internshipId, studentId, options) {
        options = {
            url: urlApi('internships/' + internshipId + '/students/' + studentId + '/update', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    existsStudent (internshipId, studentId, options) {
        options = {
            url: urlApi('internships/' + internshipId + '/students/' + studentId + '/exists', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    deleteStudent (internshipId, studentId, options) {
        options = {
            url: urlApi('internships/' + internshipId + '/students/' + studentId + '/delete', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    fullStudent (internshipId, lecturerId, options) {
        options = {
            url: urlApi('internships/' + internshipId + '/lecturers/' + lecturerId + '/full-student', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    getLecturers (internshipId, options) {
        options = {
            url: urlApi('internships/' + internshipId + '/lecturers', { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    },

    getCountStudentByDate (internshipId, options) {
        options = {
            url: urlApi('internships/' + internshipId + '/students/count-by-date', { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    }
}
