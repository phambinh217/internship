import { urlApi } from '../helpers/url'
import store from '../stores'

export default {
    token () {
        return store.state.auth.token
    },

    get (options) {
        options = {
            url: urlApi('me', { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    },

    permission (options) {
        options = {
            url: urlApi('me/permissions', { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    },

    update (options) {
        options = {
            url: urlApi('me/update', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    updatePassword (options) {
        options = {
            url: urlApi('me/update-password', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },
}
