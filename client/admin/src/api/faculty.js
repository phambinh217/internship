import { urlApi } from '../helpers/url'
import store from '../stores'

export default {
    token () {
        return store.state.auth.token
    },

    paginate (options) {
        options = {
            url: urlApi('faculties', { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    },

    get (facultyId, options) {
        options = {
            url: urlApi('faculties/' + facultyId, { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    },

    create (options) {
        options = {
            url: urlApi('faculties/store', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    update (facultyId, options) {
        options = {
            url: urlApi('faculties/' + facultyId + '/update', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    delete (facultyId, options) {
        options = {
            url: urlApi('faculties/' + facultyId + '/delete', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },

    sortOrder (options) {
        options = {
            url: urlApi('faculties/sort-order', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    }
}
