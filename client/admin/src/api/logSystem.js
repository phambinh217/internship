import { urlApi } from '../helpers/url'
import store from '../stores'

export default {
    token () {
        return store.state.auth.token
    },

    get (date, options) {
        options = {
            url: urlApi('logs-system/' + date, { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    },

    all (options) {
        options = {
            url: urlApi('logs-system', { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    },

    delete (options) {
        options = {
            url: urlApi('logs-system/delete', { token: this.token() }),
            type: 'POST',
            ...options
        }

        return $.ajax(options)
    },
}
