import store from '../../stores'
import { can } from '../../helpers/gate'

export default (to, from, next) => {
    if (to.meta.auth) {
        if (store.state.auth.check === false) {
            next({ name: 'auth.login' })
            return
        }
    }

    if (to.meta.guest) {
        if (store.state.auth.check == true) {
            next({ name: 'home' })
            return
        }
    }

    if (to.meta.can) {
        if (!can(to.meta.can)) {
            next({ name: 'error-403' })
            return
        }
    }

    next()
}