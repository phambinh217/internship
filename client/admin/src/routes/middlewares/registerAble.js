import store from '../../stores'

export default (to, from, next) => {
    if (store.state.setting.register != 1) {
        next({ name: 'auth.login' })
        return
    }
    next()
}