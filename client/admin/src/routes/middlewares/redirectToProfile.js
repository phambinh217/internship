import store from '../../stores'

export default (to, from, next) => {
    if (to.params.userId === store.state.auth.user.id) {
        next({ name: 'profile.index' })
        return
    }
    next()
}