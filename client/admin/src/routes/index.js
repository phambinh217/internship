import vue from 'vue'
import vueRouter from 'vue-router'
import store from '../stores'
import routes from './routes'
import beforeEach from './middlewares/beforeEach'

vue.use(vueRouter)

const router = new vueRouter({
    // mode: 'history',
    routes
})

router.beforeEach(beforeEach)

export default router
