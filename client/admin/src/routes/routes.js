import redirectToProfile from './middlewares/redirectToProfile'
import registerAble from './middlewares/registerAble'

export default [
    // template
    { path: '/template', name: 'template.index', component: () => import('../app/pages/template/index'), meta: { auth: true }},

    // Base
    { path: '/', name: 'home', component: () => import('../app/pages/home'), meta: { auth: true }},
    { path: '/profile', name: 'profile.index', component: () => import('../app/pages/profile/index'), meta: { auth: true }},
    { path: '/profile/change-password', name: 'profile.change-password', component: () => import('../app/pages/profile/changePassword'), meta: { auth: true }},

    // Auth
    { path: '/login', name: 'auth.login', component: () => import('../app/pages/auth/login'), meta: { guest: true, }} ,
    { path: '/register',
        name: 'auth.register',
        component: () => import('../app/pages/auth/register'),
        beforeEnter: registerAble,
        meta: { guest: true }
    },
    { path: '/forgot-password', name: 'auth.forgot-password', component: () => import('../app/pages/auth/forgotPassword'), meta: { guest: true } },
    { path: '/reset-password', name: 'auth.reset-password', component: () => import('../app/pages/auth/resetPassword'), meta: { guest: true } },

    // Student
    { path: '/students', name: 'student.index', component: () => import('../app/pages/student/index'), meta: { auth: true, can: 'student.read' }},
    { path: '/students/create', name: 'student.create', component: () => import('../app/pages/student/create'), meta: { auth: true, can: 'student.create' }},
    { path: '/students/:studentId/edit', name: 'student.edit', component: () => import('../app/pages/student/edit'), meta: { auth: true, can: 'student.edit' }},
    
    // Lecturer
    { path: '/lecturers', name: 'lecturer.index', component: () => import('../app/pages/lecturer/index'), meta: { auth: true, can: 'lecturer.read' }},
    { path: '/lecturers/create', name: 'lecturer.create', component: () => import('../app/pages/lecturer/create'), meta: { auth: true, can: 'lecturer.create' }},
    { path: '/lecturers/:lecturerId/edit', name: 'lecturer.edit', component: () => import('../app/pages/lecturer/edit'), meta: { auth: true, can: 'lecturer.edit' }},

    // Iternship
    { path: '/internships', name: 'internship.index', component: () => import('../app/pages/internship/index'), meta: { auth: true, can: 'internship.read' }},
    { path: '/internships/create', name: 'internship.create', component: () => import('../app/pages/internship/create'), meta: { auth: true, can: 'internship.create' }},
    { path: '/internships/:internshipId/edit', name: 'internship.edit', component: () => import('../app/pages/internship/edit'), meta: { auth: true, can: 'internship.edit' }},
    { path: '/internships/:internshipId/show', name: 'internship.show', component: () => import('../app/pages/internship/show'), meta: { auth: true, can: 'internship.show' }},

    // School
    { path: '/school', name: 'school.index', component: () => import('../app/pages/school/index'), meta: { auth: true, can: 'school.read' }},
    
    // User
    { path: '/users', name: 'user.index', component: () => import('../app/pages/user/index'), meta: { auth: true, can: 'user.read' }},
    { path: '/users/create', name: 'user.create', component: () => import('../app/pages/user/create'), meta: { auth: true, can: 'user.create' }},
    { path: '/users/:userId/edit',
        name: 'user.edit',
        component: () => import('../app/pages/user/edit'),
        beforeEnter: redirectToProfile,
        meta: { auth: true, can: 'user.edit' }
    },
    { path: '/users/roles/create', name: 'role.create', component: () => import('../app/pages/user/createRole'),meta: { auth: true, can: 'role.create' }},
    { path: '/users/roles/:roleId/edit', name: 'role.edit', component: () => import('../app/pages/user/editRole'), meta: { auth: true, can: 'role.edit' }},

    // setting
    { path: '/setting', name: 'setting.index', component: () => import('../app/pages/setting/index'), meta: { auth: true, can: 'setting' }},

    // setting
    { path: '/log-system', name: 'log-system.index', component: () => import('../app/pages/logSystem/index'), meta: { auth: true, can: 'log-system.read' }},

    // Error
    { path: '/403', name: 'error-403', component: () => import('../app/pages/error/403') },
    { path: '/404', name: 'error-404', component: () => import('../app/pages/error/404') },
    { path: '*', redirect: '/404' },
]
