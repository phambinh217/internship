class Notify {
  show (type, message, timeout) {
    $('body').removeClass('has-notify')
    $('body').find('.notify').remove()

    $('body').addClass('has-notify')
    $('body').append('<div class="notify notify-' + type + '">' + message + '</div>')

    if (timeout) {
      setTimeout(function () {
        $('body').removeClass('has-notify')
      }, timeout)
    }
  }

  clear () {
    $('body').removeClass('has-notify')
    $('body').find('.notify').remove()
  }

  primary (message, timeout) {
    return this.show('primary', message, timeout)
  }

  success (message, timeout) {
    return this.show('success', message, timeout)
  }

  error (message, timeout) {
    return this.show('error', message, timeout)
  }

  warning (message, timeout) {
    return this.show('warning', message, timeout)
  }

  info (message, timeout) {
    return this.show('info', message, timeout)
  }
}

export default new Notify()
