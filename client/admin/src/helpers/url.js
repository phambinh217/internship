import config from '../configs'

export const url = (url) => {
  return config.baseUrl + url
}

export const urlStorage = (url) => {
  return config.storageUrl + url
}

export const urlApi = (url, queries) => {
  if (queries) {
    url = url + '?' + $.param(queries)
  }
  return config.apiUrl + url
}

export const imageUrl = (url, size) => {
  if (size) {
    url = 'photos/resize/' + size.join('x') + '/' + url
    return config.baseUrl + url
  }

  return config.storageUrl + url
}