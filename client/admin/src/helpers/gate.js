import store from '../stores'

export const can = (gate) => {
    const permissions = store.state.auth.permissions
    if (permissions === '*') {
        return true
    } else if (permissions == 0) {
        return false
    } else if (typeof permissions === 'object') {
        if (permissions.hasOwnProperty(gate)) {
            return permissions[gate]
        }

        return true
    }

    return true
}