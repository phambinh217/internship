export default {
    storageUrl: window.app.config.storageUrl,
    baseUrl: window.app.config.baseUrl,
    apiUrl: window.app.config.apiUrl,
    demoMode: window.app.config.demoMode
}
