import { urlApi } from '../helpers/url'
import store from '../stores'

export default {
    token () {
        return store.state.auth.token
    },

    namespace (namespace, options) {
        options = {
            url: urlApi('configurations/' + namespace, { token: this.token() }),
            type: 'GET',
            ...options
        }

        return $.ajax(options)
    }
}
