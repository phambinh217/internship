import moment from 'moment'

export const fromNow = (time) => {
  return moment(time, 'YYYY-MM-DD HH:mm:ss').fromNow()
}

export const format = (time, format) => {
  return moment(time, 'YYYY-MM-DD HH:mm:ss').format(format)
}

export const today = (format) => {
  return moment().format(format)
}