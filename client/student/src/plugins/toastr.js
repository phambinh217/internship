import toastr from 'toastr/toastr'

toastr.options.closeButton = false
toastr.options.positionClass = 'toast-bottom-left'
toastr.options.showMethod = 'slideDown'
toastr.options.hideMethod = 'slideUp'
toastr.options.closeMethod = 'slideUp'
toastr.options.closeDuration = 5000000;

export default toastr
