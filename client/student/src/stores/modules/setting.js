import configApi from '../../api/config'

const state = {
    'logo': '',
    'register': 0,
    'default_user_role_id': 0
}

const getters = {
}

const mutations = {
    setState (state, payload) {
        // state = _.cloneDeep(payload)
        for (let key in payload) {
            state[key] = payload[key]
        }
    }
}

const actions = {
    all ({ commit }) {
        return configApi.namespace('setting', {
            success (res) {
                commit('setState', res.data)
            }
        })
    }
}

export default {
    state,
    getters,
    mutations,
    actions,
    namespaced: true
}
