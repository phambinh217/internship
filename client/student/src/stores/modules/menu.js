const state = {
    active: 'home',
    data: [
        {
            name: 'home',
            icon: 'home',
            label: 'Home page',
            route: 'home',
            gate: 'home'
        },
        {
            name: 'user',
            icon: 'supervisor_account',
            label: 'Users',
            route: 'user.index',
            gate: 'user.read'
        },
        {
            name: 'setting',
            icon: 'settings',
            label: 'Settings',
            route: 'setting.index',
            gate: 'setting'
        }
    ],
    total: 3
}

const getters = {
    collections (state) {
        return state.data
    },

    active (state) {
        return state.active
    }
}

const mutations = {
    setActive (state, menu) {
        state.active = menu
    }
}

const actions = {
    setActive ({ commit }, menu) {
        commit('setActive', menu)
    }
}

export default {
    state,
    getters,
    mutations,
    actions,
    namespaced: true
}
