export const togglecolumnLeft = ({ commit }) => {
  commit('togglecolumnLeft')
}

export const setActiveColumnLeft = ({ commit }, status) => {
  commit('setActiveColumnLeft', status)
}

export const setAdminMenu = ({ commit }, menus) => {
  commit('setAdminMenu', menus)
}

export const setBreadcrumb = ({ commit }, breadcrumbs) => {
  commit('setBreadcrumb', breadcrumbs)
}

export const setPageTitle = ({ commit }, title) => {
  commit('setPageTitle', title)
}

export const setMenuMenuActive = ({ commit }, menuName) => {
  commit('setMenuMenuActive', menuName)
}

export const setAppLoading = ({ commit }, status) => {
  commit('setAppLoading', status)
}
