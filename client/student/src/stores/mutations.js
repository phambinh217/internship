export const togglecolumnLeft = (state) => {
  state.activeColumnLeft = !state.activeColumnLeft
}

export const setActiveColumnLeft = (state, status) => {
  state.activeColumnLeft = status
}

export const setAdminMenu = (state, menus) => {
  state.adminMenus = menus
}

export const setBreadcrumb = (state, breadcrumbs) => {
  state.breadcrumbs = breadcrumbs
}

export const setPageTitle = (state, pageTitle) => {
  state.pageTitle = pageTitle
}

export const setMenuMenuActive = (state, menuName) => {
  state.adminMenuActive = menuName
}

export const setAppLoading = (state, status) => {
  state.appLoading = status
}
