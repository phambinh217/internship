import vue from 'vue'
import vuex from 'vuex'
import * as getters from './getters'
import * as mutations from './mutations'
import * as actions from './actions'
import auth from './modules/auth'
import createPersistedState from 'vuex-persistedstate'
import createMutationsSharer from 'vuex-shared-mutations'
import menu from './modules/menu'
import setting from './modules/setting'

vue.use(vuex)

const state = {
    appLoading: false,
    activeColumnLeft: true,
    breadcrumbs: [],
    pageTitle: 'Home'
}

export default new vuex.Store({
    state,
    getters,
    mutations,
    actions,
    modules: {
        auth,
        menu,
        setting
    },
    plugins: [
        createPersistedState({
            key: '__________student________'
        }),
        createMutationsSharer({ predicate: [] })
    ],
    strict: true
})
