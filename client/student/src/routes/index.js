import vue from 'vue'
import vueRouter from 'vue-router'
import store from '../stores'
import { can } from '../helpers/gate'

vue.use(vueRouter)

const router = new vueRouter({
    // mode: 'history',
    routes: [
        // Base
        { path: '/', name: 'home', component: () => import('../app/pages/home'), meta: { auth: true }},
        { path: '/profile', name: 'profile.index', component: () => import('../app/pages/profile/index'), meta: { auth: true }},
        { path: '/profile/change-password', name: 'profile.change-password', component: () => import('../app/pages/profile/changePassword'), meta: { auth: true }},

        // Auth
        { path: '/login', name: 'auth.login', component: () => import('../app/pages/auth/login'), meta: { guest: true, }} ,
        { path: '/register',
            name: 'auth.register',
            component: () => import('../app/pages/auth/register'),
            beforeEnter (to, from, next) {
                if (store.state.setting.register != 1) {
                    next({ name: 'auth.login' })
                    return
                }
                next()
            },
            meta: { guest: true }
        },
        { path: '/forgot-password', name: 'auth.forgot-password', component: () => import('../app/pages/auth/forgotPassword'), meta: { guest: true } },
        { path: '/reset-password', name: 'auth.reset-password', component: () => import('../app/pages/auth/resetPassword'), meta: { guest: true } },

        // internship
        { path: '/internships', name: 'internship.show', component: () => import('../app/pages/internship/show'), meta: { auth: true } },

        // Error
        { path: '/403', name: 'error-403', component: () => import('../app/pages/error/403') },
        { path: '/404', name: 'error-404', component: () => import('../app/pages/error/404') },
        { path: '*', redirect: '/404' },  
    ],
})

router.beforeEach((to, from, next) => {
    if (to.meta.auth) {
        if (store.state.auth.check === false) {
            next({ name: 'auth.login' })
            return
        }
    }

    if (to.meta.guest) {
        if (store.state.auth.check == true) {
            next({ name: 'home' })
            return
        }
    }

    if (to.meta.can) {
        if (!can(to.meta.can)) {
            next({ name: 'error-403' })
            return
        }
    }

    next()
})

export default router
