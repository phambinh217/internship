export default {
  baseUrl: window.app.config.baseUrl,
  apiUrl: window.app.config.apiUrl,
  storageUrl: window.app.config.storageUrl,
  demoMode: window.app.config.demoMode
}
