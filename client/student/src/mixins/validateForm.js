import toastr from '../plugins/toastr'

export default {
    methods: {
        resetMessageBags () {
            for (var key in this.messageBags) {
                this.messageBags[key] = ''
            }
        },

        resetStatusBags () {
            for (var key in this.statusBags) {
                this.statusBags[key] = 'normal'
            }
        },

        formFail (res) {
            if (res) {
                if (res.message) {
                    toastr.error(res.message)
                }

                if (res.errors) {
                    for (let key in res.errors) {
                        this.statusBags[key] = 'error'
                        this.messageBags[key] = res.errors[key]
                    }
                }
            }
        }
    }
}