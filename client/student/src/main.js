import Vue from 'vue'
import app from './app/app'
import router from './routes'
import store from './stores'

require('./bootstrap')

var vm = new Vue({
  el: '#app',
  router,
  store,
  render: h => h(app)
})
