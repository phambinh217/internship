<?php

// Application enviroment
// allows: development, testing, production, demo
define('APPLICATION_ENVIRONMENT', 'development');

// Encryption Key
// If you use the Encryption class, you must set an encryption key.
// See the user guide for more info.
// https://codeigniter.com/user_guide/libraries/encryption.html
define('APPLICATION_KEY', '0bf182be554fbcfde6e9e6b4cc1438fa');

// Application name
define('APPLICATION_NAME', 'Thực tập chuyên ngành');

// Base Url
define('BASE_URL', 'http://thuctap.local/');

// Admin
define('ADMIN_URL', 'http://thuctap.local/admin');

// Paths
define('ROOTPATH', __DIR__ .'/');
define('PUBLICPATH', __DIR__ .'/public/');

// Database
define('DB_DRIVER', 'mysqli');
define('DB_HOST', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'thuctap');
define('DB_PREFIX', '');

// email
define('EMAIL_FROM', 'noreply@poll.local');
define('EMAIL_NAME', 'Poll');
define('EMAIL_USER', 'fgp.phambinh.net@gmail.com');
define('EMAIL_PASSWORD', 'nnbhwiyxdylbehzw');
define('EMAIL_PORT', '465');

// Other constants
define('INTERNSHIP_TYPE_ALL_STUDENTS', 1);
define('INTERNSHIP_TYPE_FACULTY_STUDENTS', 2);  
define('INTERNSHIP_TYPE_MAJOR_STUDENTS', 3);
define('INTERNSHIP_TYPE_CLASS_STUDENTS', 4);
define('INTERNSHIP_TYPE_STUDENTS', 5);