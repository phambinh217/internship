# Các Table tối thiểu
* users: Lưu thông tin admin
* lecturers: Lưu thông tin giảng viên
* students: Lưu thông tin sinh viên:
* classes: Lưu thông tin lớp
* faculties: Lưu thông tin khoa
* majors: Lưu thông tin ngành
* internships: Lưu thông tin các đợt thực tập
* internship_lecturers: Lưu thông tin các giảng viên tham gia vào đợt thực tập
* internship_objects: Lưu thông tin phạm vi sinh viên được phép tham gia vào đợt thực tập.
* internship_students: Là bảng lưu thông tin các sinh viên được tham gia vào đợt thực tập. Bảng này sẽ chỉ có giá trị khi đợt thực tập này được diễn ra với chỉ định một số sinh viên nào đó
* student_internships: Lưu thông tin các sinh viên đăng ký tham gia vào đợt thực tập

# Table lecturers
* id
* code
* lastname
* firstname
* phone
* email
* major_id: ID ngành
* faculty_id: ID khoa
* forte: Sở trường, lưu dưới dạng json
* avatar

# Table students
* id
* code
* phone
* email
* firstname
* lastname
* class_id: ID lớp
* major_id: ID ngành
* faculty_id: ID khoa
* term: Khóa
* password

# Table classes
* id
* name
* code: Mã lớp
* major_id: ID ngành
* faculty_id: ID khoa
* term: Khóa

# Table majors
* id
* name
* code: Mã ngành
* faculty_id: ID khoa

# Table faculties
* id
* name
* code: Mã khoa

# Table internships
* id
* name
* type
* date_open
* date_close

# Table internship_to_lecturer
* id
* internship_id
* lecturer_id
* limit_student

# Table student_internships
* id
* internship_id
* lecturer_id
* student_id
* topic_name
* topic_description
* created_at

# internship_objects
* id
* internship_id
* object
* object_id

# internship_students
* id
* internship_id
* student_id

# Questions?
## 1. Lấy ra tất cả các đợt thực tập có thể tham gia của một sinh viên
Các đợt thực tập là khả dụng với một sinh viên khi đáp ứng đủ một trong các yêu cầu
    1. Đợt thực tập này diễn ra cho sinh viên này
    2. Đợt thực tập này diễn ra cho lớp của sinh viên này
    3. Đợt thực tập này diễn ra cho ngành và khóa của sinh viên này
    4. Đợt thực tập này diễn ra cho khoa và khóa của sinh viên này
    5. Đợt thực tập này diễn ra cho tất cả sinh viên và khóa của sinh viên này
Và đáp ứng được tất cả các yêu cầu sau
    1. Đợt thực tập còn đang mở