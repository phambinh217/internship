# Các màn hình cho sinh viên
* Màn hình đăng ký
* Màn hình đăng nhập
* Màn hình danh sách đợt thực tập
* Màn chi tiết đợt thực tập
* Màn hình đăng ký tham gia đợt thực tập
* Màn hình quản lý các đợt thực tập đã đăng ký

# Màn hình đăng ký
Màn hình này thiết kế đơn giản, với form cho phép nhập các thông tin phù hợp với chức năng "sinh viên đăng ký"

# Màn hình đăng nhập
Màn hình này thiết kế đơn giản, với form cho phép nhập các thông tin phù hợp với chức năng "sinh viên đăng nhập"

# Màn hình danh sách đợt thực tập
Màn hình này có thiết kế dạng danh sách. Mỗi đợt thực tập làm nổi bật các thông tin
* Tên đợt thực tập
* Thời gian đếm ngược đăng ký
* Avatar của các giảng viên
* Số sinh viên đã đăng ký / Tổng số sinh viên được phép đăng ký
* Sinh viên đã đăng ký tham gia đợt này hay chưa?

# Màn hình chi tiết đợt thực tập
Màn hình này hiển thị chi tiết về đợt thực tập với các thông tin
* Tên đợt thực tập
* Thời gian đếm ngược đăng ký
* Danh sách giảng viên
    * Tên giảng viên
    * Số sinh viên đã đăng ký
    * Các lĩnh vực sở trường
    * Nút { Đăng ký | Chọn lại }
        * Khi click vào nút đăng ký, tiếp tục hiển thị ra 2 menu
            * Đăng ký và đề xuất đề tài: Click vào nút này sẽ hiện ra dialog
                * Tên đề tài
                * Mô tả qua về đề tài
            * Đăng ký với đề tài nhận từ giảng viên
            * Khi sinh viên đã chọn 1 trong 2 menu trên, thì các giảng viên khác sẽ bị mờ đi chỉ làm nột bật mỗi giảng viên đã chọn
        * Khi click vào nút chọn lại thì hiển thị ra 2 menu
            * Hủy đăng ký giảng viên này
            * Cập nhật lại đề tài

# Màn hình quản lý các đợt thực tập đã đăng ký
