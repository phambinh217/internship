# Màn hình quản lý đợt thực tập
* Thống kê được số sinh viên đã đăng ký
    * Sắp xếp theo họ tên, giảng viên, thời gian đăng ký
    * Xuất danh sách
* Đăng ký cho sinh viên
* Thống kê được các giảng viên
    * Đã đủ sinh viên đăng ký
    * Chưa có sinh viên nào đăng ký
    * Có nhưng chưa đủ
* Biểu đồ đường thể hiện độ gia tăng đăng ký của sinh viên

## Api
**Api lấy danh sách sinh viên trong 1 đợt**
internships/4/students

**Api lấy danh sách giảng viên kèm thông tin sinh viên đã đăng ký**
internships/4/lecturers

**Api lấy số sinh viên đăng ký theo ngày**
internships/4/students/count-by-date
{
    date_open: '',
    date_close: '',
    total_dates: "",
    dates: [
        {
            date: "",
            total_student: ""
        },
        {
            date: "",
            total_student: ""
        },
        {
            date: "",
            total_student: ""
        }
    ]
}