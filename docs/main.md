# Mô tả chung
Dự án cho phép admin đăng các đợt thực tập, sinh viên cần thực tập sẽ vào đăng ký và chọn giảng viên hướng dẫn

# Đối tượng sử dụng
Dự án tạo ra phục vụ cho hai đối tượng
* Admin: Người quản trị hệ thống
* Sinh viên: Người đăng ký thực tập

# Modules
Dự án được chia thành các module
* Giảng viên
* Sinh viên
* Quản trị viên
* Cơ cấu trường học: Khoa > Ngành > Lớp
* Các đợt thực tập

# Module sinh viên
Module sinh viên cho phép quản lý thông tin sinh viên với các thông tin sau
* Mã sinh viên
* Họ và tên
* Số điện thoại
* Email
* Khoa
* Ngành
* Lớp
* Các đợt thực tập đã tham gia
    * Tên đợt thực tập
    * Giảng viên hướng dẫn
    * Đề tài thực hiện

# Module giảng viên
Module giảng viên cho phép quản lý thông tin giảng viên với các thông tin sau
* Mã giảng viên
* Họ và tên
* Số điện thoại
* Khoa
* Email
* Ngành
* Các lĩnh vực sở trường
* Các đợt thực tập tham gia
    * Tên đợt thực tập
    * Các sinh viên của đợt thực tập này

# Module đợt thực tập
Module đợt thực tập cho phép quản lý các đợt thực tập với các thông tin
* Tên đợt thực tập
* Ngày bắt đầu mở đăng ký
* Ngày đóng đăng ký
* Các giảng viên tham gia hướng dẫn trong đợt thực tập này
* Số học viên tối đa mà mỗi giảng viên được phép hướng dẫn
* Những sinh viên nào được phép đăng ký
    **Phạm vi**
    * Sinh viên toàn trường
    * Sinh viên thuộc khoa: CNTT, TNKT
    * Sinh viên thuộc ngành
    * Sinh viên thuộc lớp
    * Chỉ áp dụng với một số sinh viên
    Nếu chọn phạm vi là sinh viên toàn trường, hoặc sinh viên thuộc khoa, hoặc sinh viên thuộc ngành, thì hiện ra thông tin về khóa
    **Khóa**
    * Áp dụng với sinh viên khóa: 13
    
* Các sinh viên đã đăng ký vào đợt thực tập
    * Tên sinh viên
    * Mã sinh viên
    * Tên giảng viên
    * Mã giảng viên
    * Tên đề tài

# Chức năng của admin
Admin được phép thực hiện các thao tác quản lý của các module như mô tả ở trên

# Chức năng của sinh viên

## Sinh viên đăng ký tài khoản
Sinh viên sẽ tiến hành đăng ký tài khoản với các thông tin
* Mã sinh viên
* Họ và tên
* Số điện thoại
* Lớp
* Mật khẩu

## Sinh viên đăng nhập với tài khoản đã có
Sinh viên sẽ tiến hành đăng nhập để với các thông tin
* Mã sinh viên | Hoặc số điện thoại
* Mật khẩu

## Sinh viên chọn đợt thực tập
Các đợt thực tập mà sinh viên được phép đăng ký tham gia sẽ hiện ra, sinh viên sẽ chọn lấy một đợt thực tập phù hợp. Sau đó sẽ hiện ra danh sách giảng viên của đợt thực tập đó, sinh viên sẽ chọn lấy một giảng viên yêu thích. Khi chọn được giảng viên phù hợp, sinh viên có thể đề xuất ý tưởng về đề tài thực tập ngay, hoặc yêu cầu giảng viên gợi ý đề tài sau.

## Sinh viên quản lý các đợt thực tập
Sau khi đăng ký tham gia đợt thực tập. Nếu đợt thực tập còn đang trong trạng thái mở đăng ký thì sinh viên có thể thay đổi các thông tin về đợt thực tập này, bao gồm các thao tác:
* Hủy đợt thực tập
* Chọn một giảng viên khác
* Cập nhật lại đề tài thực tập
