-- Lấy ra tất cả các đợt thực tập phù hợp với sinh viên
select * from internships
where id in (
    case
        when internships.type = 1 then (select id from internships where term = 13) -- term of student
        when internships.type = 2 then (
            select internships.id from internships
            join internship_objects on internships.id = internship_objects.internship_id and internship_objects.type = 2
            where internship_objects.object_id = 2 -- faculty of student
            and internships.term = 13 -- term of student
            and DATEDIFF(internships.date_close, NOW()) > 0
        )
        when internships.type = 3 then (
            select internships.id from internships
            join internship_objects on internships.id = internship_objects.internship_id and internship_objects.type = 3
            where internship_objects.object_id = 2 -- major of student
            and internships.term = 13 -- term of student
            and DATEDIFF(internships.date_close, NOW()) > 0
        )
        when internships.type = 4 then (
            select internships.id from internships
            join internship_objects on internships.id = internship_objects.internship_id and internship_objects.type = 4
            where internship_objects.object_id = 2 -- class of student
            and DATEDIFF(internships.date_close, NOW()) > 0
        )
        when internships.type = 5 then (
            select internships.id from internships
            join internship_objects on internships.id = internship_objects.internship_id and internship_objects.type = 5
            where internship_objects.object_id = 2 -- id of student
            and DATEDIFF(internships.date_close, NOW()) > 0
        )
    end
)