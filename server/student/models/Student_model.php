<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student_model extends MY_Model
{
    protected $offset_online = 60*15;

    protected $fillable = array(
        'firstname', 'lastname', 'birth', 'email', 'phone', 'avatar', 'password', 'last_online_at', 'major_id', 'faculty_id', 'class_id', 'term',
    );

    protected $show = array(
        'students.id', 'firstname', 'lastname', 'birth', 'email', 'phone', 'avatar', 'major_id', 'faculty_id', 'class_id', 'term', 'created_at', 'updated_at',
    );

    public function with(array $data)
    {
        // if (in_array('role', $data)) {
        // }

        return $this;
    }

    public function get($id)
    {
        return $this->db
            ->select(implode(',', $this->show) . ', (last_online_at + '. $this->offset_online .' > UNIX_TIMESTAMP()) as is_online')
            ->where('students.id', $id)
            ->get('students')->first_row();
    }

    public function get_or_fail($id, $fail_data = array(), $header_status = 404)
    {
        $result = $this->get($id);
        
        if ($result) {
            return $result;
        }

        if ($fail_data) {
            $this->output->json($fail_data, $header_status);
        }

        show_404();
    }

    public function get_by_email($email)
    {
        return $this->db
            ->select(implode(',', $this->show) . ', (last_online_at + '. $this->offset_online .' > UNIX_TIMESTAMP()) as is_online')
            ->where('email', $email)
            ->get('students')->first_row();   
    }

    public function attempt($code, $password)
    {
        $student = $this->db->select('id, code, password')->where(array('code' => $code, 'password' => $password))->get('students')->first_row();
        if (!$student) {
            return false;
        }

        return $student;
    }

    public function all(array $data = array())
    {
        $this->query($data)
            ->select(implode(',', $this->show) . ', (last_online_at + '. $this->offset_online .' > UNIX_TIMESTAMP()) as is_online')
            ->get('students')
            ->result();
    }

    public function paginate(array $data = array())
    {
        if (!isset($data['perpage'])) {
            $perpage = 10;
        } else {
            $perpage = ((int)$data['perpage'] < 0) ? 10 : (int)$data['perpage'];
        }

        if (!isset($data['page'])) {
            $page = 1;
        } else {
            $page = ((int)$data['page'] < 1) ? 1 : (int)$data['page'];
        }

        $results = $this
            ->query($data)
            ->select(implode(',', $this->show) . ', (last_online_at + '. $this->offset_online .' > UNIX_TIMESTAMP()) as is_online')
            ->limit((int)$perpage, (int)($perpage * $page - $perpage))
            ->get('students')
            ->result();

        $total = $this
            ->query($data)
            ->from('students')
            ->count_all_results();

        return $this->pagination($total, $results, $perpage, $page);
    }

    public function query(array $data)
    {
        if (isset($data['with'])) {
            $this->with($data['with']);
        }

        if (isset($data['name'])) {
            $this->db->where('name', $data['name']);
        }
        
        if (isset($data['email'])) {
            $this->db->where('email', $data['email']);
        }
        
        if (isset($data['order_by'])) {
            $allows = array('id', 'firstname', 'lastname', 'role_id', 'birth', 'email', 'last_online_at', 'created_at', 'updated_at');
            if (is_array($data['order_by'])) {
                foreach ($data['order_by'] as $by => $order) {
                    if (in_array($by, $allows)) {
                        $order = strtoupper($order);
                        $order = $order == 'DESC' ? 'DESC' : ($order == 'ASC' ? 'ASC' : 'DESC');
                        $this->db->order_by($by, $order);
                    }
                }
            }
        }

        return $this->db;
    }

    public function create(array $data)
    {
        if (isset($data['birth'])) {
            $data['birth'] = date_format(date_create($data['birth']), 'Y-m-d');
        }
        
        if (isset($data['fullname'])) {
            $name = explode_fullname($data['fullname']);
            $data['firstname'] = $name['firstname'];
            $data['lastname'] = $name['lastname'];
        }

        $data = array_only($data, $this->fillable);

        if ($data) {
            $this->db->set('created_at', 'NOW()', false);
            $this->db->set('updated_at', 'NOW()', false);
            $this->db->insert('students');
            return $this->get($this->db->insert_id());
        }

        return null;
    }

    public function update(array $data, $id)
    {
        if (isset($data['birth'])) {
            $data['birth'] = date_format(date_create($data['birth']), 'Y-m-d');
        }

        if (isset($data['fullname'])) {
            $name = explode_fullname($data['fullname']);
            $data['firstname'] = $name['firstname'];
            $data['lastname'] = $name['lastname'];
        }

        $data = array_only($data, $this->fillable);

        if ($data) {
            $this->db->set('updated_at', 'NOW()', false);
            $this->db->where('id', (int)$id)->update('students');
        }

        return $this->get((int)$id);
    }

    public function update_or_fail(array $data, $id, $fail_data = array(), $header_status = 404)
    {
        $result = $this->update($data, $id);
        if ($result) {
            return $result;
        }

        if ($fail_data) {
            $this->output->json($fail_data, $header_status);
        }

        show_404();
    }

    public function delete($id)
    {
        $this->db->where('id', (int)$id)->delete('students');
    }
}
