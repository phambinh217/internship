<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config_model extends CI_Model
{
    public $config = array();

    protected $fillable = array(
        'name', 'namespace', 'value', 'autoload',   
    );

    public function __construct()
    {
        parent::__construct();

        // Set config from database
        $results = $this->db->select('name, value, namespace')->where('autoload', 1)->get('configs')->result();
        foreach ($results as $result) {
            $this->config[$result->namespace][$result->name] = $result->value;
        }
    }

    public function get($code, $namespace = 'setting', $refresh = false)
    {
        if (!$refresh && isset($this->config[$namespace][$code])) {
            return $this->config[$namespace][$code];
        }

        $this->db->select('name, value')->where('name', $code);

        if ($namespace) {
            $this->db->where('namespace', $namespace);
        }
        
        $result = $this->db->get('configs')->first_row();

        if ($result) {
            $this->config[$result->name] = $result->value;
            return $this->config[$result->name];
        }

        return null;
    }

    public function bynamespace($namespace)
    {
        $results = $this->db->select('name, value')->where('namespace', $namespace)->get('configs')->result();
        $configs = array();
        foreach ($results as $result) {
            $configs[$result->name] = $result->value;
        }

        return $configs;
    }

    public function has($code, $namespace = 'setting')
    {
        return isset($this->config[$code], $namespace);
    }

    public function save(array $data)
    {
        if (isset($data['name']) && isset($data['namespace'])) {
            $this->db
                ->where('namespace', $data['namespace'])
                ->where('name', $data['name'])
                ->delete('configs');

            $this->db->insert('configs', array_only($data, $this->fillable));

            return $this->get($data['name'], $data['namespace'], true);
        }
    }

    public function delete($code, $namespace)
    {
        $this->db->where('name', $code)->where('namespace', $namespace)->delete('configs');
    }
}
