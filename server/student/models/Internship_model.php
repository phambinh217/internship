<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Internship_model extends MY_Model
{
    public function available_with_student($student, $data = array())
    {
        if (!isset($data['perpage'])) {
            $perpage = 10;
        } else {
            $perpage = ((int)$data['perpage'] < 0) ? 10 : (int)$data['perpage'];
        }

        if (!isset($data['page'])) {
            $page = 1;
        } else {
            $page = ((int)$data['page'] < 1) ? 1 : (int)$data['page'];
        }

        try {
            $sql = "SELECT * FROM internships
            WHERE
                IF (internships.type = {INTERNSHIP_TYPE_ALL_STUDENTS}, id in (SELECT id FROM internships WHERE term = '{term}'),
                IF (internships.type = {INTERNSHIP_TYPE_FACULTY_STUDENTS}, id in (
                        SELECT internships.id FROM internships
                        JOIN internship_objects on internships.id = internship_objects.internship_id AND internship_objects.type = {INTERNSHIP_TYPE_ALL_STUDENTS}
                        WHERE internship_objects.object_id = '{faculty_id}' -- faculty of student
                        AND internships.term = '{term}' -- term of student
                        AND DATEDIFF(internships.date_close, NOW()) > 0
                ),
                IF (internships.type = {INTERNSHIP_TYPE_MAJOR_STUDENTS}, id in (
                    SELECT internships.id FROM internships
                    JOIN internship_objects on internships.id = internship_objects.internship_id AND internship_objects.type = {INTERNSHIP_TYPE_MAJOR_STUDENTS}
                    WHERE internship_objects.object_id = '{major_id}' -- major of student
                    AND internships.term = '{term}' -- term of student
                    AND DATEDIFF(internships.date_close, NOW()) > 0
                ),
                IF (internships.type = {INTERNSHIP_TYPE_CLASS_STUDENTS}, id in (
                    SELECT internships.id FROM internships
                    JOIN internship_objects on internships.id = internship_objects.internship_id AND internship_objects.type = {INTERNSHIP_TYPE_CLASS_STUDENTS}
                    WHERE internship_objects.object_id = '{class_id}' -- class of student
                    AND DATEDIFF(internships.date_close, NOW()) > 0
                ),
                IF (internships.type = {INTERNSHIP_TYPE_STUDENTS}, id in (
                    SELECT internships.id FROM internships
                    JOIN internship_objects on internships.id = internship_objects.internship_id AND internship_objects.type = {INTERNSHIP_TYPE_STUDENTS}
                    WHERE internship_objects.object_id = '{student_id}' -- id of student
                    AND DATEDIFF(internships.date_close, NOW()) > 0
                ), id = null
                )))))
            LIMIT {limit}, {limit_from}";
            
            $bind = array(
                '{term}' => (int)$student->term,
                '{faculty_id}' => (int)$student->faculty_id,
                '{major_id}' => (int)$student->major_id,
                '{class_id}' => (int)$student->class_id,
                '{student_id}' => (int)$student->id,
                
                '{INTERNSHIP_TYPE_ALL_STUDENTS}' => INTERNSHIP_TYPE_ALL_STUDENTS,
                '{INTERNSHIP_TYPE_FACULTY_STUDENTS}'=> INTERNSHIP_TYPE_FACULTY_STUDENTS,
                '{INTERNSHIP_TYPE_MAJOR_STUDENTS}'=> INTERNSHIP_TYPE_MAJOR_STUDENTS,
                '{INTERNSHIP_TYPE_CLASS_STUDENTS}'=> INTERNSHIP_TYPE_CLASS_STUDENTS,
                '{INTERNSHIP_TYPE_STUDENTS}'=> INTERNSHIP_TYPE_STUDENTS,

                '{limit_from}' => (int)$perpage,
                '{limit}' => (int)($perpage * $page - $perpage)
            );

            $sql = str_replace(array_keys($bind), array_values($bind), $sql);

            $results = $this->db->query($sql)->result();

            $total = $this->count($student, $data);

            return $this->pagination($total, $results, $perpage, $page);
        } catch (\Exception $e) {

        }
    }

    public function count($student, array $data = array())
    {
        $sql = "SELECT count(*) as numrows FROM internships
            WHERE
                IF (internships.type = {INTERNSHIP_TYPE_ALL_STUDENTS}, id in (SELECT id FROM internships WHERE term = '{term}'),
                IF (internships.type = {INTERNSHIP_TYPE_FACULTY_STUDENTS}, id in (
                        SELECT internships.id FROM internships
                        JOIN internship_objects on internships.id = internship_objects.internship_id AND internship_objects.type = {INTERNSHIP_TYPE_ALL_STUDENTS}
                        WHERE internship_objects.object_id = '{faculty_id}' -- faculty of student
                        AND internships.term = '{term}' -- term of student
                        AND DATEDIFF(internships.date_close, NOW()) > 0
                ),
                IF (internships.type = {INTERNSHIP_TYPE_MAJOR_STUDENTS}, id in (
                    SELECT internships.id FROM internships
                    JOIN internship_objects on internships.id = internship_objects.internship_id AND internship_objects.type = {INTERNSHIP_TYPE_MAJOR_STUDENTS}
                    WHERE internship_objects.object_id = '{major_id}' -- major of student
                    AND internships.term = '{term}' -- term of student
                    AND DATEDIFF(internships.date_close, NOW()) > 0
                ),
                IF (internships.type = {INTERNSHIP_TYPE_CLASS_STUDENTS}, id in (
                    SELECT internships.id FROM internships
                    JOIN internship_objects on internships.id = internship_objects.internship_id AND internship_objects.type = {INTERNSHIP_TYPE_CLASS_STUDENTS}
                    WHERE internship_objects.object_id = '{class_id}' -- class of student
                    AND DATEDIFF(internships.date_close, NOW()) > 0
                ),
                IF (internships.type = {INTERNSHIP_TYPE_STUDENTS}, id in (
                    SELECT internships.id FROM internships
                    JOIN internship_objects on internships.id = internship_objects.internship_id AND internship_objects.type = {INTERNSHIP_TYPE_STUDENTS}
                    WHERE internship_objects.object_id = '{student_id}' -- id of student
                    AND DATEDIFF(internships.date_close, NOW()) > 0
                ), id = null
                )))))";
        
        $bind = array(
            '{term}' => (int)$student->term,
            '{faculty_id}' => (int)$student->faculty_id,
            '{major_id}' => (int)$student->major_id,
            '{class_id}' => (int)$student->class_id,
            '{student_id}' => (int)$student->id,
            
            '{INTERNSHIP_TYPE_ALL_STUDENTS}' => INTERNSHIP_TYPE_ALL_STUDENTS,
            '{INTERNSHIP_TYPE_FACULTY_STUDENTS}'=> INTERNSHIP_TYPE_FACULTY_STUDENTS,
            '{INTERNSHIP_TYPE_MAJOR_STUDENTS}'=> INTERNSHIP_TYPE_MAJOR_STUDENTS,
            '{INTERNSHIP_TYPE_CLASS_STUDENTS}'=> INTERNSHIP_TYPE_CLASS_STUDENTS,
            '{INTERNSHIP_TYPE_STUDENTS}'=> INTERNSHIP_TYPE_STUDENTS
        );

        $sql = str_replace(array_keys($bind), array_values($bind), $sql);

        $result = $this->db->query($sql)->first_row();
        
        return $result->numrows;
    }
}
