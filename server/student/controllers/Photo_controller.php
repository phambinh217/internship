<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Photo_controller extends CI_Controller
{
    public function resize($width, $height, $image)
    {
        $params = func_get_args();
        // Unset 2 params first are width and height of image
        unset($params[0]);
        unset($params[1]);

        // Codeigniter is not allow '/' character in url address
        // so we have to use this way to get image link
        $image = implode('/', $params);

        $new_image = image_resize($image, $width, $height);
        $image_info = getimagesize(ROOTPATH . $new_image);

        switch ($image_info[2]) {
            case IMAGETYPE_JPEG:
                header('Content-Type: image/jpeg');
                break;
            case IMAGETYPE_GIF:
                header('Content-Type: image/gif');
                break;
            case IMAGETYPE_PNG:
                header('Content-Type: image/png');
                break;
        }

        header('Content-Length: ' . filesize(ROOTPATH . $new_image));

        return $this->output
            ->set_output(file_get_contents(ROOTPATH . $new_image));
    }

    public function upload()
    {
        $this->lang->load('photo');

        $response = array();
        $errors = array();
        $header_status = 200;

        $validator = $this->validator_upload($this->input->post());
        if ($validator->run()) {
            $this->load->library('upload');

            if (! $this->upload->do_upload('photo')) {
                $errors = $this->upload->display_errors();
            } else {
                $photo = $this->upload->data();
                $width = isset($_POST['width']) ? $this->input->get('width') : 100;
                $height = isset($_POST['width']) ? $this->input->get('height') : 100;
                $source_image = '/storage/upload/' . $photo['file_name'];
                $cache = 'photo/resize/' . $width .'x' . $height . $source_image;

                $response['path'] = $source_image;
                $response['cache'] = $cache;
            }
        } else {
            $errors = $validator->get_errors_array();
        }

        if (!$errors) {
            $response['success'] = true;
            $response['message'] = trans('photo_uploaded');
        } else {
            $response['success'] = false;
            $response['message'] = trans('photo_could_not_upload');
            $response['errors'] = $errors;

            $header_status = 406;
        }

        $this->output->json($response, $header_status);
    }

    protected function validator_upload(array $data)
    {
        $this->lang->load('photo');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('photo', trans('photo'), 'trim');
        $this->form_validation->set_data($data);

        return $this->form_validation;
    }
}
