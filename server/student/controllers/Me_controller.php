<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Me_controller extends MY_Controller
{
    public function show()
    {
        $me = $this->jwt_auth->user();
        $response = array(
            'success' => true,
            'message' => trans('success'),
            'me' => $me
        );
        $this->output->json($response, 200);
    }

    public function permission()
    {
        $allow = $this->gate->get_allow();
        $response = array(
            'success' => true,
            'message' => trans('success'),
            'permission' => $allow
        );
        $this->output->json($response, 200);
    }

    public function update()
    {
        $this->lang->load(array('form_validation', 'me'));
        $user_id = $this->jwt_auth->user()->id;
        
        $response = array();
        $validator = $this->validate_update($this->input->post(), $user_id);
        $header_status = 200;

        if ($validator->run() == false) {
            $response = array(
                'success' => false,
                'message' => trans('form_validation_check_the_fields'),
                'errors' => $validator->get_errors_array(),
            );
            $header_status = 406;
        } else {
            $this->load->model('user_model');
            $data = $this->input->post();
            
            // Những trường thông tin được phép cập nhật
            $fillable = array('fullname', 'role_id', 'birth', 'phone', 'avatar', 'send_email_new_login');
            $me = $this->user_model->update(array_only($data, $fillable), $user_id);
            
            $response = array(
                'success' => true,
                'message' => trans('me_saved'),
                'me' => $me
            );
        }

        $this->output->json($response, $header_status);
    }

    public function update_password()
    {
        $this->lang->load(array('form_validation', 'me'));

        $errors = array();
        $response = array();
        $header_status = 200;

        $validator = $this->validate_update_password($this->input->post());

        if ($validator->run() == false) {
            $response = array(
                'success' => false,
                'message' => trans('form_validation_check_the_fields'),
                'errors' => $validator->get_errors_array(),
            );
            $header_status = 406;
        } else {
            $this->load->model('user_model');
            // Olny update password allow
            $data = array(
                'password' => md5(md5($this->input->post('password'))),
            );
            $me = $this->user_model->update($data, $this->jwt_auth->user()->id);

            $response = array(
                'success' => true,
                'message' => trans('me_saved'),
                'me' => $me
            );
        }

        $this->output->json($response, $header_status);
    }

    protected function validate_update_password(array $data)
    {
        $this->lang->load('me');

        $password = $this->db->select('password')->where('id', $this->jwt_auth->user()->id)->get('users')->first_row();

        $this->load->library('form_validation');
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('old_password', trans('me_current_password'), 'required|decrypt['. $password->password .']');
        $this->form_validation->set_rules('password', trans('me_new_password'), 'required');
        $this->form_validation->set_rules('password_confirmation', trans('me_repassword'), 'required|matches[password]');

        return $this->form_validation;
    }

    protected function validate_update(array $data, $user_id)
    {
        $this->lang->load('me');

        $this->load->library('form_validation');
        $this->form_validation->set_data($data);

        if (isset($data['fullname'])) {
            $this->form_validation->set_rules('fullname', trans('me_fullname'), 'required|max_length[255]');
        }

        if (isset($data['birth'])) {
            $this->form_validation->set_rules('birth', trans('me_birth'), 'required|date_format[d-m-Y]');
        }

        if (isset($data['email'])) {
            $this->form_validation->set_rules('email', trans('me_email'), 'required|valid_email|callback_unique_email['.$user_id.']');
        }

        if (isset($data['phone'])) {
            $this->form_validation->set_rules('phone', trans('me_telephone'), 'max_length[255]');
        }

        if (isset($data['avatar'])) {
            $this->form_validation->set_rules('avatar', trans('me_avatar'), 'max_length[255]');
        }

        return $this->form_validation;
    }
    
    public function unique_email($email, $user_id)
    {
        $this->lang->load('me');

        $count = $this->db->where('email', $email)->where('id !=', $user_id)->count_all_results('users');
        if ($count != 0) {
            $this->form_validation->set_message(__FUNCTION__, trans('me_email_exists'));
            return false;
        }

        return true;
    }
}
