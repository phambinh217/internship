<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Command_make_controller extends CI_Controller
{
    public function controller($path, $type = 'std', $extend = '')
    {
        $path = str_replace(':', '\\', $path);

        if (file_exists(APPPATH . 'controllers/' . $path . '.php')) {
            $this->console->log('Controller is ready exists!');
            return;
        }

        $dirpath = APPPATH . 'controllers/' . dirname($path);

        if (!is_dir($dirpath)) {
            mkdir($dirpath, 0777, true);
        }

        $controller_name = basename($path);

        if ($extend == '') {
            $extend = 'CI_Controller';
        }

        $type = in_array($type, array('std', 'resource', 'restful')) ? $type : 'std';
        $content = file_get_contents(FCPATH .'resources/stub/controller_'. $type .'.stub');

        $syntax = array(
            '{dummy_classname}' => $controller_name,
            '{dummy_extend}' => $extend,
        );

        $content = str_replace(array_keys($syntax), array_values($syntax), $content);

        file_put_contents(APPPATH .'controllers/' . $path .'.php', $content);

        $this->console->log('Controller created!');
    }

    public function model($path, $table, $primary_key= 'id', $extend = 'MY_Model')
    {
        $path = str_replace(':', '\\', $path);

        if (file_exists(APPPATH . 'models/' . $path . '.php')) {
            $this->console->log('Model is ready exists!');
            return;
        }

        $dirpath = APPPATH . 'models/' . dirname($path);

        if (!is_dir($dirpath)) {
            mkdir($dirpath, 0777, true);
        }

        $model_name = basename($path);

        if ($extend == '') {
            $extend = 'CI_Model';
        }

        $content = file_get_contents(FCPATH .'resources/stub/model.stub');

        $syntax = array(
            '{dummy_classname}' => $model_name,
            '{dummy_extend}' => $extend,
            '{dummy_table}' => $table,
            '{dummy_primary_key}' => $primary_key,
        );

        $content = str_replace(array_keys($syntax), array_values($syntax), $content);

        file_put_contents(APPPATH .'models/' . $path .'.php', $content);

        $this->console->log('Model created!');
    }

    public function helper($name)
    {
        if (file_exists(APPPATH . 'helpers/' . $name . '_helper.php')) {
            $this->console->log('Helper is ready exists!');
            return;
        }

        if (file_exists(BASEPATH . 'helpers/' . $name .'_helper.php')) {
            $name = $this->config->item('subclass_prefix') . $name;
        }

        $content = file_get_contents(FCPATH .'resources/stub/helper.stub');

        file_put_contents(APPPATH .'helpers/' . $name .'_helper.php', $content);

        $this->console->log('Helper created!');
    }

    public function library($name)
    {
        if (file_exists(APPPATH . 'libraries/' . $name . '.php')) {
            $this->console->log('Library is ready exists!');
            return;
        }

        $stub = 'library_std';
        $extend = '';

        if (file_exists(BASEPATH . 'libraries/' . $name .'.php') || file_exists(BASEPATH . 'core/' . $name .'.php')) {
            $extend = 'CI_' . $name;
            $name = $this->config->item('subclass_prefix') . $name;
            $stub = 'library_extend';
        }

        $content = file_get_contents(FCPATH .'resources/stub/'. $stub .'.stub');

         $syntax = array(
            '{dummy_classname}' => $name,
            '{dummy_extend}' => $extend,
        );
        $content = str_replace(array_keys($syntax), array_values($syntax), $content);

        file_put_contents(APPPATH .'libraries/' . $name .'.php', $content);

        $this->console->log('Library created!');
    }

    public function language($name, $lang = '')
    {
        if ($lang == '') {
            $lang = $this->config->item('language');
        }

        if (file_exists(APPPATH . 'language/' . $lang . '/' . $name . '_helper.php')) {
            $this->console->log('Language file is ready exists!');
            return;
        }

        $content = file_get_contents(FCPATH .'resources/stub/lang.stub');

        file_put_contents(APPPATH .'language/' . $lang . '/' . $name .'_lang.php', $content);

        $this->console->log('Language file created!');
    }
}