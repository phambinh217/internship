<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Internship_controller extends MY_Controller
{
    public function index()
    {
        $student = $this->jwt_auth->user();
        $this->load->model('internship_model');

        $filter = $this->input->get();
        $response = $this->internship_model->available_with_student($student, $filter);

        $this->output->json($response, 200);
    }

    public function show($id)
    {
        $this->load->model('internship_model');
        $internship = $this->internship_model->get_or_fail($id, array(
            'success' => false,
            'message' => 'Not found'
        ));

        $response = array(
            'success' => true,
            'message' => 'OK',
            'internship' => $internship,
        );

        $this->output->json($response, 200);
    }
}
