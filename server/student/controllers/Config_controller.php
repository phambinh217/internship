<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config_controller extends MY_Controller
{
    public function autoload()
    {
        $autoload = $this->config_model->config;
        $response = array(
            'success' => true,
            'message' => 'Success',
            'data' => $autoload,
        );
        $this->output->json($response, 200);
    }

    public function bynamespace($namespace)
    {
        $data = $this->config_model->bynamespace($namespace);
        $response = array(
            'success' => true,
            'message' => 'Success',
            'data' => $data,
        );
        $this->output->json($response, 200);
    }

    public function save()
    {
        $response = array();
        $validator = $this->validate_save($this->input->post());

        if ($validator->run()) {
            $response = array();
            $configs = $this->input->post('config');

            foreach ($configs as $namespace => $namespace_config) {
                foreach ($namespace_config as $config_name => $config_data) {
                    $data = array(
                        'namespace' => $namespace,
                        'name' => $config_name,
                        'value' => isset($config_data['value']) ? $config_data['value'] : '',
                        'autoload' => isset($config_data['autoload']) ? $config_data['autoload'] : 0,
                    );
                    $response['data'][$namespace][$config_name] = $this->config_model->save($data);
                }
            }
        }

        $response['success'] = true;
        $response['message'] = trans('saved');
        
        $this->output->json($response, 200);
    }

    public function validate_save(array $data)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);
        return $this->form_validation;
    }
}