<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('e')) {
    function e($val = null, $default = null)
    {
        $string = trim($val) != '' ? $val : $default;

        echo htmlentities($string);
    }
}

if (!function_exists('str_std')) {
    function str_std($str, $type = null)
    {
        $str = strtolower($str);
        $str = trim($str);
        
        $array  = explode(' ', $str);
        foreach ($array as $key => $value) {
            if (trim($value) == null) {
                unset($array[$key]);
                continue;
            }
            
            if ($type == 'noun') {
                $array[$key] = ucfirst($value);
            }
        }
        
        $result = implode(' ', $array);
        
        $result = ucfirst($result);
        
        return $result;
    }
}

if (!function_exists('explode_fullname')) {
    function explode_fullname($fullname, $firstname_last = true, $delimiters = ' ')
    {
        $fullname = str_std($fullname, 'noun');
        $arr_name = explode($delimiters, $fullname);

        if (count($arr_name) >= 2) {
            // Tên ở cuối cùng, đúng chuẩn người Việt
            if ($firstname_last) {
                $firstname = array_pop($arr_name);
                $lastname = implode($delimiters, $arr_name);

            // Tên ở đầu tiên, giống tên tiếng anh
            } else {
                $firstname = array_shift($arr_name);
                $lastname = implode($delimiters, $arr_name);
            }
        } else {
            $firstname = $fullname;
            $lastname = '';
        }

        return array(
            'firstname' => $firstname,
            'lastname' => $lastname,
        );
    }
}