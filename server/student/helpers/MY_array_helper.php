<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('array_only')) {
    function array_only($array, $needs)
    {
        $allows = array();
        foreach ($array as $key => $value) {
            if (in_array($key, $needs)) {
                $allows[$key] = $value;
            }
        }
        
        return $allows;
    }
}

if (!function_exists('array_pluck')) {
    /**
     * Fix php < 5.5
     */
    function array_pluck($array, $colum_key)
    {
        $plucks = array();
        foreach ($array as $arr) {
            $arr = (array)$arr;
            if (isset($arr[$colum_key])) {
                $plucks[] = $arr[$colum_key];
            }
        }

        return $plucks;
    }
}