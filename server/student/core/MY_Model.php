<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model
{
    protected function pagination($total, $data, $perpage, $page)
    {
        $this->load->library('pagination');
        $config = array(
            'base_url' => url('/'),
            'total_rows' => $total,
            'per_page' => $perpage,
        );
        $this->pagination->initialize($config);

        $results = new stdClass();
        $results->total = $total;
        $results->data = $data;
        $results->perpage = $perpage;
        $results->page = $page;
        $results->total_page = (int) ceil($total/$perpage);
        $results->render = $this->pagination;

        return $results;
    }
}