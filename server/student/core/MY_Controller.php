<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    protected $data;

    public function __construct()
    {
        parent::__construct();

        $this->__register();

        // Middleware required request authenticated
        $this->required_authentication();

        $this->__boot();

        // Demo mode
        if (APPLICATION_ENVIRONMENT == 'demo') {
            $this->allow_demo_mode();
        }
    }

    /**
     * Register for app
     * 
     * If you need register somes infomation for your app, placed it here.
     * Example: Register permission, autoload common file,...
     * 
     * @return void
     */
    protected function __register()
    {   
        // Load common language
        $this->lang->load('common');
    }

   /**
     * The common script run all method
     * 
     * This method is run after register method
     */
    protected function __boot()
    {
        $this->output->set_header('Access-Control-Allow-Origin: *');
        
        $this->load->model('student_model');

        // Check permission and log the last time user online
        if ($this->jwt_auth->check()) {
            $this->student_model->update(array('last_online_at' => time()), $this->jwt_auth->user()->id);
        }
    }

    protected function required_authentication()
    {
        $except = array(
            'api/login',
            'api/register',
            'api/forgot-password',
            'api/reset-password',
            'api/configurations/setting'
        );

        $token = $this->input->get_post('token');
        $is_veriry = $this->jwt_auth->verify($token);

        if (!in_array(uri_string(), $except)) {
            if (!$is_veriry) {
                $this->output->json(array(
                    'message' => 'No authenticated.',
                ), 401);
            }
        }
    }

    protected function allow_demo_mode()
    {
        // Except urls below
        $except = array(
            
        );

        $allow = true;
        foreach ($except as $route) {
            $route = str_replace(array(':any', ':num'), array('[^/]+', '[0-9]+'), $route);
            if (preg_match('#^'.$route.'$#', uri_string(), $matches)) {
                $allow = false;
            }
        }

        if (!$allow) {
            $response = array(
                'message' => trans('demo_mode_not_allow'),
                'errors' => array(
                    'not_allow' => trans('demo_mode_not_allow'),
                ),
            );

            $this->output->json($response, 406);
        }
    }
}
