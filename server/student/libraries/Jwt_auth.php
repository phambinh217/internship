<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jwt_auth
{
    private $CI;

    protected $user;

    protected $check = false;

    protected $secret_key;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->model('student_model');
        $this->secret_key = $this->CI->config->item('encryption_key');
    }

    public function user()
    {
        return $this->user;
    }

    public function check()
    {
        return $this->check;
    }

    public function attempt($code, $password)
    {
        $user = $this->CI->student_model->attempt($code, $password);

        if ($user !== false) {
            $payload = array(
                'id' => $user->id,
                'code' => $user->code,
                'password' => $password,
            );
            $token = $this->create_token($payload);
            $user = $this->CI->student_model->get($user->id);
            $this->user = $user;
            $this->check = true;
            
            return $token;
        }

        return false;
    }

    public function create_token($payload)
    {
        return $this->encode($payload);
    }

    public function verify($token)
    {
        $user = $this->decode($token);

        if (!$user) {
            return false;
        }
        $this->user = $this->CI->student_model->get($user->id);
        $this->check = true;

        return true;
    }

    public function decode($token)
    {
        $segments = explode('.', $token);
        
        if (count($segments) != 3) {
            return false;
        }

        list($header_encode, $payload_encode, $sign) = $segments;
        
        $header = json_decode($this->base64url_decode($header_encode));
        $payload = json_decode($this->base64url_decode($payload_encode));
        
        if (!isset($header->alg)) {
            return false;
        }

        if ($sign != $this->base64url_encode(($this->create_sign("$header_encode.$payload_encode", $header->alg)))) {
            return false;
        }

        return $payload;
    }

    public function encode($payload, $algo = 'HS256')
    {
        $header = array(
            'type' => 'JWT',
            'alg' => $algo,
        );

        $header_encode = $this->base64url_encode(json_encode($header));
        $payload_encode = $this->base64url_encode(json_encode($payload));
        $sign = $this->base64url_encode($this->create_sign("$header_encode.$payload_encode", $algo));

        return "$header_encode.$payload_encode.$sign";
    }

    private function create_sign($input, $method = 'HS256')
    {
        $methods = array(
            'HS256' => 'sha256',
            'HS384' => 'sha384',
            'HS512' => 'sha512',
        );
        
        if (empty($methods[$method])) {
            show_error('Algorithm not supported');
            log_message('ERROR', 'Algorithm not supported');
        }

        return hash_hmac($methods[$method], $input, $this->secret_key, true);
    }

    private function base64url_encode($data)
    { 
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
    }

    private function base64url_decode($data)
    {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)); 
    }
}
