<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gate
{
    private $CI;

    protected $gate = array();

    protected $allow = array();

    // dev
    protected $callback = array();

    /**
     * User
     * @var stdClass User
     */
    protected $user = array();

    public function __construct()
    {
        $this->CI =& get_instance();
    }

    public function define($gate, $name, $callback = '')
    {
        $this->gate[$gate] = $name;

        // dev
        if (is_callable($callback)) {
            $this->callback[$gate] = $callback;
        }
    }

    public function get($gate)
    {
        if (isset($this->gate[$gate])) {
            return $this->gate[$gate];
        }

        return null;
    }

    public function all()
    {
        return $this->gate;
    }

    public function set_user($user)
    {
        $this->user = $user;
    }

    public function set_allow($gate_allow)
    {
        $this->allow = $gate_allow;
    }

    public function get_allow()
    {
        return $this->allow;
    }

    /**
     * Kiểm tra xem user này có quyền thực hiện hành động này không
     */
    public function allow($gate, $params = array())
    {
        if ($this->allow == '*') {
            return true;
        }

        if ($this->allow === false) {
            return false;
        }

        if (isset($this->allow[$gate])) {
            if (is_bool($this->allow[$gate])) {
                return $this->allow[$gate];
            }
        }

        return true;
    }
}
