<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Console
{
    public function log($content = "")
    {
        echo $content . PHP_EOL;
    }
}
