<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['file'] = 'File';
$lang['file_directory_has_child'] = 'The folder have childs';
$lang['file_directory_has_file'] = 'The folder have files';
$lang['file_directory_name'] = 'Directory name';
$lang['file_directory_parent'] = 'Directory parent';