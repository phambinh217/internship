<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['auth_forget_password'] = 'Forgot password';
$lang['auth_email'] = 'Email address';
$lang['auth_code'] = 'Student code';
$lang['auth_password'] = 'Password';
$lang['auth_remember_password'] = 'Remember password';
$lang['auth_confirm_password'] = 'Confirm password';
$lang['auth_reset_password'] = 'Confirm password';
$lang['auth_new_password'] = 'Confirm password';
$lang['auth_change_password'] = 'Confirm password';
$lang['auth_register'] = 'Register';
$lang['auth_send_link_reset_password'] = 'Send link reset password';
$lang['auth_login'] = 'Login';

$lang['auth_email_subject_foret_password'] = 'Email reset password';
$lang['auth_could_not_send_email'] = 'Can not send email. Please check your intenet!';
$lang['auth_email_send_success'] = 'Email send succss';
$lang['auth_email_send_fail'] = 'Can not send email';
$lang['auth_email_not_exists'] = 'Email not exists';

$lang['auth_attempt_limit'] = 'You have blocked login. Try after minus';
$lang['auth_account_not_exists'] = 'Student code or password is incorrect';
$lang['auth_attempt_fail'] = 'Login fail';
$lang['auth_attempt_success'] = 'Login success';
$lang['auth_new_login'] = 'New login';
$lang['auth_alert_email_send_success'] = 'Alert email send success';
$lang['auth_alert_email_send_fail'] = 'Can not send email';

$lang['auth_change_passowrd_success'] = 'Changed password';
$lang['auth_change_passowrd_fail'] = 'Change fail';
$lang['auth_token_not_exists'] = 'Token not exists';

$lang['auth_register_success'] = 'Register success';
$lang['auth_message_register_disable'] = 'We turn off register function. Login please';