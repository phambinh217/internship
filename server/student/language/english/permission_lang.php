<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['permission_setting'] = 'Change website setting';
$lang['permission_user_read'] = 'Read user and role';
$lang['permission_user_edit'] = 'Edit user';
$lang['permission_user_create'] = 'Create user';
$lang['permission_user_delete'] = 'Delete user';
$lang['permission_user_login_as'] = 'Login as user';
$lang['permission_role_edit'] = 'Edit role';
$lang['permission_role_create'] = 'Create role';
$lang['permission_role_delete'] = 'Delete role';
$lang['permission_log_system_read'] = 'Read log system';
$lang['permission_log_system_delete'] = 'Clear log system';