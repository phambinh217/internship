<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['log_system'] = 'Log system';
$lang['log_system_status'] = 'Status';
$lang['log_system_message'] = 'Message';
$lang['log_system_file_delete'] = 'Delete log files';