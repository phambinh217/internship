<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['admin'] = 'Admin';
$lang['back'] = 'Back';
$lang['cancel'] = 'Cancel';
$lang['save'] = 'Save';
$lang['saved'] = 'Saved';
$lang['edit'] = 'Edit';
$lang['add_new'] = 'Add new';
$lang['remove'] = 'Remove';
$lang['removed'] = 'Removed';
$lang['import'] = 'Import';
$lang['export'] = 'Export';
$lang['updated_at'] = 'Updated at';
$lang['action'] = 'Action';
$lang['detail'] = 'Detail';
$lang['success'] = 'Success';
$lang['error'] = 'Error';
$lang['fail'] = 'Fail';
$lang['close'] = 'Close';    
$lang['download'] = 'Download';
$lang['publish'] = 'Publish';
$lang['hide'] = 'Hide';
$lang['are_you_sure'] = 'Are you sure?';
$lang['view'] = 'View';
$lang['create'] = 'Create';
$lang['yes'] = 'Yes';
$lang['no'] = 'No';

$lang['home_page'] = 'Home page';
$lang['profile'] = 'Your profile';
$lang['profile_info'] = 'Profile info';
$lang['change_password'] = 'Change login password';
$lang['logout'] = 'Logout';
$lang['form'] = 'Forms';

$lang['online'] = 'Online';
$lang['offline'] = 'Offline';
$lang['users'] = 'Users';

$lang['no_internet_access'] = 'No internet access';
$lang['internet_access'] = 'Intenet access';

$lang['setting'] = 'Setting';
$lang['log_system'] = 'Log system';
$lang['log_system_no_item_today'] = 'Very good. No logs today!';
$lang['visit_site'] = 'Visit site';

$lang['enable'] = 'Enable';
$lang['disable'] = 'Disable';

$lang['demo_mode_not_allow'] = 'Not allow in demo mode';