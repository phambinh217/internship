<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (ENVIRONMENT != 'production') {
    $route['make:controller/(.+)'] = 'command/Command_make_controller/controller/$1';
    $route['make:model/(.+)'] = 'command/Command_make_controller/model/$1';
    $route['make:helper/(:any)'] = 'command/Command_make_controller/helper/$1';
    $route['make:library/(:any)'] = 'command/Command_make_controller/library/$1';
    $route['make:language/(:any)'] = 'command/Command_make_controller/language/$1';
}