<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['api/login']['post'] = 'Auth_controller/login';
$route['api/register']['post'] = 'Auth_controller/register';
$route['api/forgot-password']['post'] = 'Auth_controller/forgot_password';
$route['api/reset-password']['post'] = 'Auth_controller/reset_password';
$route['api/me']['get'] = 'Me_controller/show';
$route['api/me/update']['post'] = 'Me_controller/update/$1';
$route['api/me/update-password']['post'] = 'Me_controller/update_password/$1';

$route['api/photos/upload']['post'] = 'api/Photo_controller/upload';

$route['api/configurations/autoload']['get'] = 'Config_controller/index';
$route['api/configurations/save']['post'] = 'Config_controller/save';
$route['api/configurations/(:any)']['get'] = 'Config_controller/bynamespace/$1';

$route['api/internships']['get'] = 'Internship_controller/index';