<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['photos/resize/(:num)x(:num)/(.+)'] = 'Photo_controller/resize/$1/$2/$3';