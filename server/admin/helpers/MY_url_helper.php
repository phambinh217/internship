<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('url')) {
    function url($uri, $params = array(), $protocol = null)
    {
        $query_string = null;

        $CI =& get_instance();

        $uri = rtrim($uri, '/');

        if ($params) {
            foreach ($params as $key => $value) {
                $query_string .= '&'.$key.'='.$value;
            }
            $query_string = '?' . ltrim($query_string, '&');
        }

        return get_instance()->config->base_url($uri . $query_string, $protocol);
    }
}

if (!function_exists('catalog_url')) {
    function catalog_url($uri, $params = array(), $protocol = null)
    {
        $CI =& get_instance();

        $url = url($uri, $params, $protocol);
        $catalog_url = $CI->config->item('catalog_url');
        
        return str_replace(base_url(), $catalog_url, $url);
    }
}
