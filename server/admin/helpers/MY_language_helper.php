<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('trans')) {
    function trans($line, $params = array())
    {
        $keys = array();
        foreach ($params as $key => $val) {
            $keys['{'. $key .'}'] = $val;
        }

        return str_replace(array_keys($keys), array_values($keys), lang($line));
    }
}