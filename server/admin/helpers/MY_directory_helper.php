<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (! function_exists('directory_map_full')) {
    function directory_map_full($source_dir, $directory_depth = 0, $hidden = FALSE)
    {
        if ($fp = @opendir($source_dir)) {
            $filedata   = array();
            $new_depth  = $directory_depth - 1;
            $source_dir = rtrim($source_dir, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;

            while (FALSE !== ($file = readdir($fp))) {
                // Remove '.', '..', and hidden files [optional]
                if ($file === '.' OR $file === '..' OR ($hidden === FALSE && $file[0] === '.')) {
                    continue;
                }

                is_dir($source_dir.$file) && $file .= DIRECTORY_SEPARATOR;

                if (($directory_depth < 1 OR $new_depth > 0) && is_dir($source_dir.$file)) {
                    $filedata = array_merge($filedata, directory_map_full($source_dir.$file, $new_depth, $hidden));
                } else {
                    $filedata[] = $source_dir . $file;
                }
            }

            closedir($fp);
            return $filedata;
        }

        return FALSE;
    }
}

if (!function_exists('rrmdir')) {
    function rrmdir($src)
    {
        $dir = opendir($src);
        while(false !== ($file = readdir($dir))) {
            if (( $file != '.' ) && ( $file != '..' )) {
                $full = $src . '/' . $file;
                if ( is_dir($full) ) {
                    rrmdir($full);
                } else {
                    unlink($full);
                }
            }
        }

        closedir($dir);
        rmdir($src);
    }
}