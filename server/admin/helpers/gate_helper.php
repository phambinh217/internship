<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('can')) {
    function can($gate)
    {
        $CI =& get_instance();
        return call_user_func_array(array($CI->gate, 'allow'), array($gate));
    }
}
