<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('image_resize')) {
    function image_resize($image, $width, $height)
    {
        $image_path = PUBLICPATH . $image;
        
        if (!file_exists($image_path)) {
            return image_resize('storage/image-not-found.png', $width, $height);
        }

        $dest_image = 'temp/cache/' . substr($image, 0, strrpos($image, '.')) . '_' .$width . 'x' . $height . '.' . pathinfo($image_path, PATHINFO_EXTENSION);
        $new_image = ROOTPATH . $dest_image;
        $dest_path = dirname($new_image);

        if (!is_dir($dest_path)) {
            mkdir($dest_path, 0777, true);
        }

        if (!file_exists($new_image)) {
            $CI =& get_instance();

            $config = array(
                'image_library'     => 'gd2',
                'maintain_ratio'    => true,
                'create_thumb'      => false,
                'source_image'      => $image_path,
                'width'             => $width,
                'height'            => $height,
                'new_image'         => $new_image,
            );
            $CI->load->library('image_lib');
            $CI->image_lib->initialize($config);
            $CI->image_lib->resize();
        }

        return $dest_image;
    }
}
