<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['from'] = EMAIL_FROM;
$config['name'] = EMAIL_NAME;
$config['protocol'] = 'smtp';
$config['smtp_host'] = 'tls://smtp.gmail.com';
$config['smtp_user'] = EMAIL_USER;
$config['smtp_pass'] = EMAIL_PASSWORD;
$config['smtp_port'] = EMAIL_PORT;
$config['mailtype'] = 'html';
$config['charset'] = 'utf-8';
$config['newline'] = "\r\n";
$config['wordwrap'] = true;
