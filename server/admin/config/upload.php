<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['upload_path']          = FCPATH .'storage/upload/';
$config['allowed_types']        = 'gif|jpg|png|jpeg';
$config['max_size']             = 1204 * 5 * 100; // 500Mb
$config['max_width']            = 1024 * 5;
$config['max_height']           = 768 * 5;
