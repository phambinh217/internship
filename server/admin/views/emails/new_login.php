<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title></title>

    <style type="text/css">

</style>    
</head>
<body style="margin:0; padding:40px 0; background-color:#F2F2F2; font-family: sans-serif;">
    <center>
        <table width="600px" border="0" cellpadding="0" cellspacing="0" style="border: 1px solid #d9d9d9;padding: 15px;background: #fff;">
            <tr>
                <td align="center" valign="top">
                    <h1 align="left" style="margin:0; padding:0; margin-bottom:15px;">New login</h1>
                    <p align="left" style="margin:0; padding:0; margin-bottom:15px;">Hi <?php echo $user->lastname ?> <?php echo $user->firstname ?>!. You have new login to <?php echo $this->config->item('app_name') ?> system</p>
                </td>
            </tr>
            <tr>
                <td align="center" valign="top">
                    <table width="640" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px; width:100%; margin-bottom: 15px;" bgcolor="#FFFFFF">
                        <tr>
                            <td align="center" valign="top" style="padding:10px;">

                                <table width="600" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:600px; width:100%;">
                                    <tr>
                                        <td width="300" align="left" valign="top" style="padding:10px; border-bottom: 1px solid #d9d9d9;">
                                            IP address
                                        </td>
                                        <td width="300" align="left" valign="top" style="padding:10px; border-bottom: 1px solid #d9d9d9;">
                                            <?php echo $ip ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="300" align="left" valign="top" style="padding:10px; border-bottom: 1px solid #d9d9d9;">
                                            Login at
                                        </td>
                                        <td width="300" align="left" valign="top" style="padding:10px; border-bottom: 1px solid #d9d9d9;">
                                            <?php echo $logged_in_at ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="300" align="left" valign="top" style="padding:10px; border-bottom: 1px solid #d9d9d9;">
                                            OS
                                        </td>
                                        <td width="300" align="left" valign="top" style="padding:10px; border-bottom: 1px solid #d9d9d9;">
                                            <?php echo $platform ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="300" align="left" valign="top" style="padding:10px; border-bottom: 1px solid #d9d9d9;">
                                            Device
                                        </td>
                                        <td width="300" align="left" valign="top" style="padding:10px; border-bottom: 1px solid #d9d9d9;">
                                            <?php echo $device_access ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="300" align="left" valign="top" style="padding:10px; border-bottom: 1px solid #d9d9d9;">
                                            Browser
                                        </td>
                                        <td width="300" align="left" valign="top" style="padding:10px; border-bottom: 1px solid #d9d9d9;">
                                            <?php echo $browser ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" valign="top" style="padding: 20px 0;">
                    <p style="margin:0; padding:0; margin-bottom:15px;">
                        Email is automatically sent from the <a href="<?php echo catalog_url('/') ?>" target="_blank" style="color:#66cccc; text-decoration:underline;"><?php echo $this->config->item('app_name') ?></a> system
                    </p>
                </td>
            </tr>
        </table>
    </center>
</body>
</html>
