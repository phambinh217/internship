<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['api/login']['post'] = 'Auth_controller/login';
$route['api/register']['post'] = 'Auth_controller/register';
$route['api/forgot-password']['post'] = 'Auth_controller/forgot_password';
$route['api/reset-password']['post'] = 'Auth_controller/reset_password';
$route['api/me']['get'] = 'Me_controller/show';
$route['api/me/permissions']['get'] = 'Me_controller/permission';
$route['api/me/update']['post'] = 'Me_controller/update/$1';
$route['api/me/update-password']['post'] = 'Me_controller/update_password/$1';

$route['api/photos/upload']['post'] = 'api/Photo_controller/upload';

$route['api/reports/dashboard']['get'] = 'Report_controller/dashboard';

$route['api/configurations/autoload']['get'] = 'Config_controller/index';
$route['api/configurations/save']['post'] = 'Config_controller/save';
$route['api/configurations/(:any)']['get'] = 'Config_controller/bynamespace/$1';

$route['api/logs-system']['get'] = 'Log_system_controller/index';
$route['api/logs-system/([0-9]{4})-([0-1][0-9])-([0-3][0-9])']['get'] = 'Log_system_controller/show/$1-$2-$3';
$route['api/logs-system/delete']['post'] = 'Log_system_controller/delete';

$route['api/users']['get'] = 'User_controller/index';
$route['api/users/store']['post'] = 'User_controller/store';
$route['api/users/(:num)/update']['post'] = 'User_controller/update/$1';
$route['api/users/(:num)']['get'] = 'User_controller/show/$1';
$route['api/users/(:num)/delete']['post'] = 'User_controller/delete/$1';

$route['api/roles']['get'] = 'Role_controller/index';
$route['api/roles/store']['post'] = 'Role_controller/store';
$route['api/roles/(:num)/update']['post'] = 'Role_controller/update/$1';
$route['api/roles/(:num)']['get'] = 'Role_controller/show/$1';
$route['api/roles/(:num)/delete']['post'] = 'Role_controller/delete/$1';

$route['api/permissions'] = 'Permission_controller/index';

$route['api/files/(:num)/delete']['post'] = 'File_controller/delete/$1';
$route['api/files/browser'] = 'File_browser_controller/index';
$route['api/files/directories'] = 'File_directory_controller/index';
$route['api/files/directories/(:num)/upload']['post'] = 'File_controller/upload/$1';
$route['api/files/directories/store']['post'] = 'File_directory_controller/store';
$route['api/files/directories/(:num)/update']['post'] = 'File_directory_controller/update/$1';
$route['api/files/directories/(:num)/delete']['post'] = 'File_directory_controller/delete/$1';

$route['api/students']['get'] = 'Student_controller/index';
$route['api/students/store']['post'] = 'Student_controller/store';
$route['api/students/(:num)/update']['post'] = 'Student_controller/update/$1';
$route['api/students/(:num)']['get'] = 'Student_controller/show/$1';
$route['api/students/(:num)/delete']['post'] = 'Student_controller/delete/$1';

$route['api/lecturers']['get'] = 'Lecturer_controller/index';
$route['api/lecturers/store']['post'] = 'Lecturer_controller/store';
$route['api/lecturers/(:num)/update']['post'] = 'Lecturer_controller/update/$1';
$route['api/lecturers/(:num)']['get'] = 'Lecturer_controller/show/$1';
$route['api/lecturers/(:num)/delete']['post'] = 'Lecturer_controller/delete/$1';

$route['api/terms']['get'] = 'Term_controller/index';
$route['api/terms/store']['post'] = 'Term_controller/store';
$route['api/terms/(:num)/update']['post'] = 'Term_controller/update/$1';
$route['api/terms/(:num)']['get'] = 'Term_controller/show/$1';
$route['api/terms/(:num)/delete']['post'] = 'Term_controller/delete/$1';

$route['api/classes']['get'] = 'Class_controller/index';
$route['api/classes/store']['post'] = 'Class_controller/store';
$route['api/classes/(:num)/update']['post'] = 'Class_controller/update/$1';
$route['api/classes/(:num)']['get'] = 'Class_controller/show/$1';
$route['api/classes/(:num)/delete']['post'] = 'Class_controller/delete/$1';
$route['api/classes/sort-order']['post'] = 'Class_controller/sort_order';

$route['api/faculties']['get'] = 'Faculty_controller/index';
$route['api/faculties/store']['post'] = 'Faculty_controller/store';
$route['api/faculties/(:num)/update']['post'] = 'Faculty_controller/update/$1';
$route['api/faculties/(:num)']['get'] = 'Faculty_controller/show/$1';
$route['api/faculties/(:num)/delete']['post'] = 'Faculty_controller/delete/$1';
$route['api/faculties/sort-order']['post'] = 'Faculty_controller/sort_order';

$route['api/majors']['get'] = 'Major_controller/index';
$route['api/majors/store']['post'] = 'Major_controller/store';
$route['api/majors/(:num)/update']['post'] = 'Major_controller/update/$1';
$route['api/majors/(:num)']['get'] = 'Major_controller/show/$1';
$route['api/majors/(:num)/delete']['post'] = 'Major_controller/delete/$1';
$route['api/majors/sort-order']['post'] = 'Major_controller/sort_order';

$route['api/internships']['get'] = 'Internship_controller/index';
$route['api/internships/store']['post'] = 'Internship_controller/store';
$route['api/internships/(:num)/update']['post'] = 'Internship_controller/update/$1';
$route['api/internships/(:num)']['get'] = 'Internship_controller/show/$1';
$route['api/internships/(:num)/delete']['post'] = 'Internship_controller/delete/$1';
$route['api/internships/(:num)/lecturers']['get'] = 'Internship_lecturer_controller/index/$1';
$route['api/internships/(:num)/lecturers/(:num)/full-student'] = 'Internship_lecturer_controller/full_student/$1/$2';
$route['api/internships/(:num)/statistic']['get'] = 'Internship_controller/statistic/$1';

$route['api/internships/(:num)/students']['get'] = 'Internship_student_controller/index/$1';
$route['api/internships/(:num)/students/store']['post'] = 'Internship_student_controller/store/$1';
$route['api/internships/(:num)/students/(:num)'] = 'Internship_student_controller/show/$1/$2';
$route['api/internships/(:num)/students/(:num)/exists'] = 'Internship_student_controller/exists/$1/$2';
$route['api/internships/(:num)/students/(:num)/update']['post'] = 'Internship_student_controller/update/$1/$2';
$route['api/internships/(:num)/students/(:num)/delete']['post'] = 'Internship_student_controller/delete/$1/$2';
$route['api/internships/(:num)/students/count-by-date']['get'] = 'Internship_student_controller/students_count_by_date/$1';

