<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class File_browser_controller extends MY_Controller
{
    public function index()
    {
        $this->load->model('file_browser_model');
        $response = $this->file_browser_model->paginate($this->input->get());

        $this->output->json($response, 200);
    }
}