<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class File_directory_controller extends MY_Controller
{
    public function index()
    {
        $this->load->model('file_directory_model');
        $response = $this->file_directory_model->paginate($this->input->get());

        $this->output->json($response, 200);
    }

    public function store()
    {
        $this->lang->load('form_validation');

        $response = array();
        $validator = $this->validator_store($this->input->post());
        $header_status = 200;

        if ($validator->run() == false) {
            $response = array(
                'success' => false,
                'message' => trans('form_validation_check_the_fields'),
                'errors' => $validator->get_errors_array(),
            );
             $header_status = 406;
        } else {
            $this->load->model('file_directory_model');
            $directory = $this->file_directory_model->create($this->input->post());
            $response = array(
                'success' => true,
                'message' => trans('success'),
                'directory' => $directory
            );
        }

        $this->output->json($response, $header_status);
    }

    public function update($directory_id)
    {
        $this->lang->load(array('file', 'form_validation'));

        $response = array();
        $validator = $this->validator_update($this->input->post(), $directory_id);
        $header_status = 200;

        if ($validator->run() == false) {
            $response = array(
                'success' => false,
                'message' => trans('form_validation_check_the_fields'),
                'errors' => $validator->get_errors_array(),
            );
            $header_status = 406;
        } else {
            $this->load->model('file_directory_model');
            $directory = $this->file_directory_model->update($this->input->post(), $directory_id, array(
                'success' => false,
                'message' => 'Directory not found'
            ));
            
            $response = array(
                'success' => true,
                'message' => trans('success'),
                'directory' => $directory
            );
        }

        $this->output->json($response, $header_status);
    }

    public function delete($directory_id)
    {
        $this->lang->load('file');
        $response = array();
        $errors = array();
        $header_status = 200;

        $this->load->model('file_directory_model');
        $directory = $this->file_directory_model->with(array('total_file', 'total_child'))->get($directory_id);
        
        if ($directory->total_child != 0) {
            $errors['has_child'] = trans('file_directory_has_child');
        } elseif ($directory->total_file != 0) {
            $errors['has_file'] = trans('file_directory_has_file');
        }

        if (!$errors) {
            $this->file_directory_model->delete($directory_id);
            $response = array(
                'success' => true,
                'message' => trans('success'),
                'directory_id' => $directory_id,
            );
        } else {
            $response = array(
                'success' => false,
                'message' => trans('fail'),
                'errors' => $errors,
            );
            $header_status = 406;
        }

        $this->output->json($response, $header_status);
    }

    protected function validator_store(array $data)
    {
        $this->lang->load('file');
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('name', trans('file_directory_name'), 'required|max_length[255]');
        $this->form_validation->set_rules('parent_id', trans('file_directory_parent'), 'integer');
        
        return $this->form_validation;
    }

    protected function validator_update(array $data, $directory_id)
    {
        $this->lang->load('file');
        $this->load->library('form_validation');

        if (isset($data['name'])) {
            $this->form_validation->set_rules('name', trans('file_directory_name'), 'required|max_length[255]');
        }

        if (isset($data['parent_id'])) {
            $this->form_validation->set_rules('parent_id', trans('file_directory_parent'), 'integer|exists[file_directories.id]');
        }

        return $this->form_validation;
    }
}
