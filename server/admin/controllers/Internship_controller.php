<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Internship_controller extends MY_Controller
{
    public function index()
    {
        $with = (array)$this->input->get('with');
        $this->load->model('internship_model');
        $filter = $this->input->get();
        $response = $this->internship_model->paginate($filter);

        if (in_array('lecturer', $with)) {
            $internship_ids = array_pluck($response->data, 'id');
            $lecturers = $this->internship_model->get_lecturers_in($internship_ids);

            foreach ($response->data as &$internship) {
                $internship->lecturer = new stdClass();
                $internship->lecturer->data = $lecturers[$internship->id];
                $internship->lecturer->total = count($lecturers[$internship->id]);
            }
        }

        $this->output->json($response, 200);
    }

    public function show($id)
    {
        $this->load->model('internship_model');
        $with = (array)$this->input->get('with');

        $internship = $this->internship_model->get_or_fail($id, array(
            'success' => false,
            'message' => 'Not found'
        ));

        if (in_array('object', $with)) {
            $data = $this->internship_model->get_objects($internship->id, $internship->type);
            $object = new stdClass();
            $object->data = $data;
            $object->total = count($data);

            if ($internship->type == 2) {
                $internship->faculties = $object;
            } elseif ($internship->type == 3) {
                $internship->majors = $object;
            } elseif ($internship->type == 4) {
                $internship->classes = $object;
            } elseif ($internship->type == 5) {
                $internship->students = $object;
            }
        }

        if (in_array('lecturer', $with)) {
            $data = $this->internship_model->get_lecturers($internship->id);
            $lecturers = new stdClass();
            $lecturers->data = $data;
            $lecturers->total = count($data);
            $internship->lecturers = $lecturers;
        }

        $response = array(
            'success' => true,
            'message' => 'OK',
            'internship' => $internship,
        );

        $this->output->json($response, 200);
    }

    public function statistic($id)
    {
        $student_today = $this->db
            ->where('internship_id', $id)
            ->where('created_at >=', date('Y-m-d 00:00:00'))
            ->where('created_at <=', date('Y-m-d 59:59:59'))
            ->from('student_internships')
            ->count_all_results();

        $total_student = $this->db
            ->where('internship_id', $id)
            ->from('student_internships')
            ->count_all_results();

        $total_lecturer = $this->db
            ->where('internship_id', $id)
            ->from('internship_lecturers')
            ->count_all_results();

        $day_left = $this->db->select('DATEDIFF(date_close, NOW()) as day_left')
            ->where('id', $id)
            ->get('internships')
            ->first_row()
            ->day_left;

        $response = array(
            'success' => true,
            'message' => 'Success',
            'data' => array(
                'student_today' => $student_today,
                'total_student' => $total_student,
                'total_lecturer' => $total_lecturer,
                'day_left' => $day_left
            )
        );
        return $this->output->json($response, 200);
    }

    public function store()
    {
        $data = $this->input->post();
        $validator = $this->validate_store($data);
        $response = array();

        if (!$validator->run()) {
            $response = array(
                'success' => false,
                'message' => 'Fail',
                'errors' => $validator->get_errors_array(),
            );
            $header_status = 406;
        } else {
            $this->load->model('internship_model');
            $internship = $this->internship_model->create($data);
            $response = array(
                'success' => true,
                'message' => 'Success',
                'internship' => $internship,
            );
            $header_status = 200;
        }

        $this->output->json($response, $header_status);
    }

    public function update($id)
    {
        $data = $this->input->post();
        $validator = $this->validate_update($data, $id);
        $response = array();

        if (!$validator->run()) {
            $response = array(
                'success' => false,
                'message' => 'Error',
                'errors' => $validator->get_errors_array(),
            );
            $header_status = 406;
        } else {
            $this->load->model('internship_model');
            $internship = $this->internship_model->update_or_fail($data, $id, array(
                'success' => false,
                'message' => 'Not found'
            ));
            $response = array(
                'success' => true,
                'message' => 'Success',
                'internship' => $internship,
            );
            $header_status = 200;
        }

        $this->output->json($response, $header_status);
    }

    public function delete($id)
    {
        $this->load->model('internship_model');
        $this->internship_model->delete($id);

        $response = array(
            'success' => true,
            'message' => 'OK'
        );

        $this->output->json($response, 200);
    }

    public function validate_store(array $data)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);

        $this->form_validation->set_rules('name', 'Name', 'required|max_length[255]');

        $date_open_rules = array('required', 'date_format[d-m-Y]');
        if (isset($data['date_close'])) {
            $date_close = $data['date_close'];
            $date_open_rules[] = 'date_before[' . $date_close . ']';
        }
        $this->form_validation->set_rules('date_open', 'Date open', implode('|', $date_open_rules));

        $date_close_rules = array('required', 'date_format[d-m-Y]');
        if (isset($data['date_open'])) {
            $date_open = $data['date_open'];
            $date_close_rules[] = 'date_after[' . $date_open . ']';
        }
        $this->form_validation->set_rules('date_close', 'Date close', implode('|', $date_close_rules));

        $this->form_validation->set_rules('type', 'Type', 'required|in_list[1,2,3,4,5]');

        if (isset($data['type'])) {
            switch ($data['type']) {
                case 1:
                    $this->form_validation->set_rules('term', 'Term', 'required|integer|greater_than[0]');
                    break;
                case 2:
                    $this->form_validation->set_rules('term', 'Term', 'required|integer|greater_than[0]');
                    $this->form_validation->set_rules('faculty_id[]', 'Faculty', 'required|exists[faculties.id]');
                    break;
                case 3:
                    $this->form_validation->set_rules('term', 'Term', 'required|integer|greater_than[0]');
                    $this->form_validation->set_rules('major_id[]', 'Major', 'required|exists[majors.id]');
                    break;
                case 4:
                    $this->form_validation->set_rules('term', 'Term', 'required|integer|greater_than[0]');
                    $this->form_validation->set_rules('class_id[]', 'Class', 'required|exists[classes.id]');
                    break;
                case 5:
                    $this->form_validation->set_rules('student_id[]', 'Students', 'required|exists[students.id]');
                    break;
            }
        }

        $this->form_validation->set_rules('lecturer[]', 'Lecturer', 'required|is_array');

        if (isset($data['lecturer']) && is_array($data['lecturer'])) {
            foreach ($data['lecturer'] as $i => $lecturer) {
                $this->form_validation->set_rules('lecturer['. $i .'][id]', 'Lecturer', 'required|integer|exists[lecturers.id]');
                $this->form_validation->set_rules('lecturer['. $i .'][limit_student]', 'Lecturer', 'required|integer');
            }
        }

        return $this->form_validation;
    }

    public function validate_update(array $data, $id)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);

        if (isset($data['name'])) {
            $this->form_validation->set_rules('name', 'Name', 'required|max_length[255]');
        }

        if (isset($data['date_open'])) {
            $date_open_rules = array('required', 'date_format[d-m-Y]');
            if (isset($data['date_close'])) {
                $date_close = $data['date_close'];
                $date_open_rules[] = 'date_before[' . $date_close . ']';
            } else {
                $this->load->model('internship_model');
                $internship = $this->internship_model->get_or_fail($id, array(
                    'success' => false,
                    'message' => 'Not found',
                ));
                $date_close = date_format(date_crealte($internship->date_close), 'd-m-Y');
                $date_open_rules[] = 'date_before[' . $date_close . ']';
            }

            $this->form_validation->set_rules('date_open', 'Date open', implode('|', $date_open_rules));
        }

        if (isset($data['date_close'])) {
            $date_close_rules = array('required', 'date_format[d-m-Y]');
            if (isset($data['date_open'])) {
                $date_open = $data['date_open'];
                $date_close_rules[] = 'date_after[' . $date_open . ']';
            } else {
                // may be internship create before
                if (!isset($internship)) {
                    $this->load->model('internship_model');
                    $internship = $this->internship_model->get_or_fail($id, array(
                        'success' => false,
                        'message' => 'Not found',
                    ));
                }
                $date_open = date_format(date_crealte($internship->date_open), 'd-m-Y');
                $date_close_rules[] = 'date_after[' . $date_open . ']';
            }
            $this->form_validation->set_rules('date_close', 'Date close', implode('|', $date_close_rules));
        }

        if (isset($data['type'])) {
            $this->form_validation->set_rules('type', 'Type', 'required|in_list[1,2,3,4,5]');
            switch ($data['type']) {
                case 1:
                    $this->form_validation->set_rules('term', 'Term', 'required|integer|greater_than[0]');
                    break;
                case 2:
                    $this->form_validation->set_rules('term', 'Term', 'required|integer|greater_than[0]');
                    $this->form_validation->set_rules('faculty_id[]', 'Faculty', 'required|exists[faculties.id]');
                    break;
                case 3:
                    $this->form_validation->set_rules('term', 'Term', 'required|integer|greater_than[0]');
                    $this->form_validation->set_rules('major_id[]', 'Major', 'required|exists[majors.id]');
                    break;
                case 4:
                    $this->form_validation->set_rules('term', 'Term', 'required|integer|greater_than[0]');
                    $this->form_validation->set_rules('class_id[]', 'Class', 'required|exists[classes.id]');
                    break;
                case 5:
                    $this->form_validation->set_rules('student_id[]', 'Students', 'required|exists[students.id]');
                    break;
            }
        }

        if (isset($data['lecturer'])) {
            $this->form_validation->set_rules('lecturer[]', 'Lecturer', 'required|is_array');

            if (is_array($data['lecturer'])) {
                foreach ($data['lecturer'] as $i => $lecturer) {
                    $this->form_validation->set_rules('lecturer['. $i .'][id]', 'Lecturer', 'required|integer|exists[lecturers.id]');
                    $this->form_validation->set_rules('lecturer['. $i .'][limit_student]', 'Lecturer', 'required|integer');
                }
            }
        }

        return $this->form_validation;
    }
}
