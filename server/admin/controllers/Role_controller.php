<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role_controller extends MY_Controller
{
    public function index()
    {
        $this->load->model('role_model');
        $filter = $this->input->get();
        $response = $this->role_model->paginate($filter);

        $this->output->json($response, 200);
    }

    public function show($id)
    {
        $this->load->model('role_model');
        $filter = $this->input->get();
        $role = $this->role_model->get_or_fail($id, array(
            'success' => false,
            'message' => 'Role not found'
        ));

        $response = array(
            'success' => true,
            'message' => trans('success'),
            'role' => $role,
        );
        
        $this->output->json($response, 200);
    }

    public function store()
    {
        $this->lang->load(array('form_validation', 'user'));

        $data = $this->input->post();
        $validator = $this->validate_store($data);
        $response = array();

        if (!$validator->run()) {
            $response = array(
                'success' => false,
                'message' => trans('form_validation_check_the_fields'),
                'errors' => $validator->get_errors_array(),
            );
            $header_status = 406;
        } else {
            $this->load->model('role_model');
            $role = $this->role_model->create($data);
            $response = array(
                'success' => true,
                'message' => trans('success'),
                'role' => $role,
            );
            $header_status = 200;
        }

        $this->output->json($response, $header_status);
    }

    public function update($id)
    {
        $this->lang->load(array('form_validation', 'user'));
        
        $data = $this->input->post();
        $validator = $this->validate_update($data, $id);
        $response = array();
        $header_status = 200;

        if (!$validator->run()) {
            $response = array(
                'success' => false,
                'message' => trans('form_validation_check_the_fields'),
                'errors' => $validator->get_errors_array(),
            );
            $header_status = 406;
        } else {
            $this->load->model('role_model');
            $role = $this->role_model->update_or_fail($data, $id, array(
                'success' => false,
                'message' => 'Role not found',
            ));
            $response = array(
                'success' => false,
                'message' => trans('success'),
                'role' => $role,
            );
        }

        $this->output->json($response, $header_status);
    }

    public function delete($id)
    {
        $this->lang->load('user');

        $this->load->model('role_model');
        $role = $this->role_model->with(array('total_user'))->get($id);

        $response = array();
        $errors = array();
        $header_status = 200;

        if ($role->total_user != 0) {
            $errors['role_has_user'] = trans('role_has_user');
        }

        if (!$errors) {
            $this->role_model->delete($id);
            $response = array(
                'success' => true,
                'message' => trans('success'),
                'role' => $role,
            );
        } else {
            $response = array(
                'success' => false,
                'message' => trans('fail'),
                'errors' => $errors,
            );
            $header_status = 406;
        }

        if ($this->input->post('redirect')) {
            $response['redirect'] = $this->input->post('redirect');
        }

         $this->output->json($response, $header_status);
    }

    public function validate_store(array $data)
    {
        $this->lang->load('user');
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('name', trans('role_name'), 'required|max_length[255]');
        $this->form_validation->set_rules('type', trans('role_type'), 'required|in_list[*,option,0]');
        
        if (isset($data['type'])) {
            if ($data['type'] == 'option') {
                $this->form_validation->set_rules('permission[]', trans('role_permission'), 'required');        
            }
        }
        
        return $this->form_validation;
    }

    public function validate_update(array $data, $id)
    {
        $this->lang->load('user');
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);
        if (isset($data['name'])) {
            $this->form_validation->set_rules('name', trans('role_name'), 'required|max_length[255]');
        }

        if (isset($data['type'])) {
            $this->form_validation->set_rules('type', trans('role_type'), 'required|in_list[*,option,0]');
        }
        
        if (isset($data['type'])) {
            if ($data['type'] == 'option') {
                $this->form_validation->set_rules('permission[]', trans('role_permission'), 'required');        
            }
        }

        return $this->form_validation;
    }
}
