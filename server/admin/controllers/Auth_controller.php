<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_controller extends MY_Controller
{
    public function login()
    {
        $this->lang->load(array('auth', 'form_validation'));
        $validator = $this->validate_login($this->input->post());
        $response = array();
        $header_status = 200;
        $errors = array();
        $token = null;

        if (!$validator->run()) {
            $errors = $validator->get_errors_array();
        }

        if (!$errors) {
            $email = $this->input->post('email');
            $password = md5(md5($this->input->post('password')));
            $token = $this->jwt_auth->attempt($email, $password);
            if (!$token) {
                $errors['email'] = trans('auth_account_not_exists');
            }
        }

        if ($errors) {
            $response = array(
                'success' => false,
                'message' => trans('auth_attempt_fail'),
                'errors' => $errors,
            );
            $header_status = 406;
        } else {
            $this->load->model('user_model');
            $this->load->library('user_agent');

            $last_ip = $_SERVER['REMOTE_ADDR'];
            $last_logged_in = date('Y-m-d H:i:s', time());

            $data = array(
                'time' => $last_logged_in,
                'browser' => $this->agent->browser(),
                'platform' => $this->agent->platform(),
            );
            $this->user_model->push_ip($data, $last_ip, $this->jwt_auth->user()->id);

            $response = array(
                'success' => true,
                'message' => trans('auth_attempt_success'),
                'token' => $token,
            );
        }

        $this->output->json($response, $header_status);
    }

    public function register()
    {
        $this->lang->load(array('auth', 'form_validation'));
        $data = $this->input->post();
        $validator = $this->validate_register($data);
        
        $errors = array();
        $response = array();
        $token = null;
        $header_status = 200;

        if ($validator->run()) {
            $this->load->model('user_model');
            $password = md5(md5($this->input->post('password')));
            $role_id = $this->config_model->get('default_user_role_id', 'config');
            $data['password'] = $password;
            $data['role_id'] = $role_id;
            $user = $this->user_model->create($data);
            $token = $this->jwt_auth->create_token(array(
                'id' => $user->id,
                'email' => $user->email,
                'password' => $password,
            ));
        } else {
            $errors = $validator->get_errors_array();
        }

        if ($errors) {
            $response = array(
                'success'   => false,
                'message'   => trans('form_validation_check_the_fields'),
                'errors'    => $errors,
            );
            $header_status = 406;
        } else {
            $response = array(
                'success'   => true,
                'message'   => trans('auth_register_success'),
                'token' => $token,
            );
        }

        $this->output->json($response, $header_status);
    }

    public function forgot_password()
    {
        $this->lang->load('auth');

        $validator = $this->validate_forgot_password($this->input->post());
        $errors = array();
        $response = array();

        if ($validator->run()) {
            $token = encrypt(time());
            $this->load->model('user_model');
            $user = $this->user_model->get_by_email($this->input->post('email'));
            
            $this->data['user'] = $user;
            $this->data['token'] = $token;

            $this->db->where('email', $user->email)->delete('password_resets');
            $this->db->insert('password_resets', array(
                'email' => $user->email,
                'token' => $token,
            ));

            $this->config->load('email', true);
            $this->load->library('email');
            $this->email->initialize($this->config->item('email'));

            $this->email->from($this->config->item('from', 'email'), $this->config->item('name', 'email'));
            $this->email->to($user->email);
            $this->email->subject(trans('auth_email_subject_foret_password'));

            $this->load->library('user_agent');
            $this->data['ip'] = $_SERVER['REMOTE_ADDR'];
            $this->data['platform'] = $this->agent->platform();
            $this->data['send_at'] = date('d-m-Y H:i:s', time());
            $this->data['browser'] = $this->agent->browser();

            if ($this->agent->is_mobile()) {
                $this->data['device_access'] = $this->agent->mobile();
            } elseif ($this->agent->is_browser()) {
                $this->data['device_access'] = $this->agent->browser();
            } else {
                $this->data['device_access'] = '...';
            }
            $this->email->message($this->load->view('emails/forgot_password', $this->data, true));

            if (!$this->email->send()) {
                $errors['could_not_send_email'] = trans('auth_could_not_send_email');
            }
        } else {
            $errors = $validator->get_errors_array();
        }

        if (!$errors) {
            $response = array(
                'success' => true,
                'message' => trans('auth_email_send_success')
            );
            $header_status = 200;
        } else {
            $response = array(
                'success' => false,
                'message' => trans('auth_email_send_fail'),
                'errors' => $errors,
            );
            $header_status = 406;
        }

        $this->output->json($response, $header_status);
    }

    public function reset_password()
    {
        $this->lang->load('auth');
        $validator = $this->validate_reset_password($this->input->post());

        $errors = array();
        $response = array();

        if ($validator->run()) {
            $this->load->model('user_model');
            $token = str_replace(' ', '+', $this->input->post('token'));
            $reset = $this->db->select('email')->where('token', $token)->get('password_resets')->first_row();
            $user = $this->user_model->get_by_email($reset->email);
            
            $user = $this->user_model->update(array(
                'password' => md5(md5($this->input->post('password')))
            ), $user->id);

            if (!$user) {
                $errors = $this->user_model->get_errors();
            }
        } else {
            $errors = $validator->get_errors_array();
        }

        if (!$errors) {
            $response = array(
                'success' => true,
                'message' => trans('auth_change_passowrd_success'),
            );
            $header_status = 200;
        } else {
            $response = array(
                'success' => true,
                'message' => trans('auth_change_passowrd_fail'),
                'errors' => $errors,
            );
            $header_status = 406;
        }

        $this->output->json($response, $header_status);
    }

    protected function validate_login(array $data)
    {
        $this->lang->load('auth');
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('email', trans('auth_email'), 'required');
        $this->form_validation->set_rules('password', trans('auth_password'), 'required');
        return $this->form_validation;
    }

    protected function validate_register(array $data)
    {
        $this->lang->load('auth');
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('email', trans('auth_email'), 'required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', trans('auth_password'), 'required');
        $this->form_validation->set_rules('password_confirmation', trans('auth_confirm_password'), 'required|matches[password]');
        return $this->form_validation;
    }

    protected function validate_forgot_password(array $data)
    {
        $this->lang->load('auth');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', trans('auth_email'), 'required|callback_email_exists['.(isset($data['email']) ? $data['email'] : '').']');
        $this->form_validation->set_data($data);
        return $this->form_validation;
    }

    public function email_exists($email)
    {
        if (!empty($email)) {
            $this->load->model('user_model');
            $user = $this->user_model->get_by_email($email);

            if (!$user) {
                $this->form_validation->set_message(__FUNCTION__, trans('auth_email_not_exists'));
                return false;
            }
        } else {
            $this->form_validation->set_message(__FUNCTION__, trans('auth_email_not_exists'));
            return false;
        }

        return true;
    }

    protected function validate_reset_password(array $data)
    {
        $this->lang->load('auth');
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('password', trans('auth_new_password'), 'required');
        $this->form_validation->set_rules('password_confirmation', trans('auth_confirm_password'), 'required|matches[password]');
        $this->form_validation->set_rules('token', '', 'required|callback_token_exists['.(isset($data['token']) ? str_replace(' ', '+', $data['token']) : '').']');
        return $this->form_validation;
    }

    public function token_exists($token = '')
    {
        $this->lang->load('auth');
        if (!empty($token)) {
            $token = $this->db->where('token', $token)->get('password_resets')->first_row();
            if (!$token) {
                $this->form_validation->set_message(__FUNCTION__, trans('auth_token_not_exists'));
                return false;
            }
        } else {
            $this->form_validation->set_message(__FUNCTION__, trans('auth_token_not_exists'));
            return false;
        }

        return true;
    }
}
