<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Term_controller extends MY_Controller
{
    public function index()
    {
        $this->load->model('term_model');
        $filter = $this->input->get();
        $response = $this->term_model->paginate($filter);

        $this->output->json($response, 200);
    }

    public function show($id)
    {
        $this->load->model('term_model');
        $term = $this->term_model->get_or_fail($id, array(
            'success' => false,
            'message' => 'Not found'
        ));

        $response = array(
            'success' => true,
            'message' => 'OK',
            'term' => $term,
        );
        
        $this->output->json($response, 200);
    }

    public function store()
    {
        $data = $this->input->post();
        $validator = $this->validate_store($data);
        $response = array();

        if (!$validator->run()) {
            $response = array(
                'success' => false,
                'message' => 'Fail',
                'errors' => $validator->get_errors_array(),
            );
            $header_status = 406;
        } else {
            $this->load->model('term_model');
            $term = $this->term_model->create($data);
            $response = array(
                'success' => true,
                'message' => 'Success',
                'term' => $term,
            );
            $header_status = 200;
        }

        $this->output->json($response, $header_status);
    }

    public function update($id)
    {
        $data = $this->input->post();
        $validator = $this->validate_update($data, $id);
        $response = array();

        if (!$validator->run()) {
            $response = array(
                'success' => false,
                'message' => 'Error',
                'errors' => $validator->get_errors_array(),
            );
            $header_status = 406;
        } else {
            $this->load->model('term_model');
            $term = $this->term_model->update_or_fail($data, $id, array(
                'success' => false,
                'message' => 'Not found'
            ));
            $response = array(
                'success' => true,
                'message' => 'Success',
                'term' => $term,
            );
            $header_status = 200;
        }

        $this->output->json($response, $header_status);
    }

    public function delete($id)
    {
        $this->load->model('term_model');

        $term = $this->term_model->with(array('total_class', 'total_student'))->get($id);

        $errors = array();
        $response = array();
        $header_status = 200;
        
        if ($term->total_class != 0) {
            $errors['term_has_class'] = 'Term have classes';
        }

        if ($term->total_student != 0) {
            $errors['term_has_student'] = 'Term have students';
        }

        if (!$errors) {
            $this->term_model->delete($id);
            $response = array(
                'success' => true,
                'message' => 'Success'
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Fail'
            );
            $header_status = 406;
        }

        $this->output->json($response, $header_status);
    }

    public function validate_store(array $data)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);
        
        $this->form_validation->set_rules('term', 'Term', 'required|is_unique[terms.term]|integer');

        return $this->form_validation;
    }

    public function validate_update(array $data, $id)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);

        if (isset($data['term'])) {
            $this->form_validation->set_rules('term', 'Term', 'required|is_unique[terms.term]|integer');
        }

        return $this->form_validation;
    }
}
