<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class File_controller extends MY_Controller
{
    public function upload($directory_id = 0)
    {
        $response = array();
        $errors = array();
        $header_status = 200;
        $validator = $this->validator_upload($this->input->post());

        if ($validator->run()) {
            $this->config->load('upload', TRUE);
            
            $dest_path =  'storage/upload/' . date('Y-m-d', time()) . '/';

            if (!is_dir(PUBLICPATH . $dest_path)) {
                mkdir(PUBLICPATH . $dest_path, 0777, true);
            }
                
            $upload_config = $this->config->item('upload');
            $upload_config['upload_path'] = PUBLICPATH . $dest_path;
            $upload_config['allowed_types'] = 'gif|jpg|png|jpeg|jp2|jpf|jpg2|jpx|jpm|mj2|mjp2|tiff|tif|bmp|mid|midi|mpga|mp2|mp3|aif|aiff|ram|rm|rpm|ra|wav|m4a|aac|au|ac3|flac|ogg|wma|rv|mpeg|mpg|mpe|qt|mov|avi|movie|3g2|3gp|mp4|f4v|flv|webm|vlc|wmv|doc|docx|dot|dotx|worl|xl|xlsx|xls|text|txt|xml|xsl|zip|rar|css|html|htm|shtml|log';

            $this->load->library('upload');
            $this->upload->initialize($upload_config);

            if (! $this->upload->do_upload('file')) {
                $errors = $this->upload->display_errors();
            } else {
                $file = $this->upload->data();
                $this->load->model('file_model');

                $data = array(
                    'path' => $dest_path . $file['file_name'],
                    'size' => $file['file_size'],
                    'name' => $file['file_name'],
                    'type' => $file['file_type'],
                    'directory_id' => $directory_id,
                );
                $file = $this->file_model->create($data);
                $response['file'] = $file;
            }
        } else {
            $errors = $validator->get_errors_array();
        }

        if (!$errors) {
            $response['success'] = true;
            $response['message'] = trans('success');
        } else {
            $response['success'] = false;
            $response['message'] = trans('fail');
            $response['errors'] = $errors;

            $header_status = 406;
        }

        $this->output->json($response, $header_status);
    }

    public function delete($file_id)
    {
        $response = array();

        $this->load->model('file_model');
        $this->file_model->delete($file_id);
        
        $response = array(
            'success' => true,
            'message' => trans('success'),
            'file_id' => $file_id,
        );
        
        $this->output->json($response, 200);
    }

    protected function validator_upload(array $data)
    {
        $this->lang->load('file');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('file', trans('file'), 'trim');
        $this->form_validation->set_data($data);

        return $this->form_validation;
    }
}
