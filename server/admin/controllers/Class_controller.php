<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Class_controller extends MY_Controller
{
    public function index()
    {
        $this->load->model('class_model');
        $filter = $this->input->get();
        $response = $this->class_model->paginate($filter);

        $this->output->json($response, 200);
    }

    public function show($id)
    {
        $this->load->model('class_model');
        $class = $this->class_model->get_or_fail($id, array(
            'success' => false,
            'message' => 'Not found'
        ));

        $response = array(
            'success' => true,
            'message' => 'OK',
            'class' => $class,
        );
        
        $this->output->json($response, 200);
    }

    public function store()
    {
        $data = $this->input->post();
        $validator = $this->validate_store($data);
        $response = array();

        if (!$validator->run()) {
            $response = array(
                'success' => false,
                'message' => 'Fail',
                'errors' => $validator->get_errors_array(),
            );
            $header_status = 406;
        } else {
            $this->load->model('class_model');
            $class = $this->class_model->create($data);
            $response = array(
                'success' => true,
                'message' => 'Success',
                'class' => $class,
            );
            $header_status = 200;
        }

        $this->output->json($response, $header_status);
    }

    public function update($id)
    {
        $data = $this->input->post();
        $validator = $this->validate_update($data, $id);
        $response = array();

        if (!$validator->run()) {
            $response = array(
                'success' => false,
                'message' => 'Error',
                'errors' => $validator->get_errors_array(),
            );
            $header_status = 406;
        } else {
            $this->load->model('class_model');
            $class = $this->class_model->update_or_fail($data, $id, array(
                'success' => false,
                'message' => 'Not found'
            ));
            $response = array(
                'success' => true,
                'message' => 'Success',
                'class' => $class,
            );
            $header_status = 200;
        }

        $this->output->json($response, $header_status);
    }

    public function delete($id)
    {
        $this->load->model('class_model');
             
        $errors = array();
        $header_status = 200;

        $class = $this->class_model->with(array('total_student'))->get($id);

        if ($class->total_student != 0) {
            $errors['class_has_student'] = 'Class have students';
        }

        if (!$errors) {
            $this->class_model->delete($id);
            $response = array(
                'success' => true,
                'message' => 'Success'
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Fail'
            );
            $header_status = 406;
        }

        $this->output->json($response, $header_status);
    }

    public function sort_order()
    {
        $sort_orders = $this->input->post();
        $data = array();

        foreach ($sort_orders as $sort_order => $class) {
            $data[$sort_order] = array(
                'id' => $class['id'],
            );
        }

        if ($data) {
            $this->load->model('class_model');
            $this->class_model->sort_order($data);
        }

        $response = array(
            'success' => true,
            'message' => trans('saved'),
        );

        $this->output->json($response, 200);
    }

    public function validate_store(array $data)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);
        
        $this->form_validation->set_rules('name', 'Name', 'required|max_length[255]');
        $this->form_validation->set_rules('code', 'Code', 'required|is_unique[classes.code]');
        $this->form_validation->set_rules('term', 'Term', 'integer');
        $this->form_validation->set_rules('major_id', 'Major id', 'required|integer|exists[majors.id]');
        $this->form_validation->set_rules('faculty_id', 'Faculty id', 'required|integer|exists[faculties.id]');

        return $this->form_validation;
    }

    public function validate_update(array $data, $id)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);

        if (isset($data['name'])) {
            $this->form_validation->set_rules('name', 'Name', 'required|max_length[255]');
        }

        if (isset($data['code'])) {
            $this->form_validation->set_rules('code', 'Code', 'required|callback_is_unique_class_code['. $id .']');
        }

        if (isset($data['term'])) {
            $this->form_validation->set_rules('term', 'Term', 'integer');
        }

        if (isset($data['major_id'])) {
            $this->form_validation->set_rules('major_id', 'Major id', 'required|integer|exists[majors.id]');
        }
        
        if (isset($data['faculty_id'])) {
            $this->form_validation->set_rules('faculty_id', 'Faculty id', 'required|integer|exists[faculties.id]');
        }

        return $this->form_validation;
    }

    public function is_unique_class_code($code, $except_id)
    {
        $count = $this->db->where('id !=', $except_id)->where('code', $code)->from('classes')->count_all_results();
        if ($count) {
            $this->form_validation->set_message(__FUNCTION__, 'Class code have been taken');
            return false;
        }

        return true;
    }
}
