<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_controller extends MY_Controller
{
    public function dashboard()
    {
        $this->load->model(array('report_model', 'user_model', 'role_model'));
        $this->load->library('log_reader');

        $total_users_online = $this->report_model->count_users_online();
        $logs = $this->log_reader->all(date('Y-m-d'));
        $roles = $this->role_model->all(array('with' => array('total_user')));
        $users = $this->user_model->paginate(array('with' => array('role')));

        $response = array(
            'total_users' => $users->total,
            'total_roles' => count($roles),
            'total_logs' => count($logs),
            'total_users_online' => $total_users_online,
            'top_ten_users' => $users->data,
            'roles' => $roles,
            'top_logs' => array_slice($logs, 0, 10),
        );

        $this->output->json($response, 200);
    }
}
