<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faculty_controller extends MY_Controller
{
    public function index()
    {
        $this->load->model('faculty_model');
        $filter = $this->input->get();
        $response = $this->faculty_model->paginate($filter);

        $this->output->json($response, 200);
    }

    public function show($id)
    {
        $this->load->model('faculty_model');
        $faculty = $this->faculty_model->get_or_fail($id, array(
            'success' => false,
            'message' => 'Not found'
        ));

        $response = array(
            'success' => true,
            'message' => 'OK',
            'faculty' => $faculty,
        );
        
        $this->output->json($response, 200);
    }

    public function store()
    {
        $data = $this->input->post();
        $validator = $this->validate_store($data);
        $response = array();

        if (!$validator->run()) {
            $response = array(
                'success' => false,
                'message' => 'Fail',
                'errors' => $validator->get_errors_array(),
            );
            $header_status = 406;
        } else {
            $this->load->model('faculty_model');
            $faculty = $this->faculty_model->create($data);
            $response = array(
                'success' => true,
                'message' => 'Success',
                'faculty' => $faculty,
            );
            $header_status = 200;
        }

        $this->output->json($response, $header_status);
    }

    public function update($id)
    {
        $data = $this->input->post();
        $validator = $this->validate_update($data, $id);
        $response = array();

        if (!$validator->run()) {
            $response = array(
                'success' => false,
                'message' => 'Error',
                'errors' => $validator->get_errors_array(),
            );
            $header_status = 406;
        } else {
            $this->load->model('faculty_model');
            $faculty = $this->faculty_model->update_or_fail($data, $id, array(
                'success' => false,
                'message' => 'Not found'
            ));
            $response = array(
                'success' => true,
                'message' => 'Success',
                'faculty' => $faculty,
            );
            $header_status = 200;
        }

        $this->output->json($response, $header_status);
    }

    public function delete($id)
    {
        $this->load->model('faculty_model');
        $faculty = $this->faculty_model->with(array('total_major', 'total_class', 'total_student'))->get($id);
        
        $response = array();
        $errors = array();
        $header_status = 200;

        if ($faculty->total_major != 0) {
            $errors['faculty_has_major'] = 'Faculty has majors';
        }

        if ($faculty->total_class != 0) {
            $errors['faculty_has_class'] = 'Faculty has classes';
        }

        if ($faculty->total_student != 0) {
            $errors['faculty_has_student'] = 'Faculty has students';
        }

        if (!$errors) {
            $this->faculty_model->delete($id);
            $response = array(
                'success' => true,
                'message' => 'Removed faculty success'
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Fail',
                'errors' => $errors
            );
            $header_status = 406;
        }

        $this->output->json($response, $header_status);
    }

    public function sort_order()
    {
        $sort_orders = $this->input->post();
        $data = array();

        foreach ($sort_orders as $sort_order => $faculty) {
            $data[$sort_order] = array(
                'id' => $faculty['id'],
            );
        }

        if ($data) {
            $this->load->model('faculty_model');
            $this->faculty_model->sort_order($data);
        }

        $response = array(
            'success' => true,
            'message' => trans('saved'),
        );

        $this->output->json($response, 200);
    }

    public function validate_store(array $data)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);
        
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('code', 'Code', 'required|is_unique[faculties.code]');

        return $this->form_validation;
    }

    public function validate_update(array $data, $id)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);

        if (isset($data['name'])) {
            $this->form_validation->set_rules('name', 'Name', 'required');
        }

        if (isset($data['code'])) {
            $this->form_validation->set_rules('code', 'Code', 'required|callback_is_unique_faculty_code['. $id .']');
        }

        return $this->form_validation;
    }

    public function is_unique_faculty_code($code, $except_id)
    {
        $count = $this->db->where('id !=', $except_id)->where('code', $code)->from('faculties')->count_all_results();
        if ($count) {
            $this->form_validation->set_message(__FUNCTION__, 'Faculty code have been taken');
            return false;
        }

        return true;
    }
}
