<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Command_list_controller extends CI_Controller
{
    public function index()
    {
        $this->console->log('Command lists');
        $this->console->log('Make', 1);
        $this->console->log('make:controller Controller_name type(std: default|resource|restful) extends(default is MY_Controller)', 2);
        $this->console->log('make:model Model_name table primary_key(default is id) extends(default is MY_Model)', 2);
        $this->console->log('make:helper helper_name', 2);
        $this->console->log('make:library Library_name', 2);
        $this->console->log('make:language language_file_name language(default is current language)', 2);
    }
}
