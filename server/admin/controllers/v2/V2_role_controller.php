<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class V2_role_controller extends MY_Controller
{
    public function index()
    {
        $this->load->model('v2/v2_role_model');
        $filter = $this->input->get();

        $response = $this->v2_role_model->paginate($filter);

        $this->output->json($filter, 200);
    }
}