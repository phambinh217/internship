<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catalog_controller extends CI_Controller
{
    public function index()
    {
        return $this->load->view('catalog');
    }
}