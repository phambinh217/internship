<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lecturer_controller extends MY_Controller
{
    public function index()
    {
        $this->load->model('lecturer_model');
        $filter = $this->input->get();
        $response = $this->lecturer_model->paginate($filter);

        $this->output->json($response, 200);
    }

    public function show($id)
    {
        $this->load->model('lecturer_model');
        $with = (array) (isset($_GET['with']) ? $this->input->get('with') : array());
        $lecturer = $this->lecturer_model->with($with)->get_or_fail($id, array(
            'success' => false,
            'message' => 'Not found'
        ));

        $response = array(
            'success' => true,
            'message' => 'OK',
            'lecturer' => $lecturer,
        );

        $this->output->json($response, 200);
    }

    public function store()
    {
        $data = $this->input->post();
        $validator = $this->validate_store($data);
        $response = array();

        if (!$validator->run()) {
            $response = array(
                'success' => false,
                'message' => 'Fail',
                'errors' => $validator->get_errors_array(),
            );
            $header_status = 406;
        } else {
            $this->load->model('lecturer_model');
            $lecturer = $this->lecturer_model->create($data);
            $response = array(
                'success' => true,
                'message' => 'Success',
                'lecturer' => $lecturer,
            );
            $header_status = 200;
        }

        $this->output->json($response, $header_status);
    }

    public function update($id)
    {
        $data = $this->input->post();
        $validator = $this->validate_update($data, $id);
        $response = array();

        if (!$validator->run()) {
            $response = array(
                'success' => false,
                'message' => 'Error',
                'errors' => $validator->get_errors_array(),
            );
            $header_status = 406;
        } else {
            $this->load->model('lecturer_model');
            $lecturer = $this->lecturer_model->update_or_fail($data, $id, array(
                'success' => false,
                'message' => 'Not found'
            ));
            $response = array(
                'success' => true,
                'message' => 'Success',
                'lecturer' => $lecturer,
            );
            $header_status = 200;
        }

        $this->output->json($response, $header_status);
    }

    public function delete($id)
    {
        $this->load->model('lecturer_model');
        $this->lecturer_model->delete($id);

        $response = array(
            'success' => true,
            'message' => 'OK'
        );

        $this->output->json($response, 200);
    }

    public function validate_store(array $data)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);

        $this->form_validation->set_rules('code', 'Code', 'required|max_length[255]|is_unique[lecturers.code]');
        $this->form_validation->set_rules('fullname', 'Fullname', 'required|max_length[255]');
        $this->form_validation->set_rules('birth', 'Birth', 'date_format[d-m-Y]');
        $this->form_validation->set_rules('phone', 'Phone', 'max_length[255]');
        $this->form_validation->set_rules('email', 'Email', 'valid_email|max_length[255]');
        $this->form_validation->set_rules('faculty_id', 'Faculty id', 'required|integer|exists[faculties.id]');

        return $this->form_validation;
    }

    public function validate_update(array $data, $id)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);

        if (isset($data['code'])) {
            $this->form_validation->set_rules('code', 'Code', 'required|max_length[255]|callback_is_unique_lecturer_code[' . $id . ']');
        }

        if (isset($data['birth'])) {
            $this->form_validation->set_rules('birth', 'Birth', 'date_format[d-m-Y]');
        }

        if (isset($data['fullname'])) {
            $this->form_validation->set_rules('fullname', 'Fullname', 'required|max_length[255]');
        }

        if (isset($data['phone'])) {
            $this->form_validation->set_rules('phone', 'Phone', 'max_length[255]');
        }

        if (isset($data['email'])) {
            $this->form_validation->set_rules('email', 'Email', 'valid_email|max_length[255]');
        }

        if (isset($data['faculty_id'])) {
            $this->form_validation->set_rules('faculty_id', 'Faculty id', 'required|integer|exists[faculties.id]');
        }

        return $this->form_validation;
    }

    public function is_unique_lecturer_code($code, $except_id)
    {
        $count = $this->db->where('id !=', $except_id)->where('code', $code)->from('lecturers')->count_all_results();
        if ($count) {
            $this->form_validation->set_message(__FUNCTION__, 'Lecturer code have been taken');
            return false;
        }

        return true;
    }
}
