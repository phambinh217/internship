<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student_controller extends MY_Controller
{
    public function index()
    {
        $this->load->model('student_model');
        $filter = $this->input->get();
        $response = $this->student_model->paginate($filter);

        $this->output->json($response, 200);
    }

    // students/ID/show
    public function show($id)
    {
        $this->load->model('student_model');
        $with = (array) (isset($_GET['with']) ? $this->input->get('with') : array());
        $student = $this->student_model->with($with)->get_or_fail($id, array(
            'success' => false,
            'message' => 'Not found'
        ));

        $response = array(
            'success' => true,
            'message' => 'OK',
            'student' => $student,
        );

        $this->output->json($response, 200);
    }

    // students/codes/
    public function code()
    {

    }

    public function store()
    {
        $data = $this->input->post();
        $validator = $this->validate_store($data);
        $response = array();

        if (!$validator->run()) {
            $response = array(
                'success' => false,
                'message' => 'Fail',
                'errors' => $validator->get_errors_array(),
            );
            $header_status = 406;
        } else {
            $this->load->model('student_model');
            $student = $this->student_model->create($data);
            $response = array(
                'success' => true,
                'message' => 'Success',
                'student' => $student,
            );
            $header_status = 200;
        }

        $this->output->json($response, $header_status);
    }

    public function update($id)
    {
        $data = $this->input->post();
        $validator = $this->validate_update($data, $id);
        $response = array();

        if (!$validator->run()) {
            $response = array(
                'success' => false,
                'message' => 'Error',
                'errors' => $validator->get_errors_array(),
            );
            $header_status = 406;
        } else {
            $this->load->model('student_model');
            $student = $this->student_model->update_or_fail($data, $id, array(
                'success' => false,
                'message' => 'Not found'
            ));
            $response = array(
                'success' => true,
                'message' => 'Success',
                'student' => $student,
            );
            $header_status = 200;
        }

        $this->output->json($response, $header_status);
    }

    public function delete($id)
    {
        $this->load->model('student_model');
        $this->student_model->delete($id);

        $response = array(
            'success' => true,
            'message' => 'OK'
        );

        $this->output->json($response, 200);
    }

    public function validate_store(array $data)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('code', 'Code', 'required|is_unique[students.code]');
        $this->form_validation->set_rules('fullname', 'Fullname', 'required|max_length[255]');
        $this->form_validation->set_rules('birth', 'Birth', 'date_format[d-m-Y]');
        $this->form_validation->set_rules('term', 'Term', 'required|integer|exists[terms.term]');
        $this->form_validation->set_rules('class_id', 'Class id', 'required|integer|exists[classes.id]');
        $this->form_validation->set_rules('major_id', 'Major id', 'required|integer|exists[majors.id]');
        $this->form_validation->set_rules('faculty_id', 'Faculty id', 'required|integer|exists[faculties.id]');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('term', 'Term', 'required|integer');
        $this->form_validation->set_rules('phone', 'Phone', '');
        $this->form_validation->set_rules('email', 'Email', 'valid_email');

        return $this->form_validation;
    }

    public function validate_update(array $data, $id)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);

        if (isset($data['code'])) {
            $this->form_validation->set_rules('code', 'Code', 'required|callback_is_unique_student_code['. $id .']');
        }

        if (isset($data['birth'])) {
            $this->form_validation->set_rules('birth', 'Birth', 'date_format[d-m-Y]');
        }

        if (isset($data['term'])) {
            $this->form_validation->set_rules('term', 'Term', 'integer');
        }

        if (isset($data['phone'])) {
            $this->form_validation->set_rules('phone', 'Phone', '');
        }

        if (isset($data['email'])) {
            $this->form_validation->set_rules('email', 'Email', 'valid_email');
        }

        if (isset($data['fullname'])) {
            $this->form_validation->set_rules('fullname', 'Fullname', 'required|max_length[255]');
        }

        if (isset($data['term'])) {
            $this->form_validation->set_rules('term', 'Term', 'required|integer|exists[terms.term]');
        }

        if (isset($data['class_id'])) {
            $this->form_validation->set_rules('class_id', 'Class id', 'required|integer|exists[classes.id]');
        }

        if (isset($data['major_id'])) {
            $this->form_validation->set_rules('major_id', 'Major id', 'required|integer|exists[majors.id]');
        }

        if (isset($data['faculty_id'])) {
            $this->form_validation->set_rules('faculty_id', 'Faculty id', 'required|integer|exists[faculties.id]');
        }

        return $this->form_validation;
    }

    public function is_unique_student_code($code, $except_id)
    {
        $count = $this->db->where('id !=', $except_id)->where('code', $code)->from('students')->count_all_results();
        if ($count) {
            $this->form_validation->set_message(__FUNCTION__, 'Student code have been taken');
            return false;
        }

        return true;
    }
}
