<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permission_controller extends MY_Controller
{
    public function index()
    {
        $response = array(
            'data' => $this->gate->all(),
            'total' => count($this->gate->all()),
        );

        $this->output->json($response, 200);
    }
}
