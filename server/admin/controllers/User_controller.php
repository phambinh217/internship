<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_controller extends MY_Controller
{
    public function index()
    {
        $this->load->model('user_model');
        $filter = $this->input->get();
        $response = $this->user_model->paginate($filter);

        $this->output->json($response, 200);
    }

    public function show($id)
    {
        $this->load->model('user_model');
        $with = (array) (isset($_GET['with']) ? $this->input->get('with') : array());
        $user = $this->user_model->with($with)->get_or_fail($id, array(
            'success' => false,
            'message' => 'User not found'
        ));

        $response = array(
            'success' => true,
            'message' => trans('success'),
            'user' => $user,
        );
        
        $this->output->json($response, 200);
    }

    public function store()
    {
        $this->lang->load(array('form_validation', 'user'));
        $data = $this->input->post();
        $validator = $this->validate_store($data);
        $response = array();

        if (!$validator->run() || !$data) {
            $response = array(
                'success' => false,
                'message' => trans('form_validation_check_the_fields'),
                'errors' => $validator->get_errors_array(),
            );
            $status_header = 406;
        } else {
            $this->load->model('user_model');
            $user = $this->user_model->create($data);
            $response = array(
                'success' => false,
                'message' => 'Fail',
                'user' => $user,
            );
            $status_header = 200;
        }

        $this->output->json($response, $status_header);
    }

    public function update($id)
    {
        $this->lang->load(array('form_validation', 'user'));
        $data = $this->input->post();
        $validator = $this->validate_update($data, $id);
        $response = array();

        if (!$validator->run()) {
            $response = array(
                'success' => false,
                'message' => trans('form_validation_check_the_fields'),
                'errors' => $validator->get_errors_array(),
            );
            $status_header = 406;
        } else {
            $this->load->model('user_model');

            $user = $this->user_model->update_or_fail($data, $id, array(
                'success' => false,
                'message' => 'User not found'
            ));

            $response = array(
                'success' => true,
                'message' => 'Success',
                'user' => $user,
            );
            $status_header = 200;
        }

        $this->output->json($response, $status_header);
    }

    public function delete($id)
    {
        $this->lang->load('user');

        $this->load->model('user_model');
        $errors = array();
        $response = array();
        $header_status = 200;

        if ($this->jwt_auth->user()->id == $id) {
            $errors['your_self'] = trans('user_delete_my_self_message');
            $response = array(
                'success' => false,
                'message' => trans('fail'),
                'errors' => $errors,
            );
            $header_status = 406;
        } else {
            $this->user_model->delete($id);
            $response = array(
                'success' => true,
                'message' => 'Success'
            );
        }

        $this->output->json($response, $header_status);
    }

    protected function validate_store(array $data)
    {
        $this->lang->load('user');
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('fullname', trans('user_fullname'), 'required|max_length[255]');
        $this->form_validation->set_rules('birth', trans('user_birth'), 'date_format[d-m-Y]');
        $this->form_validation->set_rules('email', trans('user_email'), 'required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('phone', trans('user_telephone'), 'max_length[255]');
        $this->form_validation->set_rules('avatar', trans('user_avatar'), 'max_length[255]');
        $this->form_validation->set_rules('password', trans('user_password'), 'required|max_length[255]');
        $this->form_validation->set_rules('role_id', trans('user_role'), 'required|exists[roles.id]');
        $this->form_validation->set_rules('send_email_new_login', trans('user_send_email_new_login'), 'in_list[0,1]');

        return $this->form_validation;
    }

    protected function validate_update(array $data, $user_id)
    {
        $this->lang->load('user');
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);

        if (isset($data['fullname'])) {
            $this->form_validation->set_rules('fullname', trans('user_fullname'), 'required|max_length[255]');
        }

        if (isset($data['birth'])) {
            $this->form_validation->set_rules('birth', trans('user_birth'), 'date_format[d-m-Y]');
        }

        if (isset($data['email'])) {
            $this->form_validation->set_rules('email', trans('user_email'), 'required|valid_email|callback_unique_email['.$user_id.']');
        }

        if (isset($data['phone'])) {
            $this->form_validation->set_rules('phone', trans('user_telephone'), 'max_length[255]');
        }

        if (isset($data['avatar'])) {
            $this->form_validation->set_rules('avatar', trans('user_avatar'), 'max_length[255]');
        }

        if (isset($data['send_email_new_login'])) {
            $this->form_validation->set_rules('send_email_new_login', trans('user_send_email_new_login'), 'in_list[0,1]');
        }

        if (isset($data['role_id'])) {
            $this->form_validation->set_rules('role_id', trans('user_role'), 'required|exists[roles.id]');
        }

        return $this->form_validation;
    }

    public function unique_email($email, $user_id)
    {
        $this->lang->load('user');

        $count = $this->db->where('email', $email)->where('id !=', $user_id)->count_all_results('users');
        if ($count != 0) {
            $this->form_validation->set_message(__FUNCTION__, trans('email_exists'));
            return false;
        }

        return true;
    }
}
