<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Internship_lecturer_controller extends MY_Controller
{
    public function index($internship_id)
    {
        $this->load->model('internship_lecturer_model');

        $filter = $this->input->get();
        $filter['internship_id'] = $internship_id;
        $response = $this->internship_lecturer_model->paginate($filter);

        $this->output->json($response, 200);    
    }

    public function full_student($internship_id, $lecturer_id)
    {
        $this->load->model('internship_lecturer_model');
        $in_scope = $this->internship_lecturer_model->in_scope($internship_id, $lecturer_id);
        
        if ($in_scope) {
            $response = $this->internship_lecturer_model->is_full_student($internship_id, $lecturer_id);
        } else {
            $response = -1;
        }

        $this->output->json($response, 200);
    }
}
