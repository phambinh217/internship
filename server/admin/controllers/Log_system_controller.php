<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log_system_controller extends MY_Controller
{
    public function index()
    {
        $this->load->library('log_reader');
        $files = $this->log_reader->files();
        $response = array(
            'success' => true,
            'message' => trans('success'),
            'data' => $files,
            'total' => count($files)
        );
        $this->output->json($response, 200);
    }

    public function show($date)
    {
        $this->load->library('log_reader');
        $logs = $this->log_reader->all($date);
        $response = array(
            'success' => true,
            'message' => trans('success'),
            'total' => count($logs),
            'log' => $logs
        );

        $this->output->json($response, 200);
    }

    public function delete()
    {
        $this->lang->load('log_system');
        $files_success = array();
        if ($this->input->post('file')) {
            $files = (array)$this->input->post('file');
            $this->load->library('log_reader');
            foreach ($files as $file) {
                $this->log_reader->delete($file);
                $files_success[] = $file;
            }
        }

        $response = array(
            'success' => true,
            'message' => trans('log_system_file_delete'),
            'files' => $files_success
        );

        $this->output->json($response, 200);
    }
}