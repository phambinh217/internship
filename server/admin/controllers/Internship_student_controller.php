<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Internship_student_controller extends MY_Controller
{
    public function index($internship_id)
    {
        $this->load->model('student_internship_model');

        $filter = $this->input->get();
        $filter['internship_id'] = $internship_id;
        $response = $this->student_internship_model->paginate($filter);

        $this->output->json($response, 200);    
    }

    public function show($internship_id, $student_id)
    {
        $this->load->model('student_internship_model');
        $with = (array) (isset($_GET['with']) ? $this->input->get('with') : array());
        $internship_student = $this->student_internship_model->internship_student_get($internship_id, $student_id);
        $internship_student = $this->student_internship_model->with($with)->get_or_fail($internship_student->id, array(
            'success' => false,
            'message' => 'Data not found'
        ));

        $response = array(
            'success' => true,
            'message' => trans('success'),
            'internship_student' => $internship_student,
        );
        
        $this->output->json($response, 200);
    }

    public function exists($internship_id, $student_id)
    {
        $this->load->model('student_internship_model');

        $in_scope = $this->student_internship_model->in_scope($student_id, $internship_id);

        if ($in_scope) {
            $c = $this->db->where(array('student_id' => (int)$student_id, 'internship_id' => (int)$internship_id))->from('student_internships')->count_all_results();
            $response = $c != 0;
        } else {
            $response = -1;
        }
        
        return $this->output->json($response);
    }

    public function delete($internship_id, $student_id)
    {
        $this->load->model('student_internship_model');
        $internship_student = $this->student_internship_model->internship_student_get($internship_id, $student_id);
        
        if ($internship_student) {
            $this->student_internship_model->delete($internship_student->id);
        }

        $response = array(
            'success' => true,
            'message' => 'OK'
        );

        $this->output->json($response, 200);
    }

    public function store($internship_id)
    {
        $data = $this->input->post();
        $data['internship_id'] = $internship_id;
        $validator = $this->validate_store($data);
        $response = array();

        if (!$validator->run()) {
            $response = array(
                'success' => false,
                'message' => 'Fail',
                'errors' => $validator->get_errors_array(),
            );
            $header_status = 406;
        } else {
            $this->load->model('student_internship_model');
            $internship_student = $this->student_internship_model->create($data);

            $response = array(
                'success' => true,
                'message' => 'Success',
                'internship_student' => $internship_student,
            );
            $header_status = 200;
        }

        $this->output->json($response, $header_status);
    }

    public function update($internship_id, $student_id)
    {
        $data = $this->input->post();
        $data['internship_id'] = $internship_id;
        $data['student_id'] = $student_id;
        $validator = $this->validate_update($data, $internship_id, $student_id);
        $response = array();

        if (!$validator->run()) {
            $response = array(
                'success' => false,
                'message' => 'Error',
                'errors' => $validator->get_errors_array(),
            );
            $header_status = 406;
        } else {
            $this->load->model('student_internship_model');
            $internship_student = $this->student_internship_model->internship_student_get($internship_id, $student_id);
            $internship_student = $this->student_internship_model->update_or_fail($data, isset($internship_student->id) ? $internship_student->id : -1, array(
                'success' => false,
                'message' => 'Not found'
            ));
            $response = array(
                'success' => true,
                'message' => 'Success',
                'internship_student' => $internship_student,
            );
            $header_status = 200;
        }

        $this->output->json($response, $header_status);
    }

    public function students_count_by_date($internship_id)
    {
        
    }

    public function validate_store(array $data)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);

        $this->form_validation->set_rules('student_id', 'student_id', 'exists[students.id]|callback_student_scope['. $data['internship_id'] .']|callback_student_registered['. $data['internship_id'] .']');
        $this->form_validation->set_rules('lecturer_id', 'lecturer_id', 'exists[lecturers.id]');
        $this->form_validation->set_rules('internship_id', 'internship_id', 'exists[internships.id]');
        $this->form_validation->set_rules('message', 'message', 'max_length[500]');

        return $this->form_validation;
    }

    public function validate_update(array $data, $internship_id, $student_id)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);

        if (isset($data['student_id'])) {
            $this->form_validation->set_rules('student_id', 'student_id', 'exists[students.id]');
        }

        if (isset($data['lecturer_id'])) {
            $this->form_validation->set_rules('lecturer_id', 'lecturer_id', 'exists[lecturers.id]');
        }

        if (isset($data['internship_id'])) {
            $this->form_validation->set_rules('internship_id', 'internship_id', 'exists[internships.id]');
        }

        if (isset($data['message'])) {
            $this->form_validation->set_rules('message', 'message', 'max_length[500]');
        }

        return $this->form_validation;
    }

    public function student_registered($student_id, $internship_id)
    {
        $c = $this->db->where(array('student_id' => $student_id, 'internship_id' => $internship_id))->from('student_internships')->count_all_results();
        
        if ($c) {
            $this->form_validation->set_message(__FUNCTION__, 'Student have been reigsterd');
            return false;
        }

        return true;
    }

    public function student_scope($student_id, $internship_id)
    {
        $this->load->model('student_internship_model');
        $in_scope = $this->student_internship_model->in_scope($student_id, $internship_id);
        
        if (!$in_scope) {
            $this->form_validation->set_message(__FUNCTION__, 'Student not in internship\'s scope');
            return false;
        }

        return true;
    }
}
