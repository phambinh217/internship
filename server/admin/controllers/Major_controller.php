<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Major_controller extends MY_Controller
{
    public function index()
    {
        $this->load->model('major_model');
        $filter = $this->input->get();
        $response = $this->major_model->paginate($filter);

        $this->output->json($response, 200);
    }

    public function show($id)
    {
        $this->load->model('major_model');
        $major = $this->major_model->get_or_fail($id, array(
            'success' => false,
            'message' => 'Not found'
        ));

        $response = array(
            'success' => true,
            'message' => 'OK',
            'major' => $major,
        );
        
        $this->output->json($response, 200);
    }

    public function store()
    {
        $data = $this->input->post();
        $validator = $this->validate_store($data);
        $response = array();

        if (!$validator->run()) {
            $response = array(
                'success' => false,
                'message' => 'Fail',
                'errors' => $validator->get_errors_array(),
            );
            $header_status = 406;
        } else {
            $this->load->model('major_model');
            $major = $this->major_model->create($data);
            $response = array(
                'success' => true,
                'message' => 'Success',
                'major' => $major,
            );
            $header_status = 200;
        }

        $this->output->json($response, $header_status);
    }

    public function update($id)
    {
        $data = $this->input->post();
        $validator = $this->validate_update($data, $id);
        $response = array();

        if (!$validator->run()) {
            $response = array(
                'success' => false,
                'message' => 'Error',
                'errors' => $validator->get_errors_array(),
            );
            $header_status = 406;
        } else {
            $this->load->model('major_model');
            $major = $this->major_model->update_or_fail($data, $id, array(
                'success' => false,
                'message' => 'Not found'
            ));
            $response = array(
                'success' => true,
                'message' => 'Success',
                'major' => $major,
            );
            $header_status = 200;
        }

        $this->output->json($response, $header_status);
    }

    public function delete($id)
    {
        $this->load->model('major_model');
        
        $errors = array();
        $header_status = 200;

        $major = $this->major_model->with(array('total_student', 'total_class'))->get($id);

        if ($major->total_student != 0) {
            $errors['major_has_student'] = 'Major have students';
        }

        if ($major->total_class != 0) {
            $errors['major_has_class'] = 'Major have classes';
        }

        if (!$errors) {
            $this->major_model->delete($id);
            $response = array(
                'success' => true,
                'message' => 'Success'
            );
        } else {
            $response = array(
                'success' => false,
                'message' => 'Fail'
            );
            $header_status = 406;
        }

        $this->output->json($response, $header_status);
    }

    public function sort_order()
    {
        $sort_orders = $this->input->post();
        $data = array();

        foreach ($sort_orders as $sort_order => $major) {
            $data[$sort_order] = array(
                'id' => $major['id'],
            );
        }

        if ($data) {
            $this->load->model('major_model');
            $this->major_model->sort_order($data);
        }

        $response = array(
            'success' => true,
            'message' => trans('saved'),
        );

        $this->output->json($response, 200);
    }

    public function validate_store(array $data)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);
        
        $this->form_validation->set_rules('name', 'Name', 'required|max_length[255]');
        $this->form_validation->set_rules('code', 'Code', 'required|is_unique[majors.code]');
        $this->form_validation->set_rules('faculty_id', 'Faculty id', 'required|integer|exists[faculties.id]');

        return $this->form_validation;
    }

    public function validate_update(array $data, $id)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);

        if (isset($data['name'])) {
            $this->form_validation->set_rules('name', 'Name', 'required|max_length[255]');
        }

        if (isset($data['code'])) {
            $this->form_validation->set_rules('code', 'Code', 'required|callback_is_unique_major_code['. $id .']');
        }

        if (isset($data['faculty_id'])) {
            $this->form_validation->set_rules('faculty_id', 'Faculty id', 'required|integer|exists[faculties.id]');
        }

        return $this->form_validation;
    }

    public function is_unique_major_code($code, $except_id)
    {
        $count = $this->db->where('id !=', $except_id)->where('code', $code)->from('majors')->count_all_results();
        if ($count) {
            $this->form_validation->set_message(__FUNCTION__, 'Major code have been taken');
            return false;
        }

        return true;
    }
}
