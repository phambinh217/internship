<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Policy
{
    private $CI;
    
    protected $policies = array();

    public function __construct()
    {
        $this->CI =& get_instance();
    }

    public function define($controller, $gate = '')
    {
        if (is_array($controller)) {
            foreach ($controller as $c => $g) {
                $this->policies[$c] = $g;
            }
        } else {
            $this->policies[$controller] = $gate;
        }

        return $this;
    }

    public function all()
    {
        return $this->policies;
    }

    public function policy_request()
    {
        $controller = $this->CI->router->class . '/' . $this->CI->router->method;
        $allow = true;
        if (isset($this->policies[$controller])) {
            $allow = $this->CI->gate->allow($this->policies[$controller]);
        }
        
        if (!$allow) {
            $this->CI->output->json(array(
                'success' => false,
                'message' => 'Fail',
                'errors' => array(
                    'forbidden' => 'You don\'t have access to the url you where trying to reach.' 
                )
            ), 403);
        }
    }
}
