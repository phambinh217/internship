<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Console
{
    private $tab = '    ';
    
    public function log($content = '', $deep = 0)
    {
        echo str_repeat($this->tab, $deep) . $content . PHP_EOL;
    }
}
