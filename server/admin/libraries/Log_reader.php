<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log_reader
{
    private $CI;

    protected $log_path;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->log_path = $this->CI->config->item('log_path') ? $this->CI->config->item('log_path') : APPPATH .'logs';
        $this->CI->load->helper('file');
    }

    public function all($date, array $data = array())
    {
        $results = array();
        $file_path = $this->log_path . '/log-' . $date .'.php';
        if (file_exists($file_path)) {
            $file = fopen($file_path, 'r');
            $i = 1;
            
            while(!feof($file)) {
                preg_match("/(ERROR|DEBUG|INFO) - ([0-9]{4}-[0-1][0-9]-[0-3][0-9] [0-2][0-9]:[0-6][0-9]:[0-6][0-9]) --> (.*)/", fgets($file), $match);
                if ($match) {
                    $row = new stdClass();
                    $row->order = $i;
                    $row->type = $match[1];
                    $row->time_gmt = $match[2];
                    $row->message = $match[3];
                    $results[] = $row;
                    $i++;
                }
            }

            fclose($file);
        }

        return $results;
    }

    public function files()
    {
        $files = glob($this->log_path .'/log-*.php');
        $results = array();
        $i = 1;
        foreach ($files as $file) {
            $row = new stdClass();
            $row->order = $i;
            $row->date = str_replace(array('log-', '.php'), '', basename($file));
            $results[] = $row;
            $i++;
        }

        return $results;
    }

    public function content($date)
    {
        $content = array();
        $file_path = $this->log_path . '/log-' . $date .'.php';
        if (file_exists($file_path)) {
            $content = file_get_contents($file_path);
        }

        return $content;
    }

    public function delete($date)
    {
        $file_path = $this->log_path . '/log-' . $date .'.php';
        if (file_exists($file_path)) {
            @unlink($file_path);
            return true;
        }

        return false;
    }
}
