<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends MY_Model
{
    protected $offset_online = 60*15;

    protected $fillable = array(
        'firstname', 'lastname', 'birth', 'email', 'phone', 'avatar', 'role_id', 'password', 'remember_token', 'send_email_new_login', 'last_online_at',
    );

    protected $show = array(
        'users.id', 'firstname', 'lastname', 'birth', 'email', 'phone', 'role_id', 'avatar', 'send_email_new_login', 'created_at', 'updated_at',
    );

    public function with(array $data)
    {
        if (in_array('role', $data)) {
            $this->db
                ->select('roles.name as role_name, roles.permission as permission')
                ->join('roles', 'users.role_id=roles.id', 'left');
        }

        return $this;
    }

    public function get($id)
    {
        return $this->db
            ->select(implode(',', $this->show) . ', (last_online_at + '. $this->offset_online .' > UNIX_TIMESTAMP()) as is_online')
            ->where('users.id', $id)
            ->get('users')->first_row();
    }

    public function get_or_fail($id, $fail_data = array(), $header_status = 404)
    {
        $result = $this->get($id);
        
        if ($result) {
            return $result;
        }

        if ($fail_data) {
            $this->output->json($fail_data, $header_status);
        }

        show_404();
    }

    public function get_by_email($email)
    {
        return $this->db
            ->select(implode(',', $this->show) . ', (last_online_at + '. $this->offset_online .' > UNIX_TIMESTAMP()) as is_online')
            ->where('email', $email)
            ->get('users')->first_row();   
    }

    public function attempt($email, $password)
    {
        $user = $this->db->select('id, email, password')->where(array('email' => $email, 'password' => $password))->get('users')->first_row();
        if (!$user) {
            return false;
        }

        return $user;
    }

    public function all(array $data = array())
    {
        $this->query($data)
            ->select(implode(',', $this->show) . ', (last_online_at + '. $this->offset_online .' > UNIX_TIMESTAMP()) as is_online')
            ->get('users')
            ->result();
    }

    public function paginate(array $data = array())
    {
        if (!isset($data['perpage'])) {
            $perpage = 10;
        } else {
            $perpage = ((int)$data['perpage'] < 0) ? 10 : (int)$data['perpage'];
        }

        if (!isset($data['page'])) {
            $page = 1;
        } else {
            $page = ((int)$data['page'] < 1) ? 1 : (int)$data['page'];
        }

        $results = $this
            ->query($data)
            ->select(implode(',', $this->show) . ', (last_online_at + '. $this->offset_online .' > UNIX_TIMESTAMP()) as is_online')
            ->limit((int)$perpage, (int)($perpage * $page - $perpage))
            ->get('users')
            ->result();

        $total = $this
            ->query($data)
            ->from('users')
            ->count_all_results();

        return $this->pagination($total, $results, $perpage, $page);
    }

    public function query(array $data)
    {
        if (isset($data['with'])) {
            $this->with($data['with']);
        }

        if (isset($data['name'])) {
            $this->db->where('name', $data['name']);
        }

        if (isset($data['role_id'])) {
            $this->db->where('role_id', (int)$data['role_id']);
        }

        if (isset($data['email'])) {
            $this->db->where('email', $data['email']);
        }

        if (isset($data['keyword'])) {
            $keyword = str_replace(' ', '%', str_std($data['keyword']));
            $this->db->like('CONCAT_WS(\'.\', email, lastname, firstname)', $keyword);
        }
        
        if (isset($data['order_by'])) {
            $allows = array('id', 'firstname', 'lastname', 'role_id', 'birth', 'email', 'last_online_at', 'created_at', 'updated_at');
            if (is_array($data['order_by'])) {
                foreach ($data['order_by'] as $by => $order) {
                    if (in_array($by, $allows)) {
                        $order = strtoupper($order);
                        $order = $order == 'DESC' ? 'DESC' : ($order == 'ASC' ? 'ASC' : 'DESC');
                        $this->db->order_by($by, $order);
                    }
                }
            }
        }

        return $this->db;
    }

    public function create(array $data)
    {
        if (isset($data['birth'])) {
            $data['birth'] = date_format(date_create($data['birth']), 'Y-m-d');
        }
        
        if (isset($data['fullname'])) {
            $name = explode_fullname($data['fullname']);
            $data['firstname'] = $name['firstname'];
            $data['lastname'] = $name['lastname'];
        }

        $data = array_only($data, $this->fillable);

        if ($data) {
            $this->db->set('created_at', 'NOW()', false);
            $this->db->set('updated_at', 'NOW()', false);
            $this->db->insert('users');
            return $this->get($this->db->insert_id());
        }

        return null;
    }

    public function update(array $data, $id)
    {
        if (isset($data['birth'])) {
            $data['birth'] = date_format(date_create($data['birth']), 'Y-m-d');
        }

        if (isset($data['fullname'])) {
            $name = explode_fullname($data['fullname']);
            $data['firstname'] = $name['firstname'];
            $data['lastname'] = $name['lastname'];
        }

        $data = array_only($data, $this->fillable);

        if ($data) {
            $this->db->set('updated_at', 'NOW()', false);
            $this->db->where('id', (int)$id)->update('users', $data);
        }

        return $this->get((int)$id);
    }

    public function update_or_fail(array $data, $id, $fail_data = array(), $header_status = 404)
    {
        $result = $this->update($data, $id);
        if ($result) {
            return $result;
        }

        if ($fail_data) {
            $this->output->json($fail_data, $header_status);
        }

        show_404();
    }

    public function delete($id)
    {
        $this->db->where('id', (int)$id)->delete('users');
    }

    public function login_histories($user_id)
    {
        return $this->db->where('user_id', $user_id)->get('user_ips')->result();
    }

    public function push_ip(array $data, $ip, $user_id)
    {
        $allows = array('user_id', 'ip', 'last_browser', 'last_platform', 'total', 'last_time');
        $results = $this->db->where('user_id', $user_id)->get('user_ips')->result();
        
        if (isset($data['time'])) {
            $data['last_time'] = $data['time'];
        }

        if (isset($data['browser'])) {
            $data['last_browser'] = $data['browser'];
        }

        if (isset($data['platform'])) {
            $data['last_platform'] = $data['platform'];
        }

        // Không cho phép cập nhật totol như một tham số truyền vào
        if (isset($data['total'])) {
            unset($data['total']);
        }

        if ($results) {
            $find_ip = false;
            foreach ($results as $result) {
                if ($result->ip == $ip) {
                    $find_ip = $result;
                    break;
                }
            }

            if ($find_ip) {
                $data['total'] = $find_ip->total + 1;
                $this->db->where('id', $find_ip->id)->update('user_ips', array_only($data, $allows));
                return;
            }
        }

        $data['user_id'] = $user_id;
        $data['total'] = 0;
        $data['ip'] = $ip;

        $this->db->insert('user_ips', array_only($data, $allows));
    }
}
