<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Internship_lecturer_model extends MY_Model
{
    public function with(array $data)
    {
        if (in_array('total_student', $data)) {
            $this->db
                ->select('count(student_internships.student_id) as total_student')
                ->join('student_internships', 'student_internships.lecturer_id = internship_lecturers.lecturer_id AND student_internships.internship_id = internship_lecturers.internship_id', 'left')
                ->group_by('internship_lecturers.lecturer_id');
        }


        if (in_array('lecturer', $data)) {
            $this->db->select('lecturers.firstname as lecturer_firstname, lecturers.lastname as lecturer_lastname, lecturers.code as lecturer_code, lecturers.phone as lecturer_phone, lecturers.email as lecturer_email, lecturers.birth as lecturer_birth, lecturers.avatar as lecturer_avatar')->join('lecturers', 'lecturers.id = internship_lecturers.lecturer_id');
        }

        return $this;
    }

    public function get($id)
    {
        return $this->db->select('internship_lecturers.*')->where('id', $id)->get('internship_lecturers')->first_row();
    }

    public function get_or_fail($id, $fail_data = array(), $header_status = 404)
    {
        $result = $this->get($id);

        if ($result) {
            return $result;
        }

        if ($fail_data) {
            $this->output->json($fail_data, $header_status);
        }

        show_404();
    }

    public function all(array $data = array())
    {
        $this->query($data)->select('internship_lecturers.*')->get('internship_lecturers')->result();
    }

    public function paginate(array $data = array())
    {
        if (!isset($data['perpage'])) {
            $perpage = 10;
        } else {
            $perpage = ((int)$data['perpage'] < 0) ? 10 : (int)$data['perpage'];
        }

        if (!isset($data['page'])) {
            $page = 1;
        } else {
            $page = ((int)$data['page'] < 1) ? 1 : (int)$data['page'];
        }

        $total = $this
            ->query($data)
            ->select('internship_lecturers.*')
            ->from('internship_lecturers')
            ->count_all_results();

        $results = $this
            ->query($data)
            ->select('internship_lecturers.*')
            ->limit((int)$perpage, (int)($perpage * $page - $perpage))
            ->get('internship_lecturers')
            ->result();

        return $this->pagination($total, $results, $perpage, $page);
    }

    public function query(array $data)
    {
        if (isset($data['with'])) {
            $this->with($data['with']);
        }

        if (isset($data['internship_id'])) {
            $this->db->where('internship_lecturers.internship_id', (int)$data['internship_id']);
        }

        if (isset($data['keyword'])) {
            $keyword = str_replace(' ', '%', str_std($data['keyword']));
            $concat_fields = array('internship_lecturers.id');
            
            if (isset($data['with'])) {
                if (in_array('lecturer', $data['with'])) {
                    $concat_fields[] = 'lecturers.code';
                    $concat_fields[] = 'lecturers.firstname';
                    $concat_fields[] = 'lecturers.lastname';
                    $concat_fields[] = 'lecturers.phone';
                    $concat_fields[] = 'lecturers.email';
                }
            }

            $this->db->like('CONCAT_WS(\'.\', '. implode(',', $concat_fields) .')', $keyword);
        }
        
        return $this->db;
    }

    public function is_full_student($internship_id, $lecturer_id)
    {
        $limit = $this->db
            ->select('limit_student')
            ->where('internship_id', (int)$internship_id)
            ->where('lecturer_id', (int)$lecturer_id)
            ->get('internship_lecturers')
            ->first_row();

        $total_student = $this->db
            ->where('internship_id', (int)$internship_id)
            ->where('lecturer_id', (int)$lecturer_id)
            ->from('student_internships')
            ->count_all_results();

        if (!$limit) {
            return false;
        }

        return ($limit->limit_student <= $total_student);
    }

    public function in_scope($internship_id, $lecturer_id)
    {
        return $this->db
            ->where('internship_id', (int)$internship_id)
            ->where('lecturer_id', (int)$lecturer_id)
            ->from('internship_lecturers')
            ->count_all_results();
    }
}
