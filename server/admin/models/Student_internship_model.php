<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student_internship_model extends MY_Model
{
    protected $fillable = array(
        'internship_id', 'lecturer_id', 'student_id', 'message'
    );

    public function with(array $data)
    {
        if (in_array('student', $data)) {
            $this->db
                ->select('students.firstname as student_firstname, students.lastname as student_lastname, students.code as student_code, students.phone as student_phone, students.email as student_email, students.birth as student_birth, students.avatar as student_avatar')
                ->join('students', 'students.id = student_internships.student_id');
        }

        if (in_array('lecturer', $data)) {
            $this->db
                ->select('lecturers.firstname as lecturer_firstname, lecturers.lastname as lecturer_lastname, lecturers.code as lecturer_code, lecturers.phone as lecturer_phone, lecturers.email as lecturer_email, lecturers.birth as lecturer_birth, lecturers.avatar as lecturer_avatar')
                ->join('lecturers', 'lecturers.id = student_internships.lecturer_id');
        }

        return $this;
    }

    public function internship_student_get($internship_id, $student_id)
    {
        return $this->db->select('student_internships.*')
            ->where('internship_id', (int)$internship_id)
            ->where('student_id', (int)$student_id)
            ->get('student_internships')
            ->first_row();
    }

    public function get($id)
    {
        $this->with(array('lecturer', 'student'));
        return $this->db->select('student_internships.*')->where('student_internships.id', (int)$id)->get('student_internships')->first_row();
    }

    public function get_or_fail($id, $fail_data = array(), $header_status = 404)
    {
        $result = $this->get($id);

        if ($result) {
            return $result;
        }

        if ($fail_data) {
            $this->output->json($fail_data, $header_status);
        }

        show_404();
    }

    public function all(array $data = array())
    {
        $this->query($data)->select('student_internships.*')->get('student_internships')->result();
    }

    public function paginate(array $data = array())
    {
        if (!isset($data['perpage'])) {
            $perpage = 10;
        } else {
            $perpage = ((int)$data['perpage'] < 0) ? 10 : (int)$data['perpage'];
        }

        if (!isset($data['page'])) {
            $page = 1;
        } else {
            $page = ((int)$data['page'] < 1) ? 1 : (int)$data['page'];
        }

        $results = $this
            ->query($data)
            ->select('student_internships.*')
            ->limit((int)$perpage, (int)($perpage * $page - $perpage))
            ->get('student_internships')
            ->result();

        $total = $this
            ->query($data)
            ->select('student_internships.*')
            ->from('student_internships')
            ->count_all_results();

        return $this->pagination($total, $results, $perpage, $page);
    }

    public function query(array $data)
    {
        if (isset($data['with'])) {
            $this->with($data['with']);
        }

        if (isset($data['internship_id'])) {
            $this->db->where('internship_id', (int) $data['internship_id']);
        }

        if (isset($data['lecturer_id'])) {
            $this->db->where('lecturer_id', (int) $data['lecturer_id']);
        }

        if (isset($data['student_id'])) {
            $this->db->where('student_id', (int) $data['student_id']);
        }

        if (isset($data['keyword'])) {
            $keyword = str_replace(' ', '%', str_std($data['keyword']));
            $concat_fields = array('student_internships.id');
            
            if (isset($data['with'])) {
                if (in_array('student', $data['with'])) {
                    $concat_fields[] = 'students.code';
                    $concat_fields[] = 'students.firstname';
                    $concat_fields[] = 'students.lastname';
                    $concat_fields[] = 'students.phone';
                    $concat_fields[] = 'students.email';
                }

                if (in_array('lecturer', $data['with'])) {
                    $concat_fields[] = 'lecturers.code';
                    $concat_fields[] = 'lecturers.firstname';
                    $concat_fields[] = 'lecturers.lastname';
                    $concat_fields[] = 'lecturers.phone';
                    $concat_fields[] = 'lecturers.email';
                }
            }

            $this->db->like('CONCAT_WS(\'.\', '. implode(',', $concat_fields) .')', $keyword);
        }

        if (isset($data['order_by'])) {
            $allows = array('id', 'internship_id', 'lecturer_id', 'student_id', 'message', 'created_at', 'updated_at');
            
            if (isset($data['with'])) {
                if (in_array('student', $data['with'])) {
                    $allows['students.code'] = 'student_code';
                    $allows['students.firstname'] = 'student_firstname';
                    $allows['students.lastname'] = 'student_lastname';
                    $allows['students.phone'] = 'student_phone';
                    $allows['students.email'] = 'student_email';
                }

                if (in_array('lecturer', $data['with'])) {
                    $allows['lecturers.code'] = 'lecturer_code';
                    $allows['lecturers.firstname'] = 'lecturer_firstname';
                    $allows['lecturers.lastname'] = 'lecturer_lastname';
                    $allows['lecturers.phone'] = 'lecturer_phone';
                    $allows['lecturers.code'] = 'lecturer_email';
                }
            }

            if (is_array($data['order_by'])) {
                foreach ($data['order_by'] as $by => $order) {
                    if (in_array($by, $allows)) {
                        $flip = array_flip($allows);
                        
                        if (!is_numeric($flip[$by])) {
                            $by = $flip[$by];
                        }

                        $order = strtoupper($order);
                        $order = $order == 'DESC' ? 'DESC' : ($order == 'ASC' ? 'ASC' : 'DESC');
                        $this->db->order_by($by, $order);
                    }
                }
            }
        }
        
        return $this->db;
    }

    public function create(array $data)
    {
        $data = array_only($data, $this->fillable);
        if ($data) {
            $this->db->set('created_at', 'NOW()', false);
            $this->db->set('updated_at', 'NOW()', false);
            $this->db->insert('student_internships', $data);
            return $this->get($this->db->insert_id());
        }

        return null;
    }

    public function update(array $data, $id)
    {
        $data = array_only($data, $this->fillable);
        if ($data) {
            $this->db->set('updated_at', 'NOW()', false);
            $this->db->where('id', (int)$id)->update('student_internships', $data);
        }

        return $this->get((int)$id);
    }

    public function update_or_fail(array $data, $id, $fail_data = array(), $header_status = 404)
    {
        $result = $this->update($data, $id);
        if ($result) {
            return $result;
        }

        if ($fail_data) {
            $this->output->json($fail_data, $header_status);
        }

        show_404();
    }

    public function delete($id)
    {
        $this->db->where('id', (int)$id)->delete('student_internships');
    }

    public function in_scope($student_id, $internship_id)
    {
        $internship = $this->db->select('type')->where('id', (int)$internship_id)->get('internships')->first_row();

        if (!$internship) {
            return false;
        }

        $type = (int)$internship->type;
        switch ($type) {
            case INTERNSHIP_TYPE_ALL_STUDENTS:
                return true;
                break;

            case  INTERNSHIP_TYPE_FACULTY_STUDENTS:
                $internship_faculties = $this->db->select('object_id')->where('internship_id', (int)$internship_id)->get('internship_objects')->result();
                $student = $this->db->select('faculty_id')->where('id', (int)$student_id)->get('students')->first_row();
                if (!$student) {
                    return false;
                }
                return in_array($student->faculty_id, array_pluck($internship_faculties, 'object_id'));
                break;

            case INTERNSHIP_TYPE_MAJOR_STUDENTS:
                $internship_majors = $this->db->select('object_id')->where('internship_id', (int)$internship_id)->get('internship_objects')->result();
                $student = $this->db->select('major_id')->where('id', (int)$student_id)->get('students')->first_row();
                if (!$student) {
                    return false;
                }
                return in_array($student->major_id, array_pluck($internship_majors, 'object_id'));
                break;

            case INTERNSHIP_TYPE_CLASS_STUDENTS:
                $internship_classes = $this->db->select('object_id')->where('internship_id', (int)$internship_id)->get('internship_objects')->result();
                $student = $this->db->select('class_id')->where('id', (int)$student_id)->get('students')->first_row();
                if (!$student) {
                    return false;
                }
                return in_array($student->class_id, array_pluck($internship_classes, 'object_id'));
                break;

            case INTERNSHIP_TYPE_STUDENTS:
                $internship_students = $this->db->select('object_id')->where('internship_id', (int)$internship_id)->get('internship_objects')->result();
                $student = $this->db->select('id')->where('id', (int)$student_id)->get('students')->first_row();
                if (!$student) {
                    return false;
                }
                return in_array($student->id, array_pluck($internship_students, 'object_id'));
                break;
        }

        return false;
    }
}
