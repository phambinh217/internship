<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faculty_model extends MY_Model
{
    protected $fillable = array(
        'name', 'code',
    );

    public function with(array $data)
    {
        if (in_array('total_major', $data)) {
            $this->db
                ->select('count(majors.id) as total_major')
                ->join('majors', 'faculties.id=majors.faculty_id', 'left')
                ->group_by('faculties.id');
        }

        if (in_array('total_class', $data)) {
            $this->db
                ->select('count(classes.id) as total_class')
                ->join('classes', 'faculties.id=classes.faculty_id', 'left')
                ->group_by('faculties.id');
        }

        if (in_array('total_student', $data)) {
            $this->db
                ->select('count(students.id) as total_student')
                ->join('students', 'faculties.id=students.faculty_id', 'left')
                ->group_by('faculties.id');
        }

        return $this;
    }

    public function get($id)
    {
        return $this->db->where('faculties.id', $id)->get('faculties')->first_row();
    }

    public function get_or_fail($id, $fail_data = array(), $header_status = 404)
    {
        $result = $this->get($id);

        if ($result) {
            return $result;
        }

        if ($fail_data) {
            $this->output->json($fail_data, $header_status);
        }

        show_404();
    }

    public function all(array $data = array())
    {
        $this->query($data)->get('faculties')->result();
    }

    public function paginate(array $data = array())
    {
        if (!isset($data['perpage'])) {
            $perpage = 10;
        } else {
            $perpage = ((int)$data['perpage'] < 0) ? 10 : (int)$data['perpage'];
        }

        if (!isset($data['page'])) {
            $page = 1;
        } else {
            $page = ((int)$data['page'] < 1) ? 1 : (int)$data['page'];
        }

        $results = $this
            ->query($data)
            ->limit((int)$perpage, (int)($perpage * $page - $perpage))
            ->get('faculties')
            ->result();

        $total = $this
            ->query($data)
            ->from('faculties')
            ->count_all_results();

        return $this->pagination($total, $results, $perpage, $page);
    }

    public function query(array $data)
    {
        if (isset($data['with'])) {
            $this->with($data['with']);
        }

        if (isset($data['keyword'])) {
            $keyword = str_replace(' ', '%', str_std($data['keyword']));
            $this->db->like('CONCAT_WS(\'.\', code, name)', $keyword);
        }

        if (isset($data['order_by'])) {
            $allows = array('id', 'name', 'code', 'sort_order', 'created_at', 'updated_at');
            if (is_array($data['order_by'])) {
                foreach ($data['order_by'] as $by => $order) {
                    if (in_array($by, $allows)) {
                        $order = strtoupper($order);
                        $order = $order == 'DESC' ? 'DESC' : ($order == 'ASC' ? 'ASC' : 'DESC');
                        $this->db->order_by($by, $order);
                    }
                }
            }
        } else {
            $this->db->order_by('sort_order', 'ASC');
        }
        
        return $this->db;
    }

    public function create(array $data)
    {
        $data = array_only($data, $this->fillable);
        
        if ($data) {
            $this->db->set('created_at', 'NOW()', false);
            $this->db->set('updated_at', 'NOW()', false);
            $this->db->insert('faculties', $data);
            return $this->get($this->db->insert_id());
        }

        return null;
    }

    public function update(array $data, $id)
    {
        $data = array_only($data, $this->fillable);

        if ($data) {
            $this->db->set('updated_at', 'NOW()', false);
            $this->db->where('id', (int)$id)->update('faculties', $data);
        }

        return $this->get((int)$id);
    }

    public function update_or_fail(array $data, $id, $fail_data = array(), $header_status = 404)
    {
        $result = $this->update($data, $id);
        if ($result) {
            return $result;
        }

        if ($fail_data) {
            $this->output->json($fail_data, $header_status);
        }

        show_404();
    }

    public function delete($id)
    {
        $this->db->where('id', (int)$id)->delete('faculties');
    }

    public function sort_order($sort_orders)
    {
        $data = array();
        foreach ($sort_orders as $sort_order => $faculty) {
            $data[] = array(
                'id' => (int)$faculty['id'],
                'sort_order' => $sort_order,
            );
        }

        if ($data) {
            $this->db->update_batch('faculties', $data, 'id');
        }
    }
}
