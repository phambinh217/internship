<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lecturer_model extends MY_Model
{
    protected $fillable = array(
        'code', 'lastname', 'firstname', 'phone', 'email', 'faculty_id', 'forte', 'avatar', 'birth'
    );

    public function with(array $data)
    {
        if (in_array('faculty', $data)) {
            $this->db
                ->select('faculties.name as faculty_name, faculties.code as faculty_code')
                ->join('faculties', 'lecturers.faculty_id = faculties.id');
        }

        return $this;
    }

    public function get($id)
    {
        return $this->db->select('lecturers.*')->where('lecturers.id', $id)->get('lecturers')->first_row();
    }

    public function get_or_fail($id, $fail_data = array(), $header_status = 404)
    {
        $result = $this->get($id);

        if ($result) {
            return $result;
        }

        if ($fail_data) {
            $this->output->json($fail_data, $header_status);
        }

        show_404();
    }

    public function all(array $data = array())
    {
        $this->query($data)->select('lecturers.*')->get('lecturers')->result();
    }

    public function paginate(array $data = array())
    {
        if (!isset($data['perpage'])) {
            $perpage = 10;
        } else {
            $perpage = ((int)$data['perpage'] < 0) ? 10 : (int)$data['perpage'];
        }

        if (!isset($data['page'])) {
            $page = 1;
        } else {
            $page = ((int)$data['page'] < 1) ? 1 : (int)$data['page'];
        }

        $results = $this
            ->query($data)
            ->select('lecturers.*')
            ->limit((int)$perpage, (int)($perpage * $page - $perpage))
            ->get('lecturers')
            ->result();

        $total = $this
            ->query($data)
            ->select('lecturers.*')
            ->from('lecturers')
            ->count_all_results();

        return $this->pagination($total, $results, $perpage, $page);
    }

    public function query(array $data)
    {
        if (isset($data['with'])) {
            $this->with($data['with']);
        }

        if (isset($data['keyword'])) {
            $keyword = str_replace(' ', '%', str_std($data['keyword']));
            $this->db->like('CONCAT_WS(\'.\', lecturers.code, firstname, phone, email)', $keyword);
        }

        if (!empty($data['id'])) {
            $this->db->where('lecturers.id', (int)$data['id']);
        }

        if (!empty($data['code'])) {
            $this->db->where('lecturers.code', $data['code']);
        }

        if (!empty($data['faculty_code'])) {
            // connect student to faculty
            if (!in_array('faculty', $data['with'])) {
                $this->with(array('faculty'));
            }

            $this->db->where('faculties.code', $data['faculty_code']);
        }

        if (isset($data['order_by'])) {
            $allows = array('id', 'code', 'lastname', 'firstname', 'phone', 'email', 'faculty_id', 'forte', 'avatar', 'created_at', 'updated_at');
            if (is_array($data['order_by'])) {
                foreach ($data['order_by'] as $by => $order) {
                    if (in_array($by, $allows)) {
                        $order = strtoupper($order);
                        $order = $order == 'DESC' ? 'DESC' : ($order == 'ASC' ? 'ASC' : 'DESC');
                        $this->db->order_by($by, $order);
                    }
                }
            }
        }
        
        return $this->db;
    }

    public function create(array $data)
    {
        if (isset($data['fullname'])) {
            $name = explode_fullname($data['fullname']);
            $data['firstname'] = $name['firstname'];
            $data['lastname'] = $name['lastname'];
        }

        if (isset($data['birth'])) {
            $data['birth'] = date_format(date_create($data['birth']), 'Y-m-d');
        }

        $data = array_only($data, $this->fillable);

        if ($data) {
            $this->db->set('created_at', 'NOW()', false);
            $this->db->set('updated_at', 'NOW()', false);
            $this->db->insert('lecturers', $data);
            return $this->get($this->db->insert_id());
        }

        return null;
    }

    public function update(array $data, $id)
    {
        if (isset($data['fullname'])) {
            $name = explode_fullname($data['fullname']);
            $data['firstname'] = $name['firstname'];
            $data['lastname'] = $name['lastname'];
        }

        if (isset($data['birth'])) {
            $data['birth'] = date_format(date_create($data['birth']), 'Y-m-d');
        }

        $data = array_only($data, $this->fillable);

        if ($data) {
            $this->db->set('updated_at', 'NOW()', false);
            $this->db->where('id', (int)$id)->update('lecturers', $data);
        }

        return $this->get((int)$id);
    }

    public function update_or_fail(array $data, $id, $fail_data = array(), $header_status = 404)
    {
        $result = $this->update($data, $id);
        if ($result) {
            return $result;
        }

        if ($fail_data) {
            $this->output->json($fail_data, $header_status);
        }

        show_404();
    }

    public function delete($id)
    {
        $this->db->where('id', (int)$id)->delete('lecturers');
    }
}
