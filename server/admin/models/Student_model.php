<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student_model extends MY_Model
{
    protected $fillable = array(
        'code', 'phone', 'email', 'firstname', 'lastname', 'class_id', 'major_id', 'faculty_id', 'password', 'term', 'avatar', 'last_online_at', 'birth'
    );

    protected $show = array(
        'students.id', 'students.code', 'phone', 'email', 'firstname', 'lastname', 'class_id', 'students.major_id', 'students.faculty_id', 'students.term', 'avatar', 'last_online_at', 'birth'
    );

    public function with(array $data)
    {
        if (in_array('class', $data)) {
             $this->db
                ->select('classes.name as class_name, classes.code as class_code')
                ->join('classes', 'students.class_id = classes.id');
        }

        if (in_array('major', $data)) {
            $this->db
                ->select('majors.name as major_name, majors.code as major_code')
                ->join('majors', 'students.major_id = majors.id');
        }

        if (in_array('faculty', $data)) {
            $this->db
                ->select('faculties.name as faculty_name, faculties.code as faculty_code')
                ->join('faculties', 'students.faculty_id = faculties.id');
        }

        return $this;
    }

    public function get($id)
    {
        return $this->db->select(implode(',', $this->show))->where('students.id', $id)->get('students')->first_row();
    }

    public function get_or_fail($id, $fail_data = array(), $header_status = 404)
    {
        $result = $this->get($id);

        if ($result) {
            return $result;
        }

        if ($fail_data) {
            $this->output->json($fail_data, $header_status);
        }

        show_404();
    }

    public function all(array $data = array())
    {
        $this->query($data)->select(implode(',', $this->show))->get('students')->result();
    }

    public function paginate(array $data = array())
    {
        if (!isset($data['perpage'])) {
            $perpage = 10;
        } else {
            $perpage = ((int)$data['perpage'] < 0) ? 10 : (int)$data['perpage'];
        }

        if (!isset($data['page'])) {
            $page = 1;
        } else {
            $page = ((int)$data['page'] < 1) ? 1 : (int)$data['page'];
        }

        $total = $this
            ->query($data)
            ->select('students.*')
            ->from('students')
            ->count_all_results();

        $results = $this
            ->query($data)
            ->select('students.*')
            ->limit((int)$perpage, (int)($perpage * $page - $perpage))
            ->get('students')
            ->result();

        return $this->pagination($total, $results, $perpage, $page);
    }

    public function query(array $data)
    {
        if (isset($data['with'])) {
            $this->with($data['with']);
        }

        if (isset($data['keyword'])) {
            $keyword = str_replace(' ', '%', str_std($data['keyword']));
            $this->db->like('CONCAT_WS(\'.\', students.code, firstname, phone, email)', $keyword);
        }

        if (!empty($data['id'])) {
            $this->db->where('students.id', (int)$data['id']);
        }

        if (!empty($data['code'])) {
            $this->db->where('students.code', $data['code']);
        }

        if (!empty($data['term'])) {
            $this->db->where('students.term', $data['term']);
        }

        if (!empty($data['class_code'])) {
            // connect student to class
            if (!in_array('class', $data['with'])) {
                $this->with(array('class'));
            }

            $this->db->where('classes.code', $data['class_code']);
        }

        if (!empty($data['major_code'])) {
            // connect student to major
            if (!in_array('major', $data['with'])) {
                $this->with(array('major'));
            }

            $this->db->where('majors.code', $data['major_code']);
        }

        if (!empty($data['faculty_code'])) {
            // connect student to faculty
            if (!in_array('faculty', $data['with'])) {
                $this->with(array('faculty'));
            }

            $this->db->where('faculties.code', $data['faculty_code']);
        }

        if (isset($data['order_by'])) {
            $allows = array('id', 'code', 'phone', 'email', 'firstname', 'lastname', 'class_id', 'major_id', 'faculty_id', 'term', 'created_at', 'updated_at');
            if (is_array($data['order_by'])) {
                foreach ($data['order_by'] as $by => $order) {
                    if (in_array($by, $allows)) {
                        $order = strtoupper($order);
                        $order = $order == 'DESC' ? 'DESC' : ($order == 'ASC' ? 'ASC' : 'DESC');
                        $this->db->order_by($by, $order);
                    }
                }
            }
        }

        return $this->db;
    }

    public function create(array $data)
    {
        if (isset($data['birth'])) {
            $data['birth'] = date_format(date_create($data['birth']), 'Y-m-d');
        }

        if (isset($data['fullname'])) {
            $name = explode_fullname($data['fullname']);
            $data['firstname'] = $name['firstname'];
            $data['lastname'] = $name['lastname'];
        }

        if (isset($data['password'])) {
            $data['password'] = md5(md5($data['password']));
        }

        $data = array_only($data, $this->fillable);

        if ($data) {
            $this->db->set('created_at', 'NOW()', false);
            $this->db->set('updated_at', 'NOW()', false);
            $this->db->insert('students', $data);
            return $this->get($this->db->insert_id());
        }

        return null;
    }

    public function update(array $data, $id)
    {
        if (isset($data['birth'])) {
            $data['birth'] = date_format(date_create($data['birth']), 'Y-m-d');
        }

        if (isset($data['fullname'])) {
            $name = explode_fullname($data['fullname']);
            $data['firstname'] = $name['firstname'];
            $data['lastname'] = $name['lastname'];
        }

        if (isset($data['password'])) {
            $data['password'] = md5(md5($data['password']));
        }

        $data = array_only($data, $this->fillable);

        if ($data) {
            $this->db->set('updated_at', 'NOW()', false);
            $this->db->where('id', (int)$id)->update('students', $data);
        }

        return $this->get((int)$id);
    }

    public function update_or_fail(array $data, $id, $fail_data = array(), $header_status = 404)
    {
        $result = $this->update($data, $id);
        if ($result) {
            return $result;
        }

        if ($fail_data) {
            $this->output->json($fail_data, $header_status);
        }

        show_404();
    }

    public function delete($id)
    {
        $this->db->where('id', (int)$id)->delete('students');
    }
}
