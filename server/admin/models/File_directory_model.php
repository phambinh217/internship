<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class File_directory_model extends MY_Model
{
    protected $fillable = array(
        'name', 'parent_id'
    );

    public function with($data)
    {
        if (in_array('total_file', $data)) {
            $this->db
                ->select('count(files.id) as total_file')
                ->join('files', 'files.directory_id = file_directories.id', 'left')
                ->group_by('file_directories.id');
        }

        if (in_array('total_child', $data)) {
            $this->db
                ->select('count(bc2.id) as total_child')
                ->join('file_directories as bc2', 'bc2.parent_id = file_directories.id', 'left')
                ->group_by('file_directories.id');
        }

        return $this;
    }

    public function get($directory_id)
    {
        return $this->db
            ->select('file_directories.*')
            ->where('file_directories.id', $directory_id)
            ->get('file_directories')
            ->first_row();
    }

    public function get_or_fail($directory_id, $fail_data = array(), $header_status = 404)
    {
        $result = $this->get($directory_id);
        
        if ($result) {
            return $result;
        }

        if ($fail_data) {
            $this->output->json($fail_data, $header_status);
        }

        show_404();
    }

    public function paginate($data = array())
    {
        if (!isset($data['perpage'])) {
            $perpage = 10;
        } else {
            $perpage = ((int)$data['perpage'] < 0) ? 10 : (int)$data['perpage'];
        }

        if (!isset($data['page'])) {
            $page = 1;
        } else {
            $page = ((int)$data['page'] < 1) ? 1 : (int)$data['page'];
        }

        $results = $this
            ->query($data)
            ->limit((int)$perpage, (int)($perpage * $page - $perpage))
            ->order_by('file_directories.id', 'ASC')
            ->get('file_directories')
            ->result();

        $total = $this
            ->query($data)
            ->from('file_directories')
            ->count_all_results();

        return $this->pagination($total, $results, $perpage, $page);
    }

    public function all($data = array())
    {
        return $this
            ->query($data)
            ->order_by('file_directories.id', 'ASC')
            ->get('file_directories')
            ->result();
    }

    protected function query($data = array())
    {
        $this->db->select('file_directories.*');

        if (isset($data['with'])) {
            $this->with($data['with']);
        }

        if (isset($data['parent_id'])) {
            $this->db->where('file_directories.parent_id', (int)$data['parent_id']);
        }

        if (isset($data['order_by'])) {
            $allows = array('id', 'name', 'created_at', 'updated_at');
            if (is_array($data['order_by'])) {
                foreach ($data['order_by'] as $by => $order) {
                    if (in_array($by, $allows)) {
                        $order = strtoupper($order);
                        $order = $order == 'DESC' ? 'DESC' : ($order == 'ASC' ? 'ASC' : 'DESC');
                        $this->db->order_by($by, $order);
                    }
                }
            }
        }
        
        return $this->db;
    }

    public function create(array $data)
    {
        $data = array_only($data, $this->fillable);

        if ($data) {
            $this->db->set('created_at', 'NOW()', false);
            $this->db->set('updated_at', 'NOW()', false);   
            $this->db->insert('file_directories', $data);

            $directory = $this->get($this->db->insert_id());

            $results = $this->db
                ->where('directory_id', $directory->parent_id)
                ->order_by('level', 'ASC')
                ->get('file_directory_paths')
                ->result();
            
            $level = 0;

            // reset old data
            $data = array();

            foreach ($results as $result) {
                $data[] = array(
                    'directory_id' => $directory->id,
                    'path_id' => $result->path_id,
                    'level' => $level,
                );
                $level++;
            }

            $data[] = array(
                'directory_id' => $directory->id,
                'path_id'   => $directory->id,
                'level' => $level
            );

            if ($data) {
                $this->db->insert_batch('file_directory_paths', $data);
            }

            return $directory;
        }

        return null;
    }

    public function update(array $data, $directory_id)
    {
        $data = array_only($data, $this->fillable);
        $directory = $this->get($directory_id);

        if ($data) {
            $this->db->set('updated_at', 'NOW()', false);
            $this->db->where('id', $directory_id)->update('file_directories', $data);

            if (isset($data['parent_id']) && $directory->parent_id != $data['parent_id']) {
                $results = $this->db
                    ->where('path_id', $directory->id)
                    ->order_by('level', 'ASC')
                    ->get('file_directory_paths')
                    ->result();

                if ($results) {
                    foreach ($results as $directory_path) {
                        $this->db
                            ->where('directory_id', $directory_path->directory_id)
                            ->where('level <', $directory_path->level)
                            ->delete('file_directory_paths');

                        $path = array();

                        $query = $this->db
                            ->where('directory_id', $directory->parent_id)
                            ->order_by('level', 'ASC');

                        foreach ($query->result() as $result) {
                            $path[] = $result['path_id'];
                        }
                        
                        $query = $this->db
                            ->where('directory_id', $directory_path->directory_id)
                            ->order_by('level', 'ASC');

                        foreach ($query->result() as $result) {
                            $path[] = $result['path_id'];
                        }

                        $level = 0;

                        foreach ($path as $path_id) {
                            // reset old data
                            $data = array(
                                'directory_id' => $directory_path->directory_id,
                                'path_id' => $path_id,
                                'level' => $level,
                            );
                            $this->db->replace('file_directory_paths', $data);
                            $level++;
                        }
                    }
                } else {
                    $this->db
                        ->where('directory_id', $directory->id)
                        ->delete('file_directory_paths');

                    $level = 0;

                    $query = $this->db
                        ->where('directory_id', $directory->parent_id)
                        ->order_by('level', 'ASC');

                    // reset old data
                    $data = array();

                    foreach ($query->result() as $result) {
                        $data[] = array(
                            'directory_id' => $directory->id,
                            'path_id' => $result->path_id,
                            'level' => $level
                        );
                        $level++;
                    }

                    if ($data) {
                        $this->db->insert_bath('file_directory_paths', $data);
                    }

                    // reset old data
                    $data = array(
                        'directory_id' => $directory->id,
                        'path_id' => $directory->id,
                        'level' => $level,
                    );
                    $this->db->replace('file_directory_paths', $data);
                }
            }
        }

        return $directory;
    }

    public function update_or_fail(array $data, $directory_id, $fail_data = array(), $header_status = 404)
    {
        $result = $this->update($data, $directory_id);
        if ($result) {
            return $result;
        }

        if ($fail_data) {
            $this->output->json($fail_data, $header_status);
        }

        show_404();
    }

    public function get_path_ids($directory_id)
    {
        $stacks = array();        
        $results = $this->db
            ->select('path_id')
            ->where('directory_id', (int)$directory_id)
            ->order_by('level', 'ASC')
            ->get('file_directory_paths')
            ->result();

        foreach ($results as $result) {
            $stacks[] = $result->path_id;
        }

        return $stacks;
    }

    public function delete($directory_id)
    {
        $this->db->where('id', $directory_id)->delete('file_directories');
        $this->db->where('directory_id', $directory_id)->delete('file_directory_paths');

        return true;
    }
}
