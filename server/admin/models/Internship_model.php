<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Internship_model extends MY_Model
{
    protected $fillable = array(
        'name', 'type', 'term', 'date_open', 'date_close'
    );

    public function with(array $data)
    {
        // if (in_array('object', $data)) {

        // }

        return $this;
    }

    public function get($id)
    {
        return $this->db
            ->select('internships.*, DATEDIFF(date_close, NOW()) as day_left, DATEDIFF(date_close, NOW()) > 0 as is_open')
            ->where('id', $id)
            ->get('internships')
            ->first_row();
    }

    public function get_or_fail($id, $fail_data = array(), $header_status = 404)
    {
        $result = $this->get($id);

        if ($result) {
            return $result;
        }

        if ($fail_data) {
            $this->output->json($fail_data, $header_status);
        }

        show_404();
    }

    public function all(array $data = array())
    {
        $this->query($data)->get('internships')->result();
    }

    public function paginate(array $data = array())
    {
        if (!isset($data['perpage'])) {
            $perpage = 10;
        } else {
            $perpage = ((int)$data['perpage'] < 0) ? 10 : (int)$data['perpage'];
        }

        if (!isset($data['page'])) {
            $page = 1;
        } else {
            $page = ((int)$data['page'] < 1) ? 1 : (int)$data['page'];
        }

        $total = $this
            ->query($data)
            ->from('internships')
            ->count_all_results();

        $results = $this
            ->query($data)
            ->select('internships.*, DATEDIFF(date_close, NOW()) as day_left, DATEDIFF(date_close, NOW()) > 0 as is_open')
            ->limit((int)$perpage, (int)($perpage * $page - $perpage))
            ->get('internships')
            ->result();

        return $this->pagination($total, $results, $perpage, $page);
    }

    public function query(array $data)
    {
        if (isset($data['with'])) {
            $this->with($data['with']);
        }

        if (isset($data['keyword'])) {
            // $keyword = str_replace(' ', '%', str_std($data['keyword']));
            $keyword = str_std($data['keyword']);
            $this->db->like('CONCAT_WS(\'.\', name, term)', $keyword);
        }

        if (isset($data['order_by'])) {
            $allows = array('id', 'name', 'type', 'term', 'date_open', 'date_close', 'created_at', 'updated_at');
            if (is_array($data['order_by'])) {
                foreach ($data['order_by'] as $by => $order) {
                    if (in_array($by, $allows)) {
                        $order = strtoupper($order);
                        $order = $order == 'DESC' ? 'DESC' : ($order == 'ASC' ? 'ASC' : 'DESC');
                        $this->db->order_by($by, $order);
                    }
                }
            }
        }

        return $this->db;
    }

    public function create(array $data)
    {
        $type = 1;
        $students = array();
        $lecturers = array();
        $faculties = array();
        $classes = array();
        $majors = array();

        if (isset($data['type'])) {
            $type = (int)$data['type'];
        }

        if (isset($data['student_id']) && is_array($data['student_id'])) {
            $students = $data['student_id'];
        }

        if (isset($data['lecturer']) && is_array($data['lecturer'])) {
            $lecturers = $data['lecturer'];
        }

        if (isset($data['major_id']) && is_array($data['major_id'])) {
            $majors = $data['major_id'];
        }

        if (isset($data['class_id']) && is_array($data['class_id'])) {
            $classes = $data['class_id'];
        }

        if (isset($data['faculty_id']) && is_array($data['faculty_id'])) {
            $faculties = $data['faculty_id'];
        }

        if (isset($data['date_open'])) {
            $data['date_open'] = date_format(date_create($data['date_open']), 'Y-m-d');
        }

        if (isset($data['date_close'])) {
            $data['date_close'] = date_format(date_create($data['date_close']), 'Y-m-d');
        }

        $data = array_only($data, $this->fillable);

        $internship = null;

        if ($data) {
            $this->db->set('created_at', 'NOW()', false);
            $this->db->set('updated_at', 'NOW()', false);
            $this->db->insert('internships', $data);
            $internship = $this->get($this->db->insert_id());

            $data = array();
            foreach ($lecturers as $lecturer) {
                if (is_array($lecturer) && isset($lecturer['id'])) {
                    $data[] = array(
                        'internship_id' => $internship->id,
                        'lecturer_id' => (int)$lecturer['id'],
                        'limit_student' => isset($lecturer['limit_student']) ? (int)$lecturer['limit_student'] : 0,
                    );
                }
            }

            if ($data) {
                $this->db->insert_batch('internship_lecturers', $data);
            }

            if ($type != 1) {
                $data = array();
                foreach ($faculties as $faculty_id) {
                    $data[] = array(
                        'type' => INTERNSHIP_TYPE_FACULTY_STUDENTS,
                        'object_id' => $faculty_id,
                        'internship_id' => $internship->id,
                    );
                }

                foreach ($majors as $major_id) {
                    $data[] = array(
                        'type' => INTERNSHIP_TYPE_MAJOR_STUDENTS,
                        'object_id' => $major_id,
                        'internship_id' => $internship->id,
                    );
                }

                foreach ($classes as $class_id) {
                    $data[] = array(
                        'type' => INTERNSHIP_TYPE_CLASS_STUDENTS,
                        'object_id' => $class_id,
                        'internship_id' => $internship->id,
                    );
                }

                foreach ($students as $student_id) {
                    $data[] = array(
                        'type' => INTERNSHIP_TYPE_STUDENTS,
                        'object_id' => $student_id,
                        'internship_id' => $internship->id,
                    );
                }



                if ($data) {
                    $this->db->insert_batch('internship_objects', $data);
                }
            }
        }

        return $internship;
    }

    public function update(array $data, $id)
    {
        $type = 1;
        $students = array();
        $lecturers = array();
        $faculties = array();
        $classes = array();
        $majors = array();

        if (isset($data['type'])) {
            $type = (int)$data['type'];
        }

        if (isset($data['student_id']) && is_array($data['student_id'])) {
            $students = $data['student_id'];
        }

        if (isset($data['lecturer']) && is_array($data['lecturer'])) {
            $lecturers = $data['lecturer'];
        }

        if (isset($data['major_id']) && is_array($data['major_id'])) {
            $majors = $data['major_id'];
        }

        if (isset($data['class_id']) && is_array($data['class_id'])) {
            $classes = $data['class_id'];
        }

        if (isset($data['faculty_id']) && is_array($data['faculty_id'])) {
            $faculties = $data['faculty_id'];
        }

        if (isset($data['date_open'])) {
            $data['date_open'] = date_format(date_create($data['date_open']), 'Y-m-d');
        }

        if (isset($data['date_close'])) {
            $data['date_close'] = date_format(date_create($data['date_close']), 'Y-m-d');
        }

        $data = array_only($data, $this->fillable);

        $internship = null;

        if ($data) {
            $this->db->set('created_at', 'NOW()', false);
            $this->db->set('updated_at', 'NOW()', false);
            $this->db->where('internships.id', (int)$id)->update('internships', $data);
            $internship = $this->get((int)$id);

            $data = array();
            foreach ($lecturers as $lecturer) {
                if (is_array($lecturer) && isset($lecturer['id'])) {
                    $data[] = array(
                        'internship_id' => $internship->id,
                        'lecturer_id' => (int)$lecturer['id'],
                        'limit_student' => isset($lecturer['limit_student']) ? (int)$lecturer['limit_student'] : 0,
                    );
                }
            }

            if ($data) {
                $this->db->where('internship_id', $internship->id)->delete('internship_lecturers');
                $this->db->insert_batch('internship_lecturers', $data);
            }

            if ($type != 1) {
                $data = array();
                foreach ($faculties as $faculty_id) {
                    $data[] = array(
                        'type' => INTERNSHIP_TYPE_FACULTY_STUDENTS,
                        'object_id' => $faculty_id,
                        'internship_id' => $internship->id,
                    );
                }

                foreach ($majors as $major_id) {
                    $data[] = array(
                        'type' => INTERNSHIP_TYPE_MAJOR_STUDENTS,
                        'object_id' => $major_id,
                        'internship_id' => $internship->id,
                    );
                }

                foreach ($classes as $class_id) {
                    $data[] = array(
                        'type' => INTERNSHIP_TYPE_CLASS_STUDENTS,
                        'object_id' => $class_id,
                        'internship_id' => $internship->id,
                    );
                }

                foreach ($students as $student_id) {
                    $data[] = array(
                        'type' => INTERNSHIP_TYPE_STUDENTS,
                        'object_id' => $student_id,
                        'internship_id' => $internship->id,
                    );
                }

                if ($data) {
                    $this->db->where('internship_id', $internship->id)->delete('internship_objects');
                    $this->db->insert_batch('internship_objects', $data);
                }
            }
        }

        return $internship;
    }

    public function update_or_fail(array $data, $id, $fail_data = array(), $header_status = 404)
    {
        $result = $this->update($data, $id);
        if ($result) {
            return $result;
        }

        if ($fail_data) {
            $this->output->json($fail_data, $header_status);
        }

        show_404();
    }

    public function delete($id)
    {
        $this->db->where('id', (int)$id)->delete('internships');
    }

    public function typeable()
    {
        $types = array();
        $types[0] = new stdClass();
        $types[0]->id = INTERNSHIP_TYPE_ALL_STUDENTS;
        $types[0]->name = 'All students';

        $types[1] = new stdClass();
        $types[1]->id = INTERNSHIP_TYPE_FACULTY_STUDENTS;
        $types[1]->name = 'Students in faculties';

        $types[2] = new stdClass();
        $types[2]->id = INTERNSHIP_TYPE_MAJOR_STUDENTS;
        $types[2]->name = 'Student in majors';

        $types[3] = new stdClass();
        $types[3]->id = INTERNSHIP_TYPE_CLASS_STUDENTS;
        $types[3]->name = 'Student in classes';

        $types[4] = new stdClass();
        $types[4]->id = INTERNSHIP_TYPE_STUDENTS;
        $types[4]->name = 'Only students';
    }

    public function get_objects($id, $type)
    {
        $data = array();
        switch ($type) {
            case 2:
                $data = $this->db
                    ->select('faculties.id, faculties.code, faculties.name')
                    ->join('internship_objects', 'internship_objects.object_id = faculties.id AND internship_objects.type = 2')
                    ->where('internship_objects.internship_id', (int)$id)
                    ->get('faculties')
                    ->result();
                break;
            case 3:
                $data = $this->db
                    ->select('majors.id, majors.code, majors.name')
                    ->join('internship_objects', 'internship_objects.object_id = majors.id AND internship_objects.type = 3')
                    ->where('internship_objects.internship_id', (int)$id)
                    ->get('majors')
                    ->result();
                break;
            case 4:
                $data = $this->db
                    ->select('classes.id, classes.code, classes.name')
                    ->join('internship_objects', 'internship_objects.object_id = classes.id AND internship_objects.type = 4')
                    ->where('internship_objects.internship_id', (int)$id)
                    ->get('classes')
                    ->result();
                break;

            case 5:
                $data = $this->db
                    ->select('students.id, students.code, students.lastname, students.firstname')
                    ->join('internship_objects', 'internship_objects.object_id = students.id AND internship_objects.type = 5')
                    ->where('internship_objects.internship_id', (int)$id)
                    ->get('students')
                    ->result();
                break;
        }

        return $data;
    }

    public function get_lecturers($id)
    {
        return $this->db
            ->select('lecturers.id, lecturers.code, lecturers.avatar, lecturers.lastname, lecturers.firstname, lecturers.avatar, lecturers.phone, lecturers.forte, internship_lecturers.limit_student, internship_lecturers.internship_id')
            ->join('internship_lecturers', 'internship_lecturers.lecturer_id = lecturers.id')
            ->where('internship_id', (int)$id)
            ->get('lecturers')
            ->result();
    }

    public function get_lecturers_in($ids)
    {
        if ($ids) {
            $lecturers = $this->db
                ->select('lecturers.id, lecturers.code, lecturers.avatar, lecturers.lastname, lecturers.firstname, lecturers.avatar, lecturers.phone, lecturers.forte, internship_lecturers.limit_student, internship_lecturers.internship_id')
                ->join('internship_lecturers', 'internship_lecturers.lecturer_id = lecturers.id')
                ->where_in('internship_id', $ids)
                ->get('lecturers')
                ->result();

            $internship_lecturer = array();
            foreach ($lecturers as $lecturer) {
                $internship_lecturer[$lecturer->internship_id][] = $lecturer;
            }

            return $internship_lecturer;
        }
    }
}
