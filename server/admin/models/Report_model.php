<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_model extends CI_Model
{
    protected $offset_online = 60*15;

    public function count_users_online()
    {
        return $this->db
            ->select('(last_online_at + '. $this->offset_online .' > UNIX_TIMESTAMP()) as is_online')
            ->where('last_online_at + '. $this->offset_online .' >', 'UNIX_TIMESTAMP()', false)
            ->from('users')
            ->count_all_results();
    }
}
