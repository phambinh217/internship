<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Major_model extends MY_Model
{
    protected $fillable = array(
        'name', 'faculty_id', 'code',
    );

    public function with(array $data)
    {
        if (in_array('total_class', $data)) {
            $this->db
                ->select('count(classes.id) as total_class')
                ->join('classes', 'majors.id=classes.major_id', 'left')
                ->group_by('majors.id');
        }

        if (in_array('total_student', $data)) {
            $this->db
                ->select('count(students.id) as total_student')
                ->join('students', 'majors.id=students.major_id', 'left')
                ->group_by('majors.id');
        }

        return $this;
    }

    public function get($id)
    {
        return $this->db->where('majors.id', $id)->get('majors')->first_row();
    }

    public function get_or_fail($id, $fail_data = array(), $header_status = 404)
    {
        $result = $this->get($id);

        if ($result) {
            return $result;
        }

        if ($fail_data) {
            $this->output->json($fail_data, $header_status);
        }

        show_404();
    }

    public function all(array $data = array())
    {
        $this->query($data)->get('majors')->result();
    }

    public function paginate(array $data = array())
    {
        if (!isset($data['perpage'])) {
            $perpage = 10;
        } else {
            $perpage = ((int)$data['perpage'] < 0) ? 10 : (int)$data['perpage'];
        }

        if (!isset($data['page'])) {
            $page = 1;
        } else {
            $page = ((int)$data['page'] < 1) ? 1 : (int)$data['page'];
        }

        $results = $this
            ->query($data)
            ->limit((int)$perpage, (int)($perpage * $page - $perpage))
            ->get('majors')
            ->result();

        $total = $this
            ->query($data)
            ->from('majors')
            ->count_all_results();

        return $this->pagination($total, $results, $perpage, $page);
    }

    public function query(array $data)
    {
        if (isset($data['with'])) {
            $this->with($data['with']);
        }

        if (isset($data['keyword'])) {
            $keyword = str_replace(' ', '%', str_std($data['keyword']));
            $this->db->like('CONCAT_WS(\'.\', code, name)', $keyword);
        }

        if (isset($data['order_by'])) {
            $allows = array('id', 'name', 'code', 'sort_order', 'created_at', 'updated_at');
            if (is_array($data['order_by'])) {
                foreach ($data['order_by'] as $by => $order) {
                    if (in_array($by, $allows)) {
                        $order = strtoupper($order);
                        $order = $order == 'DESC' ? 'DESC' : ($order == 'ASC' ? 'ASC' : 'DESC');
                        $this->db->order_by($by, $order);
                    }
                }
            }
        } else {
            $this->db->order_by('sort_order', 'ASC');
        }
        
        return $this->db;
    }

    public function create(array $data)
    {
        $data = array_only($data, $this->fillable);
        if ($data) {
            $this->db->set('created_at', 'NOW()', false);
            $this->db->set('updated_at', 'NOW()', false);
            $this->db->insert('majors', $data);
            return $this->get($this->db->insert_id());
        }
        
        return null;
    }

    public function update(array $data, $id)
    {
        $data = array_only($data, $this->fillable);
        if ($data) {
            $this->db->set('updated_at', 'NOW()', false);
            $this->db->where('id', (int)$id)->update('majors', $data);
        }

        return $this->get((int)$id);
    }

    public function update_or_fail(array $data, $id, $fail_data = array(), $header_status = 404)
    {
        $result = $this->update($data, $id);
        if ($result) {
            return $result;
        }

        if ($fail_data) {
            $this->output->json($fail_data, $header_status);
        }

        show_404();
    }

    public function delete($id)
    {
        $this->db->where('id', (int)$id)->delete('majors');
    }

    public function sort_order($sort_orders)
    {
        $data = array();
        foreach ($sort_orders as $sort_order => $major) {
            $data[] = array(
                'id' => (int)$major['id'],
                'sort_order' => $sort_order,
            );
        }

        if ($data) {
            $this->db->update_batch('majors', $data, 'id');
        }
    }
}
