<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role_model extends MY_Model
{
    protected $fillable = array(
        'name', 'permission',
    );

    public function with(array $data)
    {
        if (in_array('total_user', $data)) {
            $this->db
                ->select('count(users.id) as total_user')
                ->join('users', 'roles.id=users.role_id', 'left')
                ->group_by('roles.id');
        }

        return $this;
    }

    public function get($id)
    {
        $result = $this->db
            ->select('roles.*')
            ->where('roles.id', $id)
            ->get('roles')
            ->first_row();
        
        if ($result && !empty($result->permission)) {
            if ($result->permission != '*' && $result->permission != '0') {
                $result->permission = unserialize($result->permission);
            }
        }

        return $result;
    }

    public function get_or_fail($id, $fail_data = array(), $header_status = 404)
    {
        $result = $this->get($id);
        
        if ($result) {
            return $result;
        }

        if ($fail_data) {
            $this->output->json($fail_data, $header_status);
        }

        show_404();
    }

    public function all(array $data = array())
    {
        return $this->query($data)->select('roles.*')->get('roles')->result();
    }

    public function paginate(array $data = array())
    {
        if (!isset($data['perpage'])) {
            $perpage = 10;
        } else {
            $perpage = ((int)$data['perpage'] < 0) ? 10 : (int)$data['perpage'];
        }

        if (!isset($data['page'])) {
            $page = 1;
        } else {
            $page = ((int)$data['page'] < 1) ? 1 : (int)$data['page'];
        }

        $results = $this
            ->query($data)
            ->select('roles.*')
            ->limit((int)$perpage, (int)($perpage * $page - $perpage))
            ->get('roles')
            ->result();

        $total = $this
            ->query($data)
            ->from('roles')
            ->count_all_results();

        return $this->pagination($total, $results, $perpage, $page);
    }

    public function query(array $data)
    {
        if (isset($data['with'])) {
            $this->with($data['with']);
        }

        // if (isset($data['data_key'])) {
        //         
        // }
        
        return $this->db;
    }

    public function create(array $data)
    {
        if (isset($data['permission']) && is_array($data['permission'])) {
            $data['permission'] = serialize($data['permission']);
        }

        $data = array_only($data, $this->fillable);

        if ($data) {
            $this->db->insert('roles', $data);
            return $this->get($this->db->insert_id());
        }
        
        return null;
    }

    public function update(array $data, $id)
    {
        if (isset($data['type'])) {
            if ($data['type'] != 'option') {
                $data['permission'] = $data['type'];
            }
        }

        if (isset($data['permission']) && is_array($data['permission'])) {
            $data['permission'] = serialize($data['permission']);
        }

        $data = array_only($data, $this->fillable);

        if ($data) {
            $this->db->where('id', (int)$id)->update('roles', $data);
        }

        return $this->get((int)$id);
    }

    public function update_or_fail(array $data, $id, $fail_data = array(), $header_status = 404)
    {
        $result = $this->update($data, $id);
        if ($result) {
            return $result;
        }

        if ($fail_data) {
            $this->output->json($fail_data, $header_status);
        }

        show_404();
    }

    public function delete($id)
    {
        $this->db->where('id', (int)$id)->delete('roles');
    }
}
