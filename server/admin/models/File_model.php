<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class File_model extends MY_Model
{
    protected $fillable = array(
        'path', 'size', 'type', 'name', 'directory_id'
    );

    public function get($file_id)
    {
        return $this->db
            ->select('files.*')
            ->where('files.id', $file_id)
            ->get('files')
            ->first_row();
    }

    public function get_or_fail($file_id, $fail_data = array(), $header_status = 404)
    {
        $result = $this->get($file_id);
        
        if ($result) {
            return $result;
        }

        if ($fail_data) {
            $this->output->json($fail_data, $header_status);
        }

        show_404();
    }

    public function paginate($data = array())
    {
        if (!isset($data['perpage'])) {
            $perpage = 10;
        } else {
            $perpage = ((int)$data['perpage'] < 0) ? 10 : (int)$data['perpage'];
        }

        if (!isset($data['page'])) {
            $page = 1;
        } else {
            $page = ((int)$data['page'] < 1) ? 1 : (int)$data['page'];
        }

        $results = $this
            ->query($data)
            ->limit((int)$perpage, (int)($perpage * $page - $perpage))
            ->get('files')
            ->result();

        $total = $this
            ->query($data)
            ->from('files')
            ->count_all_results();

        return $this->pagination($total, $results, $perpage, $page);
    }

    public function all($data = array())
    {
        return $this
            ->query($data)
            ->order_by('files.id', 'ASC')
            ->get('files')
            ->result();
    }

    protected function query($data)
    {
        $this->db->select('files.*');
    
        if (isset($data['directory_id'])) {
            $this->db->where('directory_id', (int)$data['directory_id']);
        }

        if (isset($data['order_by'])) {
            $allows = array('id', 'name', 'created_at', 'updated_at');
            if (is_array($data['order_by'])) {
                foreach ($data['order_by'] as $by => $order) {
                    if (in_array($by, $allows)) {
                        $order = strtoupper($order);
                        $order = $order == 'DESC' ? 'DESC' : ($order == 'ASC' ? 'ASC' : 'DESC');
                        $this->db->order_by($by, $order);
                    }
                }
            }
        }
        
        return $this->db;
    }

    public function create(array $data)
    {
        $data = array_only($data, $this->fillable);

        if ($data) {
            $this->db->set('uploaded_at', 'NOW()', false);
            $this->db->insert('files', $data);
            return $this->get($this->db->insert_id());
        }

        return null;
    }

    public function update(array $data, $file_id)
    {
        $data = array_only($data, $this->fillable);
        if ($data) {
            $this->db->where('id', $file_id)->update('files', $data);
        }
        
        return $this->get($file_id);
    }

    public function update_or_fail(array $data, $file_id, $fail_data = array(), $header_status = 404)
    {
        $result = $this->update($data, $file_id);
        if ($result) {
            return $result;
        }

        if ($fail_data) {
            $this->output->json($fail_data, $header_status);
        }

        show_404();
    }

    public function delete($file_id)
    {
        $file = $this->get($file_id);
        $this->db->where('id', $file_id)->delete('files');
        @unlink(FCPATH . $file->path);

        return true;
    }
}
