<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class V2_user_model extends MY_Model
{
    public function get($id, array $data = array())
    {
        return $this->query($data)->where('users.id', (int)$id)->first_row();
    }

    public function paginate($data)
    {
        if (!isset($data['perpage'])) {
            $perpage = 10;
        } else {
            $perpage = ((int)$data['perpage'] < 0) ? 10 : (int)$data['perpage'];
        }

        if (!isset($data['page'])) {
            $page = 1;
        } else {
            $page = ((int)$data['page'] < 1) ? 1 : (int)$data['page'];
        }

        $total = $this
            ->query($data)
            ->from('users')
            ->count_all_results();

        $results = $this
            ->query($data)
            ->limit((int)$perpage, (int)($perpage * $page - $perpage))
            ->get('users')
            ->result();

        return $this->pagination($total, $results, $perpage, $page);
    }

    protected function query(array $data = array())
    {
        $joined_role = false;

        if (!isset($data['fields']) || !$data['fields']) {
            $this->db->select('users.id, firstname, lastname, birth, avatar, email, phone');
        } else {
            $fields = (array) $data['fields'];

            if (in_array('id', $fields)) {
                $this->db->select('id');
            }
            if (in_array('firstname', $fields)) {
                $this->db->select('firstname');
            }
            if (in_array('lastname', $fields)) {
                $this->db->select('lastname');
            }
            if (in_array('birth', $fields)) {
                $this->db->select('birth');
            }
            if (in_array('avatar', $fields)) {
                $this->db->select('avatar');
            }
            if (in_array('email', $fields)) {
                $this->db->select('email');
            }
            if (in_array('phone', $fields)) {
                $this->db->select('phone');
            }
            if (in_array('role_id', $fields)) {
                $this->db->select('role_id');
            }
            if (in_array('role_name', $fields)) {
                if (!$joined_role) {
                    $this->db->join('roles', 'users.role_id=roles.id', 'left');
                    $joined_role = true;
                }
                $this->db->select('roles.name as role_name');
            }
            if (in_array('role_permission', $fields)) {
                if (!$joined_role) {
                    $this->db->join('roles', 'users.role_id=roles.id', 'left');
                    $joined_role = true;
                }

                $this->db->select('roles.permission as role_permission');
            }
            if (in_array('created_at', $fields)) {
                $this->db->select('created_at');
            }
            if (in_array('updated_at', $fields)) {
                $this->db->select('updated_at');
            }
        }
        
        if (isset($data['role_id'])) {
            if (is_array($data['role_id'])) {
                $this->db->where_in('users.role_id', $data['role_id']);
            } else {
                $this->db->where('users.role_id', (int)$data['role_id']);
            }
        }

        return $this->db;
    }
}
