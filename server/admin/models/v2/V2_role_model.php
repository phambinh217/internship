<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class V2_role_model extends MY_Model
{
    // Has many users
    public function users($results, $type, array $data = array())
    {
        $this->load->model('v2/v2_user_model');
        return $this->has_many($results, $type, $this->v2_user_model, 'users', 'role_id', 'id', $data);
    }

    public function get($id, array $data = array())
    {
        $result = $this->query($data)->where('roles.id', (int)$id)->first_row();
        if (isset($data['users'])) {
            $this->users($results, 'row', $data['users']);
        }

        return $results;
    }

    public function paginate($data)
    {
        if (!isset($data['perpage'])) {
            $perpage = 10;
        } else {
            $perpage = ((int)$data['perpage'] < 0) ? 10 : (int)$data['perpage'];
        }

        if (!isset($data['page'])) {
            $page = 1;
        } else {
            $page = ((int)$data['page'] < 1) ? 1 : (int)$data['page'];
        }

        $total = $this
            ->query($data)
            ->from('roles')
            ->count_all_results();

        $results = $this
            ->query($data)
            ->limit((int)$perpage, (int)($perpage * $page - $perpage))
            ->get('roles')
            ->result();

        if (isset($data['fields']['users'])) {
            $this->users($results, 'list', $data['fields']['users']);
        }

        return $this->pagination($total, $results, $perpage, $page);
    }

    protected function query(array $data = array())
    {
        if (!isset($data['fields'])) {
            $this->db->select('id, name, permission');
        } else {
            $fields = (array)$data['fields'];

            if (in_array('id', $fields)) {
                $this->db->select('roles.id');
            }
            if (in_array('name', $fields)) {
                $this->db->select('roles.name');
            }
            if (in_array('permission', $fields)) {
                $this->db->select('permission');
            }
            if (in_array('total_user', $fields)) {
                $this->db
                    ->select('count(total_user.id) as total_user')
                    ->join('users as total_user', 'roles.id=total_user.role_id', 'left')
                    ->group_by('roles.id');
            }
        }
        
        return $this->db;
    }
}
