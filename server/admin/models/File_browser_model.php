<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class File_browser_model extends MY_Model
{
    public function paginate($data = array())
    {
        $sql1 = " SELECT file_directories.id as id, CONCAT('') as path, file_directories.name, CONCAT('') as size, CONCAT('folder') as type, file_directories.created_at as date_create, CONCAT('0') as pri_index FROM file_directories WHERE 1";
        $sql2 = "SELECT files.id as id, files.path as path, files.name as name, files.size as size, files.type as type, files.uploaded_at as date_create, CONCAT('1') as pri_index FROM files WHERE 1";

        if (isset($data['directory_id'])) {
            $sql1 .= " AND parent_id = '". (int)$data['directory_id']."'";
            $sql2 .= " AND directory_id = '". (int)$data['directory_id']."'";
        }

        if (isset($data['keyword']) && !empty($data['keyword'])) {
            $data['keyword'] = $this->db->escape('%'.$data['keyword'].'%');
            $data['keyword'] = str_replace(' ', '%', $data['keyword']);

            $sql1 .= " AND name LIKE ". $data['keyword'];
            $sql2 .= " AND name LIKE ". $data['keyword'];
        }

        if (isset($data['type'])) {
            $type_image = array(
                "'image/bmp'", "'image/x-bmp'", "'image/x-bitmap'", "'image/x-xbitmap'", "'image/x-win-bitmap'", "'image/x-windows-bmp'", "'image/ms-bmp'", "'image/x-ms-bmp'", "'application/bmp'", "'application/x-bmp'", "'application/x-win-bitmap'",
                "'image/gif'",
                "'image/jpeg'", "'image/pjpeg'",
                "'image/png'",  "'image/x-png'",
                "'image/tiff'",
            );

            $type_audio = array(
                "'audio/midi'",
                "'audio/mpeg'", "'audio/mpg'", "'audio/mpeg3'", "'audio/mp3'",
                "'audio/x-aiff'", "'audio/aiff'",
                "'audio/x-pn-realaudio'",
                "'audio/x-pn-realaudio-plugin'",
                "'audio/x-realaudio'",
            );

            $type_video = array(
                "'video/vnd.rn-realvideo'",
                "'video/mp4'",
                "'video/mp4'", "'video/x-f4v'",
                "'video/3gpp2'",
                "'video/3gp'", "'video/3gpp'",
                "'video/x-flv'",
                "'video/webm'",
                "'video/x-ms-wmv'", "'video/x-ms-asf'",
                "'video/vnd.rn-realvideo'",
                "'video/mpeg'",
                "'video/quicktime'",
                "'video/x-msvideo'", "'video/msvideo'", "'video/avi'", "'application/x-troff-msvideo'",
            );

            $allow = array();

            if (is_array($data['type'])) {
                foreach ($data['type'] as $type) {
                    $type = 'type_' . $type;
                    if (isset($$type)) {
                        $allow = array_merge($allow, $$type);
                    }
                }
            } else if (in_array($data['type'], array('image', 'audio', 'video'))) {
                $type = 'type_' . $data['type'];
                if (isset($$type)) {
                    $allow = $$type;
                }
            }

            if ($allow) {
                $sql2 .= " AND type IN(". implode(',', $allow) .")";
            }
        }

        $sql = "($sql1) UNION ($sql2)";

        $total = $this->db->query("SELECT COUNT(*) as total FROM ($sql) as t1")->first_row()->total;

        $sql .= ' ORDER BY pri_index ASC, id DESC';

        if (isset($data['sort']) && in_array($data['sort'], array('name', 'date_create'))) {
            $sql .= ', ' . $data['sort'];

            if (isset($data['order']) && in_array($data['order'], array('asc', 'ASC', 'desc', 'DESC'))) {
                $sql .= ' ' . strtoupper($data['order']);
            }
        }

        if (!isset($data['perpage'])) {
            $perpage = 10;
        } else {
            $perpage = ((int)$data['perpage'] < 0) ? 10 : (int)$data['perpage'];
        }

        if (!isset($data['page'])) {
            $page = 1;
        } else {
            $page = ((int)$data['page'] < 1) ? 1 : (int)$data['page'];
        }

        $sql .= ' LIMIT ' . (int)($perpage * $page - $perpage) . ', ' . $perpage;

        $results = $this->db->query($sql)->result();

        return $this->pagination($total, $results, $perpage, $page);
    }
}