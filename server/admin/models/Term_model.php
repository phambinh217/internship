<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Term_model extends MY_Model
{
    protected $fillable = array(
        'term'
    );

    public function with(array $data)
    {
        
        if (in_array('total_class', $data)) {
            $this->db
                ->select('count(classes.id) as total_class')
                ->join('classes', 'terms.term=classes.term', 'left')
                ->group_by('terms.term');
        }

        if (in_array('total_student', $data)) {
            $this->db
                ->select('count(students.id) as total_student')
                ->join('students', 'terms.term=students.term', 'left')
                ->group_by('terms.term');
        }

        return $this;
    }

    public function get($id)
    {
        return $this->db->where('terms.term', $id)->get('terms')->first_row();
    }

    public function get_or_fail($id, $fail_data = array(), $header_status = 404)
    {
        $result = $this->get($id);

        if ($result) {
            return $result;
        }

        if ($fail_data) {
            $this->output->json($fail_data, $header_status);
        }

        show_404();
    }

    public function all(array $data = array())
    {
        $this->query($data)->get('terms')->result();
    }

    public function paginate(array $data = array())
    {
        if (!isset($data['perpage'])) {
            $perpage = 10;
        } else {
            $perpage = ((int)$data['perpage'] < 0) ? 10 : (int)$data['perpage'];
        }

        if (!isset($data['page'])) {
            $page = 1;
        } else {
            $page = ((int)$data['page'] < 1) ? 1 : (int)$data['page'];
        }

        $results = $this
            ->query($data)
            ->limit((int)$perpage, (int)($perpage * $page - $perpage))
            ->get('terms')
            ->result();

        $total = $this
            ->query($data)
            ->from('terms')
            ->count_all_results();

        return $this->pagination($total, $results, $perpage, $page);
    }

    public function query(array $data)
    {
        if (isset($data['with'])) {
            $this->with($data['with']);
        }

        if (isset($data['order_by'])) {
            $allows = array('term');
            if (is_array($data['order_by'])) {
                foreach ($data['order_by'] as $by => $order) {
                    if (in_array($by, $allows)) {
                        $order = strtoupper($order);
                        $order = $order == 'DESC' ? 'DESC' : ($order == 'ASC' ? 'ASC' : 'DESC');
                        $this->db->order_by($by, $order);
                    }
                }
            }
        }
        
        return $this->db;
    }

    public function create(array $data)
    {
        $data = array_only($data, $this->fillable);
        if ($data) {
            $this->db->insert('terms', $data);
            return $this->get($this->db->insert_id());
        }

        return null;
    }

    public function update(array $data, $id)
    {
        $data = array_only($data, $this->fillable);
        if ($data) {
            $this->db->where('term', (int)$id)->update('terms', $data);
            return $this->get((int)$data['term']);
        }

        return $this->get((int)$id);
    }

    public function update_or_fail(array $data, $id, $fail_data = array(), $header_status = 404)
    {
        $result = $this->update($data, $id);

        if ($result) {
            return $result;
        }

        if ($fail_data) {
            $this->output->json($fail_data, $header_status);
        }

        show_404();
    }

    public function delete($id)
    {
        $this->db->where('term', (int)$id)->delete('terms');
    }
}
