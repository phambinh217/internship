<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Output extends CI_Output
{
    public function json($json, $header_status = 200)
    {
        $this->set_content_type('application/json')
            ->set_status_header($header_status)
            ->set_output(json_encode($json))
            ->_display();
        die;
    }
}
