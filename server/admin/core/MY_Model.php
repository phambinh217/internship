<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model
{
    protected function pagination($total, $data, $perpage, $page)
    {
        $this->load->library('pagination');
        $config = array(
            'base_url' => url('/'),
            'total_rows' => $total,
            'per_page' => $perpage,
        );
        $this->pagination->initialize($config);

        $results = new stdClass();
        $results->total = $total;
        $results->data = $data;
        $results->perpage = $perpage;
        $results->page = $page;
        $results->total_page = (int) ceil($total/$perpage);
        $results->render = $this->pagination;

        return $results;
    }

    // test for v2 model
    protected function has_many(&$results, $type, $model, $alias, $foreign_key, $primary_key = 'id', array $query = array())
    {
        if ($type == 'list') {
            $foreign_keys = array_pluck($results, $primary_key);
            if (!$foreign_keys) {
                show_error('primary key must exists in results');
            }

            $query[$foreign_key] = $foreign_keys;
        } else {
            $query[$foreign_key] = $results->{$primary_key};
        }
        
        $foreign_results = $model->paginate($query);

        if (!isset($foreign_results->data[0]->{$foreign_key})) {
            show_error('foreign keys must exists in results');
        }

        if ($type == 'list') {
            foreach ($results as &$result) {
                $result->{$alias} = array_filter($foreign_results->data, function ($item) use ($result, $primary_key, $foreign_key) {
                    return $item->{$foreign_key} == $result->{$primary_key};
                });
            }
        } else {
            $results->{$alias} = $foreign_key_data->data;
        }
    }
}