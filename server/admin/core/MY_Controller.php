<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    protected $data;

    public function __construct()
    {
        parent::__construct();

        $this->__register();

        // Middleware required request authenticated
        $this->required_authentication();

        $this->__boot();

        // Demo mode
        if (APPLICATION_ENVIRONMENT == 'demo') {
            $this->allow_demo_mode();
        }

        // Check register mode
        $this->allow_register();

        // Middleware check request permission
        $this->policy();
    }

    /**
     * Register for app
     * 
     * If you need register somes infomation for your app, placed it here.
     * Example: Register permission, autoload common file,...
     * 
     * @return void
     */
    protected function __register()
    {   
        // Load common language
        $this->lang->load('common');

        $this->register_permission();
    }

    /**
     * Register permission
     * @return void
     */
    private function register_permission()
    {
        $this->lang->load('permission');

        // Register gates
        $this->gate->define('setting', trans('permission_setting'));

        $this->gate->define('user.read', trans('permission_user_read'));
        $this->gate->define('user.edit', trans('permission_user_edit'));
        $this->gate->define('user.create', trans('permission_user_create'));
        $this->gate->define('user.delete', trans('permission_user_delete'));
        $this->gate->define('user.login-as', trans('permission_user_login_as'));

        $this->gate->define('role.edit', trans('permission_role_edit'));
        $this->gate->define('role.create', trans('permission_role_create'));
        $this->gate->define('role.delete', trans('permission_role_delete'));

        $this->gate->define('log-system.read', trans('permission_log_system_read'));
        $this->gate->define('log-system.delete', trans('permission_log_system_delete'));
        
        $this->gate->define('student.read', 'Read student');
        $this->gate->define('student.create', 'Create student');
        $this->gate->define('student.edit', 'Edit student');
        $this->gate->define('student.delete', 'Delete student');

        $this->gate->define('lecturer.read', 'Read lecturer');
        $this->gate->define('lecturer.create', 'Create lecturer');
        $this->gate->define('lecturer.edit', 'Edit lecturer');
        $this->gate->define('lecturer.delete', 'Delete lecturer');

        $this->gate->define('school.read', 'Read school');
        $this->gate->define('school.create', 'Create school');
        $this->gate->define('school.edit', 'Edit school');
        $this->gate->define('school.delete', 'Delete school');

        $this->gate->define('internship.read', 'Read internship');
        $this->gate->define('internship.create', 'Create internship');
        $this->gate->define('internship.edit', 'Edit internship');
        $this->gate->define('internship.delete', 'Delete internship');

        $this->gate->define('internship.report.read', 'Read internship report');
        $this->gate->define('internship.report.add-student', 'Add student to internship');
        $this->gate->define('internship.report.edit-student', 'Edit student from internship');
        $this->gate->define('internship.report.delete-student', 'Delete student from internship');

        // Set relationship betwen gate and controller
        $this->policy->define(array(
            'User_controller/index' => 'user.read',
            'User_controller/show' => 'user.read',
            'User_controller/store' => 'user.create',
            'User_controller/update' => 'user.update',
            'User_controller/delete' => 'user.delete',
            
            'Role_controller/index' => 'user.read',
            'Role_controller/show' => 'user.read',
            'Role_controller/store' => 'role.create',
            'Role_controller/update' => 'role.update',
            'Role_controller/delete' => 'role.delete',

            'Log_system_controller/index' => 'log-system.read',
            'Log_system_controller/show' => 'log-system.read',
            'Log_system_controller/delete' => 'log-system.delete',

            'Student_controller/index' => 'student.read',
            'Student_controller/show' => 'student.read',
            'Student_controller/store' => 'student.create',
            'Student_controller/update' => 'student.update',
            'Student_controller/delete' => 'student.delete',
            
            'Lecturer_controller/index' => 'lecturer.read',
            'Lecturer_controller/show' => 'lecturer.read',
            'Lecturer_controller/store' => 'lecturer.create',
            'Lecturer_controller/update' => 'lecturer.update',
            'Lecturer_controller/delete' => 'lecturer.delete',

            'Faculty_controller/index' => 'school.read',
            'Faculty_controller/show' => 'school.read',
            'Faculty_controller/store' => 'school.create',
            'Faculty_controller/update' => 'school.update',
            'Faculty_controller/delete' => 'school.delete',

            'Major_controller/index' => 'school.read',
            'Major_controller/show' => 'school.read',
            'Major_controller/store' => 'school.create',
            'Major_controller/update' => 'school.update',
            'Major_controller/delete' => 'school.delete',

            'Term_controller/index' => 'school.read',
            'Term_controller/show' => 'school.read',
            'Term_controller/store' => 'school.create',
            'Term_controller/update' => 'school.update',
            'Term_controller/delete' => 'school.delete',
            
            'Class_controller/index' => 'school.read',
            'Class_controller/show' => 'school.read',
            'Class_controller/store' => 'school.create',
            'Class_controller/update' => 'school.update',
            'Class_controller/delete' => 'school.delete',
            
            'Internship_student_controller/index' => 'internship.report.read',
            'Internship_student_controller/show' => 'internship.report.read',
            'Internship_student_controller/exists' => 'internship.report.read',
            'Internship_student_controller/students_count_by_date' => 'internship.report.read',
            'Internship_student_controller/student_registered' => 'internship.report.read',
            'Internship_student_controller/student_scope' => 'internship.report.read',
            'Internship_student_controller/store' => 'internship.report.add-student',
            'Internship_student_controller/update' => 'internship.report.edit-student',
            'Internship_student_controller/delete' => 'internship.report.delete-student',

            'Internship_lecturer_controller/index' => 'internship.report.read',
            'Internship_lecturer_controller/full_student' => 'internship.report.red',

            'Internship_controller/index' => 'internship.read',
            'Internship_controller/show' => 'internship.read',
            'Internship_controller/store' => 'internship.create',
            'Internship_controller/update' => 'internship.update',
            'Internship_controller/delete' => 'internship.delete',
        ));   
    }

   /**
     * The common script run all method
     * 
     * This method is run after register method
     */
    protected function __boot()
    {
        $this->output->set_header('Access-Control-Allow-Origin: *');
        
        $this->load->model(array('user_model', 'role_model'));

        // Check permission and log the last time user online
        if ($this->jwt_auth->check()) {
            $this->gate->set_user($this->jwt_auth->user());
            $this->user_model->update(array('last_online_at' => time()), $this->jwt_auth->user()->id);
            
            $role = $this->role_model->get($this->jwt_auth->user()->role_id);

            if ($role->permission == '*') {
                $this->gate->set_allow('*');
            } elseif (is_array($role->permission)) {
                $allow = array();
                foreach ($this->gate->all() as $permission_key => $permission_name) {
                    if (in_array($permission_key, $role->permission)) {
                        $allow[$permission_key] = true;
                    } else {
                        $allow[$permission_key] = false;
                    }
                }

                $this->gate->set_allow($allow);
            } else {
                $this->gate->set_allow(false);
            }
        } else {
            $this->gate->set_allow(false);
        }
    }

    protected function required_authentication()
    {
        $except = array(
            'api/login',
            'api/register',
            'api/forgot-password',
            'api/reset-password',
            'api/configurations/setting'
        );

        $token = $this->input->get_post('token');
        $is_veriry = $this->jwt_auth->verify($token);

        if (!in_array(uri_string(), $except)) {
            if (!$is_veriry) {
                $this->output->json(array(
                    'message' => 'No authenticated.',
                ), 401);
            }
        }
    }

    protected function allow_demo_mode()
    {
        // Except urls below
        $except = array(
            'api/me/update-password',
            'api/users/(:num)/delete',
            'api/roles/(:num)/update',
            'api/roles/(:num)/delete',
            'api/logs-system/delete',
            'api/configurations/save',
            'api/files/directories/(:num)/delete',
            'api/files/(:num)/delete',
            'api/students/(:num)/delete',
            'api/lecturers/(:num)/delete',
            'api/terms/(:num)/delete',
            'api/classes/(:num)/delete',
            'api/faculties/(:num)/delete',
            'api/majors/(:num)/delete',
            'api/internships/(:num)/delete',
            'api/internships/(:num)/students/(:num)/delete'
        );

        $allow = true;
        foreach ($except as $route) {
            $route = str_replace(array(':any', ':num'), array('[^/]+', '[0-9]+'), $route);
            if (preg_match('#^'.$route.'$#', uri_string(), $matches)) {
                $allow = false;
            }
        }

        if (!$allow) {
            $response = array(
                'message' => trans('demo_mode_not_allow'),
                'errors' => array(
                    'not_allow' => trans('demo_mode_not_allow'),
                ),
            );

            $this->output->json($response, 406);
        }
    }

    /**
     * Middleware check request permission
     *
     * If pass,  go to next request. If fail, throw 403 error
     * @return void
     */
    protected function policy()
    {
        $this->policy->policy_request();
    }

    protected function allow_register()
    {
        $this->lang->load('auth');

        $uri = uri_string();
        if ($uri == 'api/register' && $this->input->method() == 'post') {
            if ($this->config_model->get('register') == 0) {
                $response = array(
                    'message' => trans('auth_message_register_disable'),
                    'errors' => array(
                        'disable_register' => trans('auth_message_register_disable'),
                    ),
                );
                $this->output->json($response, 406);
            }
        }
    }
}
