<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['setting'] = 'Setting';
$lang['setting_general'] = 'Setting general';
$lang['setting_logo'] = 'Logo';
$lang['setting_register_account'] = 'Register new account';
$lang['setting_user_role_id_registered'] = 'User\'s role when registered';