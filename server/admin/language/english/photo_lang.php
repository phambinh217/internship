<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['photo'] = 'Photo';
$lang['photo_uploaded'] = 'Uploaded';
$lang['photo_could_not_upload'] = 'Could not upload';